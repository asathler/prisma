<?php

namespace Projeto;

use PHPUnit\Framework\TestCase;

class EstruturaTest extends TestCase
{
    public function test_a_estrutura_do_projeto_tem_controllers_padronizadas(): void
    {
        $controllersPath = '';
        $controllersPath .= DIRECTORY_SEPARATOR . 'app';
        $controllersPath .= DIRECTORY_SEPARATOR . 'Http';
        $controllersPath .= DIRECTORY_SEPARATOR . 'Controllers';

        $caminho = $this->appPath($controllersPath);

        $ignorarEstruturas = [
            '.', '..',
            '_exemplos'
        ];

        $conteudo = array_diff(
            scandir($caminho),
            $ignorarEstruturas
        );

        dd(
            'test',
            $controllersPath,
            $caminho,
            $conteudo,
            '...'
        );
        $this->assertTrue(true);
    }

    private function appPath($caminho = '')
    {
        $appPath = dirname(dirname(dirname(__DIR__)));

        if (! empty($caminho)) {
            $primeiroChar = substr($caminho, 0, 1);

            $caminho = (
                $primeiroChar === DIRECTORY_SEPARATOR
                ? ''
                : DIRECTORY_SEPARATOR
            ) . $caminho;
        }

        return $appPath . $caminho;
    }
}
