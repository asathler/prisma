@extends('layouts/layoutMaster')

@section('title', 'Estações')

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-buttons-bs5/buttons.bootstrap5.css') }}">
@endsection

@section('vendor-script')
    <script src="{{ asset('assets/vendor/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('assets/js/estacao/index.js') }}"></script>
@endsection

@section('content')
    <h4 class="fw-bold py-3 mb-4">
      <span class="text-muted fw-light">Estacões /</span> Listagem
    </h4>

    <!-- Column Search -->
    <div class="card">
        <div class="card-datatable table-responsive">
            <table class="dt-column-search table">
                <thead>
                <tr>
                    <th>Sigla</th>
                    <th>Nome</th>
                    <th>Nome Indicativo</th>
                    <th>Descrição</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Sigla</th>
                    <th>Nome</th>
                    <th>Nome Indicativo</th>
                    <th>Descrição</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
