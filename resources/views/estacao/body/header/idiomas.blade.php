<!-- Idiomas -->
<li class="nav-item dropdown dropdown-language">
    <a href="javascript:void(0);"
       id="dropdown-flag"
       class="nav-link dropdown-toggle"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false">
        <i class="flag-icon flag-icon-br"></i>
        <span class="selected-language">
            Português
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag">
        <a class="dropdown-item" href="javascript:void(0);" data-language="pt">
            <i class="flag-icon flag-icon-br"></i>
            Português
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="es">
            <i class="flag-icon flag-icon-es"></i>
            Espanhol
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="en">
            <i class="flag-icon flag-icon-us"></i>
            Inglês
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="fr">
            <i class="flag-icon flag-icon-fr"></i>
            Francês
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="de">
            <i class="flag-icon flag-icon-de"></i>
            Alemão
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="jp">
            <i class="flag-icon flag-icon-jp"></i>
            Japonês
        </a>
        <a class="dropdown-item" href="javascript:void(0);" data-language="sa">
            <i class="flag-icon flag-icon-sa"></i>
            Árabe
        </a>
    </div>
</li>
<!-- Idiomas -->
