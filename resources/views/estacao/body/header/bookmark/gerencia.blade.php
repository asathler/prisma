<li class="nav-item d-none d-lg-block">
    <a class="nav-link bookmark-star">
        <i class="ficon text-warning" data-feather="star"></i>
    </a>
    <div class="bookmark-input search-input">
        <div class="bookmark-input-icon"><i data-feather="search"></i></div>
        <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
        <ul class="search-list search-list-bookmark"></ul>
    </div>
</li>
