<!-- BEGIN: Content-->
<script>
    // TODO: Porque deste script?
    let $estacao_id = {!! json_encode($estacao->uuid) !!};
</script>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <!-- Pagina Equipamentos / Emissões -->
            <section id="dashboard-analytics">
                @include('estacao.body.content.linha1')

                @include('estacao.body.content.linha4')

                {{-- @include('estacao.body.content.linha2') --}}
                {{-- @include('estacao.body.content.linha3') --}}
            </section>
            <!-- Pagina Equipamentos / Emissões end -->

        </div>
    </div>
</div>
<!-- END: Content-->
