<!-- BEGIN: Header-->
<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item">
                    <a class="nav-link menu-toggle" href="javascript:void(0);">
                        <i class="ficon" data-feather="menu"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
                @include('estacao.body.header.bookmark.todos')
            </ul>
            <ul class="nav navbar-nav">
                @include('estacao.body.header.bookmark.gerencia')
            </ul>
        </div>

        <ul class="nav navbar-nav align-items-center ml-auto">
            @include('estacao.body.header.idiomas')

            @include('estacao.body.header.lightDark')

            @include('estacao.body.header.search.barra')

            @include('estacao.body.header.compras')

            @include('estacao.body.header.notificacoes')

            @include('estacao.body.header.usuario')
        </ul>
    </div>
</nav>

@include('estacao.body.header.search.list')

@include('estacao.body.header.search.semResultado')
<!-- END: Header-->
