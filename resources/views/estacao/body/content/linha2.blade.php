<div class="row match-height">
    <!-- Quadro 12/12 -->
    <div class="col-lg-12 col-12">
        <div class="card">
            <div class="card-datatable text-nowrap">
                <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                    <div class="table-responsive ">
                        <table class="dt-complex-header equipamentos_emissao table table-bordered dataTable no-footer"
                               id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info"
                               style="width: 1328px;">
                            <thead>
                            <tr>
                                <th colspan="3" rowspan="1">Equipamentos</th>
                                <th colspan="2" rowspan="1">Emissões</th>
                            </tr>
                            <tr>
                                <th>ID eQUIP</th>
                                <th>Fabricante</th>
                                <th>Modelo</th>
                                <th>Descrição</th>
                                <th>Freq. Inicial</th>
                                <th>Freq. Final</th>
                                <th>id Emissao</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($estacao->ConjuntoEstacao as $ConjuntoEstacao)
                                @foreach($ConjuntoEstacao->conjuntoEquipamentos as $conjuntoEquipamento)
                                    @foreach($conjuntoEquipamento->detalhes as $detalhes)
                                        @foreach($detalhes->equipamentos as $equipamento)
                                            <tr>
                                                <td style="white-space:pre-wrap">{{$equipamento->uuid}}</td>
                                                <td>{{$equipamento->fabricante}}</td>
                                                <td>{{$equipamento->modelo}}</td>
                                                <td style="white-space:pre-wrap">{{$equipamento->descricao}}</td>
                                                @foreach($ConjuntoEstacao->ConjuntoEmissao as $conjEmissao)
                                                    @foreach($conjEmissao->emissoes as $emissao)
                                                        <td>{{$emissao->frequencia_inicio}}</td>
                                                        <td >{{$emissao->frequencia_termino}}</td>
                                                        <td >{{$emissao->id}}</td>
                                                    @endforeach
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach

                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Quadro 12/12 -->
</div>
