<div class="row match-height">
    <!-- Timeline Left-->
    <div class="col-xl-6 mb-4 mb-xl-0">
        <div class="card">
            <h5 class="card-header">Historico de Licenças da Estação</h5>
            <div class="card-body pb-0">
                <ul class="timeline mb-0">
                    @foreach ($historicolicenca as $hist)
                        <li class="timeline-item timeline-item-transparent">
                            @if ($loop->first)
                                <span class="timeline-point timeline-point-success"></span>
                                <div class="timeline-event">
                                    <div class="timeline-header mb-sm-0 mb-3">
                                        <h6 class="mb-0">Ativa</h6>
                                        @else
                                            <span class="timeline-point timeline-point-danger"></span>
                                            <div class="timeline-event">
                                                <div class="timeline-header mb-sm-0 mb-3">
                                                    <h6 class="mb-0">Vencida</h6>
                                                    @endif

                  <h7><span class="text-muted"> Inicio: {{ date('d-m-Y', strtotime($hist->data_inicio))}}</span></h7>
                                                </div>
                                                <p>
                                                    @foreach ($licencas as $lic)
                                                        @if ($lic->uuid == $hist->licencas_uuid)
                                                            {{ $lic->descricao }}
                                                        @endif
                                                        @endforeach
                                                </p>
                                                <div class="d-flex justify-content-between">
                                                    <h6></h6>
                                                    <div class="d-flex">
                                                        <div class="mb-sm-0 mb-3">
                  <h7><span class="text-muted"> Termino: {{ date('d-m-Y', strtotime($hist->data_termino))}}</span></h7>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                        </li>
                        <br>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!-- /Timeline Left-->
</div>
