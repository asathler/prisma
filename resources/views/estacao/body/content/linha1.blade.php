<div class="row match-height">
    <!-- Quadro 8/12 -->
    <div class="col-lg-8 col-12">
        <div class="card">
            <div class="card-body">
                <div>
                    <div class="d-flex justify-content-between">
                        <h2 class="font-weight-bolder mb-25">
                            {{ $estacao->SG_ESTACOES }} - {{ $estacao->NO_ESTACOES }} ({{ $estacao->nome_indicativo }})
                        </h2>
                        <span class="badge badge-light-success text-sm h-25">
                            Verificada
                        </span>
                    </div>
{{--                    <p class="bg-info">--}}
{{--                        {{ $eq2[0] ?? 'sem dados' }}--}}
{{--                    </p>--}}
                    <p>
                        {{ $estacao->DE_ESTACOES }}
                    </p>
                </div>

                <div class="row match-height">
                    <div class="col-lg-6 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Entidade
                        </h6>
                        <p class="text-sm">
                            {{ $estacao->outorga->entidade->DE_ENTIDADE }}
                        </p>
                    </div>
                    <div class="col-lg-6 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Status atual
                        </h6>
                        <p class="text-sm">
                            {{ $estacao->status->DE_STATUS }}
                        </p>
                    </div>
                </div>

                <div class="row match-height">
                    <div class="col-lg-6 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Outorga
                        </h6>
                        <p class="text-sm">
                            {{ $estacao->outorga->DE_OUTORGAS }}
                        </p>
                    </div>
{{--                    <div class="col-lg-6 col-12">--}}
{{--                        <h6 class="font-weight-bolder mb-25">--}}
{{--                            Plano Básico--}}
{{--                        </h6>--}}
{{--                        <p class="text-sm">--}}
{{--                            {{ $estacao->plano->DE_PLANOS_BASICOS }}--}}
{{--                        </p>--}}
{{--                    </div>--}}
                </div>

                <div class="row match-height">
                    <div class="col-lg-6 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Responsável
                        </h6>
                        <p class="text-sm">
                            {{ $estacao->criador->NO_USUARIO }} <br />
                            {{ $estacao->criador->cpfLgpd() }} <br />
                            {{ $estacao->criador->cpfFormatado() }} <br />
                            {{ $estacao->criador->CO_CPF }} <br />
                        </p>
                    </div>
                    <div class="col-lg-3 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Início da atividade
                        </h6>
                        <p class="text-sm">
{{--                            {{ $estacao->dataInicioLicenca()->format('d/m/Y') }}--}}
                        </p>
                    </div>
                    <div class="col-lg-3 col-12">
                        <h6 class="font-weight-bolder mb-25">
                            Término da atividade
                        </h6>
                        <p class="text-sm">
                            <span class="text-danger">
{{--                                {{--}}
{{--                                    Str::ucfirst(--}}
{{--                                        $estacao->dataTerminoLicenca()->diffForHumans()--}}
{{--                                    ) .--}}
{{--                                    ' (' .--}}
{{--                                    $estacao->dataTerminoLicenca()->format('d/m/Y') .--}}
{{--                                    ').'--}}
{{--                                }}--}}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Quadro 8/12 -->

    <!-- Quadro 4/12 -->
    <div class="col-lg-4 col-12">
        <div class="card">
            <div class="card-body">
                <div>
                    <h2 class="font-weight-bolder mb-25">
                        {{ $estacao->tipo->NO_TIPO_ESTACOES }}
                    </h2>
                </div>

                <div>
                    <img src="{{ asset('imgs/estacoes/'.$estacao->DE_FOTO_ESTACOES) }}"
                         alt="Nome da Estação"
                         title="Nome da Estação"
                         width="100%"
                    />
                </div>
            </div>
        </div>
    </div>
    <!-- Quadro 4/12 -->
</div>
