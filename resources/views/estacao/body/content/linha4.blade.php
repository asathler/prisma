<div class="row match-height">
    <!-- Quadro 12/12 -->
    <div class="col-lg-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="row match-height">
                    <div class="col-lg-8 col-12">
                        <h2 class="font-weight-bolder">
                            Equipamentos
                        </h2>
                    </div>
                    <div class="col-lg-4 col-12">
                        <h2 class="font-weight-bolder">
                            Emissões
                        </h2>
                    </div>
                </div>

                @foreach($estacao->ConjuntoEstacao as $registro)
                    @if ($loop->last)
                        <div class="row match-height mt-4">
                    @else
                        <div class="row match-height mt-4 border-bottom border-secondary">
                    @endif
                        <div class="col-lg-8 col-12">
                            @forelse($registro->equipamentos() as $equipamento)
                                <p class="text-sm">
                                    {{ $equipamento->modelo }}
                                </p>
                            @empty
                                -
                            @endforelse
                        </div>
                        <div class="col-lg-4 col-12">
                            @forelse($registro->emissoes() as $emissao)
                                <p class="text-sm">
                                    {{ $emissao->frequencia_inicio . ' - ' . $emissao->frequencia_termino }}
                                </p>
                            @empty
                                -
                            @endforelse
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
    <!-- Quadro 12/12 -->
</div>
