<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="vuexy-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/vendors/css/charts/apexcharts.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/vendors/css/extensions/toastr.min.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/colors.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/components.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/themes/dark-layout.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/themes/bordered-layout.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/themes/semi-dark-layout.css">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/plugins/charts/chart-apex.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/plugins/extensions/ext-component-toastr.css">
<link rel="stylesheet" type="text/css" href="vuexy-assets/css/pages/app-invoice-list.css">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
<!-- END: Custom CSS-->
