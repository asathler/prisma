<link rel="apple-touch-icon" href="vuexy-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon" href="vuexy-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
      rel="stylesheet"
>
<link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.4.0/css/all.css">
