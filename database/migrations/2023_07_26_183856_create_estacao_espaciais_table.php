<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: Campo classe deve gerar tabela com tais opções? [ Linha 8. Campos de Telecom.xlsx ].
        // TODO: Revisar faixas de subida e descida com PO

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_ESPACIAL', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_ESPACIAL', 'PK_ESTACAO_ESPACIAL');
            $table->bigInteger('ID_ESTACAO_ESPACIAL');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_ESPACIAL_01')
                ->constrained('TB_ESTACAO_ESPACIAL', 'UUID_ESTACAO', 'FK_TB_ESTACAO_ESPACIAL_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_OPERADOR');
            $table->string('NO_UIT');
            $table->integer('NU_CLASSE');
            $table->date('DT_DIREITO_EXPLORACAO'); // Licença?

            // Relacionamento 1-N
            $table->string('NO_REPRESENTANTE_LEGAL')->nullable();
            $table->string('NU_CPF_REPRESENTANTE_LEGAL')->nullable();

            $table->decimal('NU_LONGITUTE_ORBITAL')->defaul(0);
            $table->string('IC_MERIDIANO')->nullable();
            $table->string('DE_POSICAO_ORBITAL');
            $table->string('DE_FAIXA_ENTRADA');
            // $table->string('DE_SUB_FAIXA');
            $table->string('CO_CONCATENADO')->nullable(); // reed solomon, turbo code
            $table->integer('VR_CODIGO_CONCATENADO')->default(0);

            // Equipamento??
            $table->string('DE_TRANSPONDER')->nullable();
            $table->string('DE_RESTRICAO')->nullable();

            // Relacionamento Cobertura? 1-N
            //TODO: Verificar se campo ED_UF_COBERTURA deveria estar na emissão
            $table->string('ED_UF_COBERTURA')->nullable();

            // Duplicidade?
            $table->string('DE_SUBIDA_DESCIDA')->nullable();
            $table->string('DE_FAIXA_SUBIDA_DISPONIVEL')->nullable();
            $table->string('DE_FAIXA_SUBIDA_UTILIZADA')->nullable();
            $table->string('DE_FAIXA_DESCIDA_DISPONIVEL')->nullable();
            $table->string('DE_FAIXA_DESCIDA_UTILIZADA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_ESPACIAL');
    }
};
