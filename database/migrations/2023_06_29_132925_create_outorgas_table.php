<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_OUTORGAS', function (Blueprint $table) {
            $table->uuid('UUID_OUTORGAS')->primary('PK_OUTORGAS')->comment('Identificador da tabela');
            $table->string('DE_OUTORGAS', 255)->nullable();
            $table->string('DE_CERTIFICADO', 255)->nullable(); // TODO: Para que esse campo aqui??
            $table->string('NU_FISTEL', 11)->index('IX_TB_OUTORGAS_01')->unique();
            $table->foreignId('FK_TB_SERVICO_TB_OUTORGA')
                ->index('IX_TB_OUTORGAS_02')
                ->constrained('CC_TB_SERVICOS_ID_SERVICOS_01')
                ->references('ID_SERVICOS')->on('TB_SERVICOS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('FK_TB_ENTIDADE_TB_OUTORGA')
                ->index('IX_TB_OUTORGAS_03')
                ->constrained('CC_TB_ENTIDADES_ID_ENTIDADE_01')
                ->references('UUID_ENTIDADE')->on('TB_ENTIDADES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('FK_TB_USUARIOS_TB_OUTORGAS_INCLUSAO')
                ->index('IX_TB_OUTORGAS_04')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_01')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('FK_TB_USUARIOS_TB_OUTORGAS_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_OUTORGAS_05')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_02')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_MOTIVOS_EXCLUSOES_TB_SERVICOS')
                ->nullable()
                ->index('IX_TB_OUTORGAS_06')
                ->constrained('CC_TB_MOTIVOS_EXCLUSOES_ID_MOTIVOS_EXCLUSOES_03')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_OUTORGAS');
    }
};
