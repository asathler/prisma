<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_MOTIVO_EXCLUSAO', function (Blueprint $table) {
            $table->id('ID_MOTIVO_EXCLUSAO')->comment('Identificador da tabela');
            $table->string('SG_MOTIVO_EXCLUSAO', 2)->index('IX_TB_MOTIVO_EXCLUSAO')->comment('Sigla do motivo da exclusao');
            $table->string('DE_MOTIVO_EXCLUSAO', 255)->comment('Descrição do motivo da exclusao');
            $table->timestamp('DH_INCLUSAO')->useCurrent()->comment('Data da inclusão');
            $table->softDeletes('DH_EXCLUSAO')->comment('Data da exclusao');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_MOTIVO_EXCLUSAO');
    }
};
