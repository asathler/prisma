<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: tipo_1 // Principal, salvamento ou outros
        // TODO: tipo_2 // Telegrafia, telefonia ou rádio
        // TODO: Ver transformação do campo arrendamento de boolean para date

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_RADIO_AMADOR', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_RADIO_AMADOR', 'PK_ESTACAO_RADIO_AMADOR');
            $table->bigInteger('ID_ESTACAO_RADIO_AMADOR');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_RADIO_AMADOR_01')
                ->constrained('TB_ESTACAO_RADIO_AMADOR', 'UUID_ESTACAO', 'FK_TB_ESTACAO_RADIO_AMADOR_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO');
            $table->string('DH_VACANCIA')->nullable();
            $table->string('DE_CLASSE_RESPONSAVEL')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_RADIO_AMADOR');
    }
};
