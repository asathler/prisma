<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_CONJUNTO_EQUIPAMENTO', function (Blueprint $table) {
            //$table->id('ID_CONJUNTO_EQUIPAMENTO');
            $table->primary('ID_CONJUNTO_EQUIPAMENTO', 'PK_TB_CONJUNTO_EQUIPAMENTO');
            $table->unsignedBigInteger('ID_CONJUNTO_EQUIPAMENTO');
            $table->string('NO_NOME_REDE')->index('IX_TB_CONJUNTO_EQUIPAMENTO_01')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_CONJUNTO_EQUIPAMENTO');
    }
};
