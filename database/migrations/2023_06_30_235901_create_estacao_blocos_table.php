<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: avaliar se teremos um novo campo ou nova tabela para as estações N-N

        Schema::create('TB_ESTACAO_BLOCOS', function (Blueprint $table) {
            $table->id('ID_ESTACAO_BLOCOS');

            $table->foreignUuid('FK_TB_OUTORGA_TB_ESTACAO_BLOCOS')
                ->index('IX_TB_ESTACAO_BLOCOS_01')
                ->constrained('CC_TB_OUTORGAS_ID_OUTORGAS_01')
                ->references('UUID_OUTORGAS')->on('TB_OUTORGAS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->string('CO_SEQUENCIAL');
            $table->string('CO_REFERENCIA', 7)->index('IX_TB_ESTACAO_BLOCOS_02')->comment('Data de referência, formato AAAA-MM');
            $table->string('CO_LETRA_DEBITO_TFI')->index('IX_TB_ESTACAO_BLOCOS_03');
            $table->unsignedBigInteger('NU_HABILITADAS')->default(0);
            $table->unsignedBigInteger('NU_DESABILITADAS')->default(0);
            $table->unsignedBigInteger('NU_REABILITADAS')->default(0);
            $table->bigInteger('NU_SALDO')->default(0); // calculado? H - D + R + (Saldo referência anterior)
            $table->unsignedBigInteger('NU_LICENCIADA')->default(0);
            $table->unsignedBigInteger('NU_CREDITO')->default(0);
            $table->unsignedBigInteger('NU_CREDITO_CANCELADO')->default(0);

            $table->foreignUuid('FK_TB_USUARIOS_TB_ESTACAO_BLOCOS_INCLUSAO')
                ->index('IX_TB_ESTACAO_BLOCOS_04')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_01')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('FK_TB_USUARIOS_TB_ESTACAO_BLOCOS_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_ESTACAO_BLOCOS_05')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_02')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_MOTIVOS_EXCLUSOES_TB_ESTACAO_BLOCOS')
                ->nullable()
                ->index('IX_TB_ESTACAO_BLOCOS_06')
                ->constrained('CC_TB_MOTIVOS_EXCLUSOES_ID_MOTIVOS_EXCLUSOES_03')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_BLOCOS');
    }
};
