<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_ENTIDADES', function (Blueprint $table) {
            $table->uuid('UUID_ENTIDADE')->primary('PK_TB_ENTIDADES')->comment('Identificador da tabela');
            $table->string('DE_ENTIDADE', 255)->index('IX_TB_ENTIDADES_01');
            $table->foreignUuid('FK_TB_USUARIOS_TB_ENTIDADES_INCLUSAO')
                ->index('IX_TB_ENTIDADES_02')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_01')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('FK_TB_USUARIOS_TB_ENTIDADES_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_ENTIDADES_03')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_02')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_MOTIVOS_EXCLUSOES_TB_ENTIDADES')
                ->nullable()
                ->index('IX_TB_ENTIDADES_04')
                ->constrained('CC_TB_MOTIVOS_EXCLUSOES_ID_MOTIVOS_EXCLUSOES_03')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_entidades');
    }
};
