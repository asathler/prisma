<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_STATUS', function (Blueprint $table) {
            $table->id('ID_STATUS');
            $table->string('DE_STATUS', 255)->index('IX_TB_STATUS');
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('status');
    }
};
