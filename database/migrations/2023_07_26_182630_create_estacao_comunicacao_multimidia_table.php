<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: campo selecao_canais não deve ir pra outra tabela?

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_COMUNICACAO_MULTIMIDIA', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_COMUNICACAO_MULTIMIDIA', 'PK_ESTACAO_COMUNICACAO_MULTIMIDIA');
            $table->bigInteger('ID_ESTACAO_COMUNICACAO_MULTIMIDIA');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_COMUNICACAO_MULTIMIDIA_01')
                ->constrained('TB_ESTACAO_COMUNICACAO_MULTIMIDIA', 'UUID_ESTACAO', 'FK_TB_ESTACAO_COMUNICACAO_MULTIMIDIA_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->integer('NU_CAPACIDADE_INSTALADA_MBPS')->default(0);
            $table->integer('QT_ACESSO_INSTALADO')->default(0);
            $table->integer('QT_ACESSO_DE_0KBPS_A_64KBPS')->default(0);
            $table->integer('QT_ACESSO_DE_64KBPS_A_256KBPS')->default(0);
            $table->integer('QT_ACESSO_DE_256KBPS_A_1024KBPS')->default(0);
            $table->integer('QT_ACESSO_DE_1024KBPS_A_2MBPS')->default(0);
            $table->integer('QT_ACESSO_DE_2MBPS_A_8MBPS')->default(0);
            $table->integer('QT_ACESSO_DE_8MBPS_A_34MBPS')->default(0);
            $table->integer('QT_ACESSO_DE_34MBPS_A_155MBPS')->default(0);
            $table->integer('QT_ACESSO_DE_155MBPS_A_622MBPS')->default(0);
            $table->integer('QT_ACESSO_ACIMA_622MBPS')->default(0);
            $table->integer('QT_ACESSO_TOTAL')->default(0);
            $table->integer('QT_CANAIS')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_COMUNICACAO_MULTIMIDIA');
    }
};
