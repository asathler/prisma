<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: conferir ONDE manter o campo tecnologia...

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_PESSOAL', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_PESSOAL', 'PK_ESTACAO_PESSOAL');
            $table->bigInteger('ID_ESTACAO_PESSOAL');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_PESSOAL_01')
                ->constrained('TB_ESTACAO_PESSOAL', 'UUID_ESTACAO', 'FK_TB_ESTACAO_PESSOAL_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('DE_TECNOLOGIA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_PESSOAL');
    }
};
