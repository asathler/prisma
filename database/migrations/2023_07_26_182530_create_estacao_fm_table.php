<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_FM', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_FM', 'PK_ESTACAO_FM');
            $table->bigInteger('ID_ESTACAO_FM');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_FM_01')
                ->constrained('TB_ESTACAO_FM', 'UUID_ESTACAO', 'FK_TB_ESTACAO_FM_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO');

            $table->foreignId('ID_PLANO_BASICO')
                ->index('IX_TB_ESTACAO_FM_02')
                ->constrained('TB_ESTACAO_FM', 'ID_PLANO_BASICO', 'FK_TB_ESTACAO_FM_TB_PLANO_BASICO')
                ->references('ID_PLANOS_BASICOS')->on('TB_PLANOS_BASICOS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_FM');
    }
};
