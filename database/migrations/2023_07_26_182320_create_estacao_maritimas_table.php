<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: BLOCO de 'Equipamentos' => possível mudança para TABELA equipamentos
        // TODO: prova é navio 'de prova' ou 'em prova'?
        // TODO: Ver origem de dados hora_servico para combo
        // TODO: Trocar nullable() para default()
        // TODO: Gerar, para natureza_servico, tabela com opções: [ CO, CP, TR, TV, OT ]
        // TODO: Gerar, para natureza_servico, tabela com opções: [ C, D, O, P, R, T ]
        // TODO: Trocar servicos_disponiveis (repetido 6x) de string para boolean (para cada repetição)
        // TODO: Campo tipo_inmarsat deve gerar tab-auxiliar [ ver linha 74, Campos de Telecom.xlsx ]
        // TODO: Campos que não são regras de negócio nem aparecem na licença: poderiam ser removidos!

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_MARITIMA', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_MARITIMA', 'PK_ESTACAO_MARITIMA');
            $table->bigInteger('ID_ESTACAO_MARITIMA');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_MARITIMA_01')
                ->constrained('TB_ESTACAO_MARITIMA', 'UUID_ESTACAO', 'FK_TB_ESTACAO_MARITIMA_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO');
            $table->boolean('IC_PROVA');
            $table->date('DT_ARRENDAMENTO')->nullable();
            $table->string('HR_SERVICO')->nullable();
            $table->string('DE_CLASSIFICACAO_GERAL')->nullable();
            $table->string('DE_CLASSIFICACAO_INDIVIDUAL')->nullable();
            $table->boolean('IC_VIAGEM_INTERNACIONAL')->default(false);
            $table->string('NU_TELEX')->nullable();
            $table->string('DE_CHAMADA_SELETIVA')->nullable();
            $table->string('DE_MMSI')->nullable();
            $table->string('DE_MMSI_GRUPO')->nullable();
            $table->string('DE_NATUREZA_SERVICO');
            $table->string('DE_SERVICOS_DISPONIVEIS');

            // Equipamentos, talvez tenha uma tab associativa 1-N
            $table->boolean('IC_EPIRB')->default(false);
            $table->boolean('IC_CHAMADA_SELETIVA_DIGITAL')->default(false);
            $table->integer('DE_TIPO_EQUIPAMENTO_AUTOMATICO_COMUNICACAO')->default(false);
            $table->string('NU_INMARSAT')->nullable();
            $table->string('DE_TIPO_INMARSAT')->nullable();
            $table->string('DE_IIAC_INMARSAT')->nullable();

            // Campos que não são regras de negócio nem integram o documento da licença
            $table->string('DE_INSCRICAO_CAPITANIA')->nullable();
            $table->string('NO_CAPITANIA')->nullable();
            $table->unsignedInteger('QT_PESSOAS_BORDO')->default(0);
            $table->unsignedInteger('QT_BOTES')->default(0);
            $table->unsignedInteger('QT_CARGA')->default(0);
            $table->unsignedInteger('NU_IDENTIFICACAO_NAVIO')->default(0);
            $table->string('NO_RESPONSAVEL_AAIC')->nullable();
            $table->string('NO_CONTATO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_MARITIMA');
    }
};
