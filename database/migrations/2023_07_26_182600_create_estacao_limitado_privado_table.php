<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: campo selecao_canais não deve ir pra outra tabela?

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_LIMITADO_PRIVADO', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_LIMITADO_PRIVADO', 'PK_ESTACAO_LIMITADO_PRIVADO');
            $table->bigInteger('ID_ESTACAO_LIMITADO_PRIVADO');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_LIMITADO_PRIVADO_01')
                ->constrained('TB_ESTACAO_LIMITADO_PRIVADO', 'UUID_ESTACAO', 'FK_TB_ESTACAO_LIMITADO_PRIVADO_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO')->nullable();
            $table->string('DE_SELECAO_CANAIS')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_LIMITADO_PRIVADO');
    }
};
