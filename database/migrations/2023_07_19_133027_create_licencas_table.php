<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: Descrição == Observacao?
        // TODO: Alterar nosso_numero_tributo_tfi para id (ou uuid) do TRIBUTO na base do SIGEC

        Schema::create('TB_LICENCA', function (Blueprint $table) {
            $table->uuid('UUID_LICENCA')->primary('PK_TB_LICENCA')->comment('Identificador único da tabela');
            $table->string('DE_LICENCA')->nullable();
            $table->text('TX_OBSERVACAO')->nullable();
            $table->string('NU_LICENCA');
            $table->string('DE_NOSSO_NUMERO_TRIBUTO_TFI', 15); // TFI
            $table->date('DH_INICIO')->useCurrent();
            $table->date('DH_TERMINO')->nullable();

            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_LICENCA_01')
                ->constrained('TB_LICENCA','UUID_USUARIO_INCLUSAO','FK_TB_LICENCA_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_LICENCA_02')
                ->constrained('TB_LICENCA','UUID_USUARIO_EXCLUSAO','FK_TB_LICENCA_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_LICENCA_03')
                ->constrained('TB_LICENCA','ID_MOTIVO_EXCLUSAO','FK_TB_LICENCA_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_LICENCA');
    }
};
