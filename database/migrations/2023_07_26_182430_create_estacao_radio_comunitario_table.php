<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: campo modalidade não deve ir pra outra tabela?

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_RADIO_COMUNITARIO', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_RADIO_COMUNITARIO', 'PK_ESTACAO_RADIO_COMUNITARIO');
            $table->bigInteger('ID_ESTACAO_RADIO_COMUNITARIO');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_RADIO_COMUNITARIO_01')
                ->constrained('TB_ESTACAO_RADIO_COMUNITARIO', 'UUID_ESTACAO', 'FK_TB_ESTACAO_RADIO_COMUNITARIO_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO')->nullable();
            $table->string('DE_MODALIDADE')->nullable(); // fixa, móvel ou telecomando
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_RADIO_COMUNITARIO');
    }
};
