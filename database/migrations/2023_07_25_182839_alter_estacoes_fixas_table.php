<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::table('TB_ESTACAO_FIXA', function (Blueprint $table) {
            // DB::statement('ALTER TABLE TB_ESTACAO_FIXA ADD GF_PONTO POINT NULL');
            // DB::statement('ALTER TABLE TB_ESTACAO_FIXA ADD GF_POLIGONO_AREA POLYGON NULL');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // DB::statement('ALTER TABLE TB_ESTACAO_FIXA DROP GF_PONTO');
        // DB::statement('ALTER TABLE TB_ESTACAO_FIXA DROP GF_POLIGONO_AREA');
    }
};
