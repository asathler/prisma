<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: campo modalidade não deve ir pra outra tabela?
        // TODO: qtde_acesso duplicado com qtde_acesso_total?
        // TODO: Revisar faixas de subida e descida com PO

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_ACESSO_CONDICIONADO', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_ACESSO_CONDICIONADO', 'PK_ESTACAO_ACESSO_CONDICIONADO');
            $table->bigInteger('ID_ESTACAO_ACESSO_CONDICIONADO');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_ACESSO_CONDICIONADO_01')
                ->constrained('TB_ESTACAO_ACESSO_CONDICIONADO', 'UUID_ESTACAO', 'FK_TB_ESTACAO_ACESSO_CONDICIONADO_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('DE_FUNCAO')->nullable(); // 'tipo_funcao': local, mista, transito ou taden
            $table->integer('PC_TECNOLOGIA_ANALOGICA')->default(0);
            $table->integer('PC_TECNOLOGIA_DIGITAL')->default(0);

            $table->boolean('IC_PRESTA_SERVICO_TVC_MMDS_DTH')->default(false);
            $table->integer('NU_SERVICO_TVC_MMDS_DTH')->nullable();
            $table->boolean('IC_FREQUENCIA_FAIXA_2500')->default(false);
            // $table->integer('NU_MODO_DISTRIBUICAO')->default(false);
            $table->boolean('IC_DISTRIBUIDO_MEIO_CONFINADO')->default(false);
            $table->boolean('IC_DISTRIBUIDO_VIA_SATELITE')->default(false);
            $table->integer('QT_MAX_CANAIS')->default(0);

            // Relacionamento abrangencia?
            $table->integer('IND_CANAL_AREA_ABRANGENCIA')->default(0);
            $table->string('DE_ABRANGENCIA_TIPO')->nullable(); // nacional, estadual, municipal
            $table->string('DE_ABRANGENCIA_UF')->nullable();
            $table->string('DE_ABRANGENCIA_MUNICIPIO')->nullable();
            $table->string('DE_ABRANGENCIA_COMENTARIO')->nullable();

            // Relacionamento rede?
            $table->string('DE_TIPO_REDE')->nullable();
            $table->string('DE_FAIXA_SUBIBA_DISPONIVEL')->nullable();
            $table->string('DE_FAIXA_SUBIBA_UTILIZADA')->nullable();
            $table->string('DE_FAIXA_DESCIDA_DISPONIVEL')->nullable();
            $table->string('DE_FAIXA_DESCIDA_UTILIZADA')->nullable();
            $table->integer('QT_CANAIS_ANALOGICOS_TOTAL')->default(0);
            $table->integer('QT_CANAIS_ANALOGICOS_UTILIZADOS')->default(0);
            $table->integer('QT_CANAIS_DIGITAIS_TOTAL')->default(0);
            $table->integer('QT_CANAIS_DIGITAIS_UTILIZADOS')->default(0);
            $table->integer('QT_CANAIS_DADOS_VOZ_TOTAL')->default(0);
            $table->integer('QT_CANAIS_DADOS_VOZ_UTILIZADOS')->default(0);
            $table->integer('QT_CANAIS_DIGITAIS_6MHZ')->default(0);
            $table->string('DE_TIPO_TECNICAS_MODULACAO')->nullable(); // 8vsb, 64tam, 256qam, outras
            $table->integer('QT_QUILOMETROS_REDE_CABO_COAXIAL')->nullable();
            $table->integer('QT_QUILOMETROS_REDE_FIBRA_OTICA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_ACESSO_CONDICIONADO');
    }
};
