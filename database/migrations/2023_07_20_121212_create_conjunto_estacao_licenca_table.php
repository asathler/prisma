<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_CONJUNTO_ESTACAO_LICENCA', function (Blueprint $table) {
            //$table->id('ID_CONJUNTO_ESTACAO_LICENCA')->comment('Identificador único da tabela');
            $table->primary('ID_CONJUNTO_ESTACAO_LICENCA', 'PK_CONJUNTO_ESTACAO_LICENCA');
            $table->unsignedBigInteger('ID_CONJUNTO_ESTACAO_LICENCA');

            $table->foreignUuid('UUID_LICENCA')
                ->index('IX_TB_CONJUNTO_ESTACAO_LICENCA_01')
                ->constrained('TB_CONJUNTO_ESTACAO_LICENCA','UUID_LICENCA','FK_TB_CONJUNTO_ESTACAO_LICENCA_TB_LICENCA')
                ->references('UUID_LICENCA')->on('TB_LICENCA')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_CONJUNTO_ESTACAO')
                ->index('IX_TB_CONJUNTO_ESTACAO_LICENCA_02')
                ->constrained('TB_CONJUNTO_ESTACAO_LICENCA','ID_CONJUNTO_ESTACAO','FK_TB_CONJUNTO_ESTACAO_LICENCA_TB_CONJUNTO_ESTACO')
                ->references('ID_CONJUNTO_ESTACAO')->on('TB_CONJUNTO_ESTACAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_CONJUNTO_ESTACAO_LICENCA_03')
                ->constrained('TB_CONJUNTO_ESTACAO_LICENCA','UUID_USUARIO_INCLUSAO','FK_TB_CONJUNTO_ESTACAO_LICENCA_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_LICENCA_04')
                ->constrained('TB_CONJUNTO_ESTACAO_LICENCA','UUID_USUARIO_EXCLUSAO','FK_TB_CONJUNTO_ESTACAO_LICENCA_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVOS_EXCLUSOES')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_LICENCA_05')
                ->constrained('TB_CONJUNTO_ESTACAO_LICENCA','ID_MOTIVOS_EXCLUSOES','FK_TB_CONJUNTO_ESTACAO_LICENCA_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_CONJUNTO_ESTACAO_LICENCA');
    }
};
