<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_TIPO_ESTACOES', function (Blueprint $table) {
            $table->id('ID_TIPO_ESTACOES');
            $table->string('NO_TIPO_ESTACOES', 255)->index('IX_TB_TIPO_ESTACOES')->unique();
            $table->boolean('IC_TERRENA')->default(false);
            $table->boolean('IC_MOVEL')->default(false);
            $table->boolean('IC_PESSOAL')->default(false);
            $table->boolean('IC_MARITIMA')->default(false);
            $table->boolean('IC_AERONAUTICA')->default(false);
            $table->boolean('IC_ESPACIAL')->default(false);
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_TIPO_ESTACOES');
    }
};
