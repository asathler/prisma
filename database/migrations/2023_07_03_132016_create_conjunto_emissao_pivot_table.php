<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_CONJUNTO_EMISSAO', function (Blueprint $table) {
            $table->primary('ID_CONJUNTO_EMISSAO', 'PK_CONJUNTO_EMISSAO');
            $table->unsignedBigInteger('ID_CONJUNTO_EMISSAO');
            $table->string('NO_CONJUNTO_EMISSAO')->index('IX_TB_CONJUNTO_EMISSAO_01')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_CONJUNTO_EMISSAO');
    }
};
