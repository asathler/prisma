<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_PLANOS_BASICOS', function (Blueprint $table) {
            $table->id('ID_PLANOS_BASICOS');
            $table->foreignId('FK_TB_SERVICOS_TB_PLANOS_BASICOS')
                ->index('IX_TB_PLANOS_BASICOS_01')
                ->constrained('CC_TB_PLANOS_BASICOS_ID_USUARIOS_02')
                ->references('ID_SERVICOS')->on('TB_SERVICOS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->string('SG_UF', 2);
            $table->string('NO_MUNICIPIO', 7); // TODO: Transpor para relacionamento com TB_IBGE_MUNICIPIO
            $table->bigInteger('ID_IDTCANALIZACAO');
            $table->string('DE_PLANOS_BASICOS', 255)->index('IX_TB_PLANOS_BASICOS_02');
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_PLANOS_BASICOS');
    }
};
