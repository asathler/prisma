<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_USUARIO', function (Blueprint $table) {
            $table->uuid('UUID_USUARIO')->primary('PK_TB_USUARIO');
            $table->string('NO_USUARIO', 255);
            $table->string('ED_EMAIL', 255)->unique()->index('IX_TB_USUARIO_01');
            $table->string('CO_CPF', 255)->unique()->index('IX_TB_USUARIO_02');
            $table->date('DH_NASCIMENTO');
            $table->string('DE_SENHA', 255);
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_USUARIO');
    }
};
