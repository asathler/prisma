<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: tipo_1 // Principal, salvamento ou outros
        // TODO: tipo_2 // Telegrafia, telefonia ou rádio
        // TODO: Ver transformação do campo arrendamento de boolean para date

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_AERONAUTICA', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_AERONAUTICA', 'PK_ESTACAO_AERONAUTICA');
            $table->bigInteger('ID_ESTACAO_AERONAUTICA');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_AERONAUTICA_01')
                ->constrained('TB_ESTACAO_AERONAUTICA', 'UUID_ESTACAO', 'FK_TB_ESTACAO_AERONAUTICA_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('NO_INDICATIVO');
            $table->string('NO_CATEGORIA')->nullable();
            $table->boolean('IC_ARRENDAMENTO')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_AERONAUTICA');
    }
};
