<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: campo modalidade não deve ir pra outra tabela?
        // TODO: qtde_acesso duplicado com qtde_acesso_total?

        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_COMUTADA', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_COMUTADA', 'PK_ESTACAO_COMUTADA');
            $table->bigInteger('ID_ESTACAO_COMUTADA');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_COMUTADA_01')
                ->constrained('TB_ESTACAO_COMUTADA', 'UUID_ESTACAO', 'FK_TB_ESTACAO_COMUTADA_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->string('DE_FUNCAO')->nullable(); // 'tipo_funcao': local, mista, transito ou taden
            $table->integer('PC_TECNOLOGIA_ANALOGICA')->default(0);
            $table->integer('PC_TECNOLOGIA_DIGITAL')->default(0);

            // Ver se é uma tabela de relacionamento 1-N
            $table->integer('QT_ACESSO_PRINCIPAL')->default(0);
            $table->integer('QT_ACESSO_ELR')->default(0);
            $table->integer('QT_ACESSO_ER')->default(0);
            $table->integer('QT_ACESSO_MD')->default(0);
            $table->integer('QT_ACESSO_AO')->default(0);
            $table->integer('QT_ACESSO_VS')->default(0);
            $table->integer('QT_ACESSO_TOTAL')->default(0);

            $table->string('DE_CLASSIFICACAO_ACESSO')->default(0);
            $table->integer('QT_ACESSO')->default(0);
            $table->string('CO_TRANSITO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_COMUTADA');
    }
};
