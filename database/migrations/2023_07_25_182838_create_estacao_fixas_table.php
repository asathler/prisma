<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_FIXA', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_FIXA', 'PK_ESTACAO_FIXA');
            $table->bigInteger('ID_ESTACAO_FIXA');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_FIXA_01')
                ->constrained('TB_ESTACAO_FIXA', 'UUID_ESTACAO', 'FK_TB_ESTACAO_FIXA_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;

            $table->decimal('NU_LATITUDE', 12, 8);
            $table->decimal('NU_LONGITUDE', 12, 8);

            // TODO: Relacionamentos com ENDERECOS; Ver todos os campos OU chave estrangeira
            $table->string('ED_ENDERECO');
            // $table->string('ED_LOGRADOURO');
            // $table->string('ED_END_NUMERO');
            // $table->string('ED_BAIRRO');
            // $table->string('ED_CIDADE');
            // $table->string('ED_UF');
            // $table->string('ED_CEP');
            // $table->string('NO_PAIS');
            $table->point('GF_PONTO', 4674)->nullable();
            $table->multiPolygon('GF_POLIGONO_AREA', 4674)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_FIXA');
    }
};
