<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('TB_ESTACOES', function (Blueprint $table) {
            // Auto relacionamento!
            $table->foreignUuid('UUID_ESTACAO_REFERENCIA')
                ->nullable()
                ->index('IX_TB_ESTACAO_09')
                ->constrained('TB_ESTACAO','UUID_ESTACAO','FK_TB_ESTACAO_TB_ESTACAO_REFERENCIA')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('TB_ESTACAO', function (Blueprint $table) {
            $table->dropForeign('UUID_ESTACAO_REFERENCIA');
        });
    }
};
