<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_CONJUNTO_EQUIPAMENTO_DETALHE', function (Blueprint $table) {
            $table->primary('ID_CONJUNTO_EQUIPAMENTO_DETALHE', 'PK_TB_CONJUNTO_EQUIPAMENTO_DETALHE');
            $table->unsignedBigInteger('ID_CONJUNTO_EQUIPAMENTO_DETALHE');

            $table->foreignId('ID_CONJUNTO_EQUIPAMENTO')
                ->index('IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_01')
                ->constrained('TB_CONJUNTO_EQUIPAMENTO_DETALHE','ID_CONJUNTO_EQUIPAMENTO','FK_TB_CONJUNTO_EQUIPAMENTO_DETALHE_TB_CONJUNTO_EQUIPAMENTO')
                ->references('ID_CONJUNTO_EQUIPAMENTO')->on('TB_CONJUNTO_EQUIPAMENTO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('UUID_EQUIPAMENTO')
                ->index('IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_02')
                ->constrained('TB_CONJUNTO_EQUIPAMENTO_DETALHE','UUID_EQUIPAMENTO','FK_TB_CONJUNTO_EQUIPAMENTO_DETALHE_TB_EQUIPAMENTO')
                ->references('UUID_EQUIPAMENTO')->on('TB_EQUIPAMENTO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_03')
                ->constrained('TB_CONJUNTO_EQUIPAMENTO_DETALHE','UUID_USUARIO_INCLUSAO','FK_TB_CONJUNTO_EQUIPAMENTO_DETALHE_TB_USUARIOS_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_04')
                ->constrained('TB_CONJUNTO_EQUIPAMENTO_DETALHE','UUID_USUARIO_EXCLUSAO','FK_TB_CONJUNTO_EQUIPAMENTO_DETALHE_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_05')
                ->constrained('TB_CONJUNTO_EQUIPAMENTO_DETALHE','ID_MOTIVO_EXCLUSAO','FK_TB_CONJUNTO_EQUIPAMENTO_DETALHE_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');

            $table->unique([
                'ID_CONJUNTO_EQUIPAMENTO_DETALHE',
                'UUID_EQUIPAMENTO',
            ], 'IX_TB_CONJUNTO_EQUIPAMENTO_DETALHE_06');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_CONJUNTO_EQUIPAMENTO_DETALHE');
    }
};
