<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_TIPO_MOVIMENTACOES', function (Blueprint $table) {
            $table->id('ID_TIPO_MOVIMENTACOES');
            $table->string('CO_TIPO_MOVIMENTACOES', 10);
            $table->string('DE_TIPO_MOVIMENTACOES', 255);
            $table->integer('NU_SEQUENCIA');
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_TIPO_MOVIMENTACOES');
    }
};
