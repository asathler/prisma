<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TB_SERVICOS', function (Blueprint $table) {
            // $table->id()->comment('Identificador da tabela'); // Antigo campo: NumServico
            $table->unsignedBigInteger('ID_SERVICOS')->primary('PK_SERVICOS')->comment('Identificador da tabela'); // Antigo campo: NumServico

            // $table->string('numero', 3)->primary()->comment('Identificador da tabela'); // ->index()->unique(); // ID!
            $table->string('DE_SERVICOS', 255);
            $table->foreignUuid('FK_TB_USUARIOS_TB_SERVICOS_INCLUSAO')
                ->index('IX_TB_SERVICOS_01')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_01')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('FK_TB_USUARIOS_TB_SERVICOS_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_SERVICOS_02')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_02')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_MOTIVOS_EXCLUSOES_TB_SERVICOS')
                ->nullable()
                ->index('IX_TB_ENTIDADES_03')
                ->constrained('CC_TB_MOTIVOS_EXCLUSOES_ID_MOTIVOS_EXCLUSOES_03')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_SERVICOS');
    }
};
