<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // // // Aqui jaz a tabela_20 💀!

        Schema::create('TB_CONJUNTO_ESTACAO', function (Blueprint $table) {
            $table->primary('ID_CONJUNTO_ESTACAO', 'PK_CONJUNTO_ESTACAO');
            $table->unsignedBigInteger('ID_CONJUNTO_ESTACAO');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_CONJUNTO_ESTACAO_01')
                ->constrained('TB_CONJUNTO_ESTACAO','UUID_ESTACAO','FK_TB_CONJUNTO_ESTACAO_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_CONJUNTO_EQUIPAMENTO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_02')
                ->constrained('TB_CONJUNTO_ESTACAO','ID_CONJUNTO_EQUIPAMENTO','FK_TB_CONJUNTO_ESTACAO_TB_CONJUNTO_EQUIPAMENTO')
                ->references('ID_CONJUNTO_EQUIPAMENTO')->on('TB_CONJUNTO_EQUIPAMENTO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_CONJUNTO_EMISSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_03')
                ->constrained('TB_CONJUNTO_ESTACAO','ID_CONJUNTO_EMISSAO','FK_TB_CONJUNTO_EMISSAO_TB_CONJUNTO_EMISSAO')
                ->references('ID_CONJUNTO_EMISSAO')->on('TB_CONJUNTO_EMISSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_CONJUNTO_ESTACAO_04')
                ->constrained('TB_CONJUNTO_ESTACAO','UUID_USUARIO_INCLUSAO','FK_TB_CONJUNTO_ESTACAO_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUDI_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_05')
                ->constrained('TB_CONJUNTO_ESTACAO','UUDI_USUARIO_EXCLUSAO','FK_TB_CONJUNTO_ESTACAO_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_CONJUNTO_ESTACAO_06')
                ->constrained('TB_CONJUNTO_ESTACAO','ID_MOTIVO_EXCLUSAO','FK_TB_CONJUNTO_ESTACAO_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');

            $table->unique([
                'UUID_ESTACAO',
                'ID_CONJUNTO_EQUIPAMENTO',
                'ID_CONJUNTO_EMISSAO',
            ], 'IX_TB_CONJUNTO_ESTACAO_07');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_CONJUNTO_ESTACAO');
    }
};
