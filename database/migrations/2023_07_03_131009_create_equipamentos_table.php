<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: meia potência não é campo calculado??

        Schema::create('TB_EQUIPAMENTO', function (Blueprint $table) {
            $table->uuid('UUID_EQUIPAMENTO')->primary('PK_TB_EQUIPAMENTOS')->comment('Identificador único da tabela');
            $table->text('DE_EQUIPAMENTO')->nullable();
            $table->string('NO_FABRICANTE')->nullable();
            $table->string('NO_MODELO')->nullable();
            $table->string('TX_NUMERO_SERIE')->nullable();
            $table->string('TX_TIPO')->nullable();
            $table->string('TX_TECNOLOGIA')->nullable();
            $table->string('TX_CLASSE')->nullable();
            $table->string('TX_SETOR')->nullable();

            $table->string('TX_POTENCIA_UNIDADE_MEDIDA')->default(0);
            // $table->integer('potencia_erp')->default(0);
            // TODO: Ajustar campos para aceitar ponto
            $table->decimal('NU_POTENCIA')->default(0);
            $table->integer('NU_MEIA_POTENCIA')->default(0);
            $table->integer('NU_MAIOR_POTENCIA_RECEPCAO')->default(0);
            $table->integer('NU_GANHO')->default(0);
            $table->integer('NU_PERDA')->default(0);
            $table->integer('NU_PERDA_ADICIONAL')->default(0);
            $table->integer('NU_TILT_ELETRICO')->default(0);
            $table->integer('NU_TILT_MECANICO')->default(0);
            $table->integer('NU_INTENSIDADE')->default(0);
            $table->integer('NU_IMPEDANCIA')->default(0);
            $table->integer('NU_ATENUACAO')->default(0);
            $table->integer('NU_ALTURA')->default(0);
            $table->integer('NU_COMPRIMENTO')->default(0);
            $table->integer('NU_DIAMETRO')->default(0);
            $table->integer('NU_EFICIENCIA')->default(0);
            $table->integer('NU_MODULACAO')->default(0);
            $table->integer('NU_FEC')->default(0);
            $table->integer('NU_AZIMUTE')->default(0);
            $table->integer('NU_ANGULO')->default(0);
            $table->integer('NU_RAIO')->default(0);
            $table->integer('NU_RADIAL')->default(0);
            $table->integer('NU_FRENTE_COSTAS')->default(0);
            $table->integer('NU_POLARIZACAO')->default(0);
            $table->integer('NU_TAXA_INFORMACAO')->default(0);
            $table->integer('NU_TEMPERATURA_RUIDO')->default(0);
            $table->integer('NU_RELACAO_PORTADORA_RUIDO')->default(0);
            $table->integer('NU_NMT')->default(0);
            $table->integer('NU_HS_NMT')->default(0);

            $table->boolean('IC_TRANSMISSAO_SIMULTANEA')->default(0);
            $table->boolean('IC_RECEPCAO_SIMULTANEA')->default(0);

            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_EQUIPAMENTO_01')
                ->constrained('TB_EQUIPAMENTO', 'UUID_USUARIO_INCLUSAO', 'FK_TB_EQUIPAMENTO_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_EQUIPAMENTO_02')
                ->constrained('TB_EQUIPAMENTO', 'UUID_USUARIO_EXCLUSAO', 'FK_TB_EQUIPAMENTO_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_EQUIPAMENTO_03')
                ->constrained('TB_EQUIPAMENTO', 'ID_MOTIVO_EXCLUSAO', 'FK_TB_EQUIPAMENTO_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_EQUIPAMENTO');
    }
};
