<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        /*
         * INSERT INTO
         *     tffs
         * SELECT letra_debito_tfi, count(letra_debito_tfi) qtde FROM estacoes group by fistel, letra_debito_tfi
         * UNION
         * SELECT letra_debito_tfi, count(letra_debito_tfi) qtde FROM estacoes group by fistel, letra_debito_tfi
         */

        Schema::create('TB_TFF', function (Blueprint $table) {
            //$table->id('ID_TFF');
            $table->primary('ID_TFF', 'PK_TFF');
            $table->unsignedBigInteger('ID_TFF');
            $table->unsignedInteger('NU_ANO');
            $table->foreignUuid('UUID_OUTORGA')
                ->index('IX_TB_TFF_01')
                ->constrained('TB_TFF', 'UUID_OUTORGA', 'FK_TB_TFF_TB_OUTORGA')
                ->references('UUID_OUTORGAS')->on('TB_OUTORGAS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->unsignedBigInteger('QT_TOTAL_ESTACAO')->default(0);
            $table->unsignedBigInteger('QT_TIPO_A')->default(0);
            $table->unsignedBigInteger('QT_TIPO_B')->default(0);
            $table->unsignedBigInteger('QT_TIPO_C')->default(0);
            $table->unsignedBigInteger('QT_TIPO_D')->default(0);
            $table->unsignedBigInteger('QT_TIPO_E')->default(0);
            $table->unsignedBigInteger('QT_TIPO_F')->default(0);
            $table->unsignedBigInteger('QT_TIPO_G')->default(0);
            $table->unsignedBigInteger('QT_TIPO_H')->default(0);
            $table->unsignedBigInteger('QT_TIPO_I')->default(0);
            $table->unsignedBigInteger('QT_TIPO_J')->default(0);
            $table->unsignedBigInteger('QT_TIPO_L')->default(0);
            $table->unsignedBigInteger('QT_TIPO_M')->default(0);
            $table->unsignedBigInteger('QT_TIPO_N')->default(0);
            $table->unsignedBigInteger('QT_TIPO_O')->default(0);
            $table->unsignedBigInteger('QT_TIPO_P')->default(0);
            $table->unsignedBigInteger('QT_TIPO_Q')->default(0);
            $table->unsignedBigInteger('QT_TIPO_R')->default(0);
            $table->boolean('IC_GERADA')->default(false);
            $table->unsignedBigInteger('NU_SEQUENCIAL')->default(0);
            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_TFF_02')
                ->constrained('TB_TFF','UUID_USUARIO_INCLUSAO','FK_TB_TFF_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_TFF_03')
                ->constrained('TB_TFF','UUID_USUARIO_EXCLUSAO','FK_TB_TFF_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_TFF_04')
                ->constrained('TB_TFF','ID_MOTIVO_EXCLUSAO','FK_TB_TFF_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_TFF');
    }
};
