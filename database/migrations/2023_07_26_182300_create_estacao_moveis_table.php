<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Versão revisada aplicando os padrões de nomenclatura da GIIB
        Schema::create('TB_ESTACAO_MOVEL', function (Blueprint $table) {
            $table->primary('ID_ESTACAO_MOVEL', 'PK_ESTACAO_MOVEL');
            $table->bigInteger('ID_ESTACAO_MOVEL');

            $table->foreignUuid('UUID_ESTACAO')
                ->index('IX_TB_ESTACAO_MOVEL_01')
                ->constrained('TB_ESTACAO_MOVEL', 'UUID_ESTACAO', 'FK_TB_ESTACAO_MOVEL_TB_ESTACAO')
                ->references('UUID_ESTACOES')->on('TB_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACAO_MOVEL');
    }
};
