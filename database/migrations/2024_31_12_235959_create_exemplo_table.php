<?php

use Illuminate\Database\Migrations\Migration;
// use Illuminate\Database\Schema\Blueprint;
// use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        /*
        Exemplos:
        Tabelas em nome singular!!

        Tabela pai:
        TB_ACAO
        - ID_ACAO (campo)
          - PK_ACAO (constraint)

        Tabela filha:
        TB_TIPO_ACAO
        - ID_TIPO_ACAO (campo)
          - PK_TIPO_ACAO (constraint)
        - ID_ACAO (campo)
          - FK_TB_TIPO_ACAO_TB_ACAO (constraint)
        - STATUS [0, 1]


        FK:
        FK_TB_TIPO_ACAO_TB_ACAO

        ...e/ou com qualificador da relação:
        FK_TB_ENTIDADES_TB_USUARIOS_INCLUSAO

        Schema::create('TB_AAAAAAAAAA', function (Blueprint $table) {
            // Comentário da tabela - NÃO EFETIVO PARA SQL Server
            $table->comment('Esta tabela é para ...');

            // Cada anterior id ou uuid irá se tornar:
            $table->primary('ID_LALALA', 'PK_LALALA');

            $table->bigInteger('ID_LALALA');

            // Exemplos de IX, Unique e comentário de campo
            $table->string('DE_LALALA')
                ->index('IX_LALALA')
                ->unique('NOME_DA_UNIQUE_CONSTRAINT')
                ->comment('Comentário da lalala')
            ;

            // Exemplo de criação de FK
            // $table->foreignUuid('UUID_TIPO_ACAO')
            //     ->index('IX_TIPO_ACAO')
            //     ->constrained('FK_TB_TIPO_ACAO_TB_ACAO')
            //     ->references('ID_ACAO')->on('ID_ACAO')->nullable()
            // ;

            // Exemplos de tipos de campos
            // $table->uuid('UUID_LALALA')->nullable();
            $table->string('DE_SENHA')->nullable();
            $table->string('DS_PATH_FOTO_ESTACAO')->nullable(); // c:\storage\estacoes\estacao
            $table->string('IC_STATUS')->nullable();

            $table->json('JSON_CAMPO')->nullable();
            // JSON: {"DE_LALALA": "valor"}
            // Anteriores XML: <xml><DE_LALALA>valor</DE_LALALA></xml>

            // Exemplos de tipos de campos georrefenciais
            $table->point('POINT_LALALA')->nullable();
            $table->multipoint('MULTIPOINT_LALALA')->nullable();
            $table->polygon('POLYGON_LALALA')->nullable();
            $table->multipolygon('MULTIPOLYGON_LALALA')->nullable();
            $table->multipolygon('GF_MP_LALALA')->nullable();
            // $table->geometry('GE_LALALA')->nullable();
        });
        */
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Schema::dropIfExists('TB_AAAAAAAAAA');
    }
};
