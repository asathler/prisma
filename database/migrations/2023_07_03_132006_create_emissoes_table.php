<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: Incluir BANDA da frequência
        // @see: https://www.whcengenharia.com.br/post/entenda-as-faixas-de-frequ%C3%AAncia-e-suas-aplica%C3%A7%C3%B5es
        // TODO: Talvez precise tipificar a emissão, como por exemplo em IndWll
        // TODO: LINK com AtoRF
        // TODO: Serializar, ou não, os valores em outras unidades de medidas

        Schema::create('TB_EMISSAO', function (Blueprint $table) {
            //$table->id('ID_EMISSAO');
            $table->primary('ID_EMISSAO', 'PK_EMISSAO');
            $table->unsignedBigInteger('ID_EMISSAO');
            $table->string('NO_SUBFAIXA')->nullable();
            $table->decimal('NU_FREQUENCIA_INICIO_TX', 13, 8);
            $table->decimal('NU_FREQUENCIA_TERMINO_TX', 13, 8);
            $table->decimal('NU_FREQUENCIA_INICIO_RX', 13, 8);
            $table->decimal('NU_FREQUENCIA_TERMINO_RX', 13, 8);
            $table->boolean('IC_SUBIDA')->default(false); // SUBIDA = TRUE; DESCIDA = FALSE
            $table->string('DE_POLARIZACAO')->nullable();
            // $table->string('banda')->nullable();

            // Ato RF
            $table->string('DE_ATO_RF')->index('IX_TB_EMISSAO_01')->nullable();
            $table->string('DH_ATO_RF')->index('IX_TB_EMISSAO_02')->nullable();

            $table->foreignUuid('UUID_USUARIO_INCLUSAO')
                ->index('IX_TB_EMISSAO_03')
                ->constrained('TB_EMISSAO','UUID_USUARIO_INCLUSAO','FK_TB_EMISSAO_TB_USUARIO_INCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('UUID_USUARIO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_EMISSAO_04')
                ->constrained('TB_EMISSAO','UUID_USUARIO_EXCLUSAO','FK_TB_EMISSAO_TB_USUARIO_EXCLUSAO')
                ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('ID_MOTIVO_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_EMISSAO_05')
                ->constrained('TB_EMISSAO','ID_MOTIVO_EXCLUSAO','FK_TB_EMISSAO_TB_MOTIVO_EXCLUSAO')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_EMISSAO');
    }
};
