<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // TODO: Perguntar ao PO:
        // TODO: Verificar se há caso de outorga de um serviço mas de outra estação de outro serviço.
        // TODO: CAMPO numero, NumEstacao, DEVERIA ser removido em favor da PK!!!
        // TODO: altura Mantem nulo ou altera para default()??
        // TODO: Ver sequencial do indicativo - pra mim não precisa, se o campo nome_indicativo for unique()
        // TODO: letra_debito_tfi porque ALTERAR esse campo conforme Tipo_Estacao_Id, durante processamento da TFF?
        // TODO: letra_debito_tfi porque ALTERAR esse campos conforme quantitativo populacional de município da estação?
        // TODO: letra_debito_tfi porque ALTERAR esse campos conforme potencia de operação?
        // TODO: tipo_estacoes PARECE ter subtipos todos incluídos na mesma tabela... SEPARAR?

        Schema::create('TB_ESTACOES', function (Blueprint $table) {
            $table->uuid('UUID_ESTACOES')->primary('PK_ESTACOES')->comment('Identificador da tabela');
            $table->string('NO_ESTACOES')->nullable();
            $table->text('DE_ESTACOES')->nullable();
            $table->integer('NU_ESTACOES')->index('IX_TB_ESTACOES_01');
            $table->string('SG_ESTACOES')->nullable();
            $table->string('DE_FOTO_ESTACOES')->nullable();
            $table->string('CO_LETRA_DEBITO_TFI')->index('IX_TB_ESTACOES_02');
            $table->decimal('VR_ALTURA', 6, 2)->nullable();
            $table->string('CO_CLASSE')->nullable();

            // TODO: Conferir campos abaixo
            $table->boolean('IC_CONFIDENCIAL')->default(false);
            $table->boolean('IC_ILHA_OCEANICA')->default(false);
            $table->boolean('IC_COMUNICACAO_SATELITE')->default(false);

            // Relacionamento 0-1 responsavel_estacao
            $table->string('NO_RESPONSAVEL')->nullable();
            $table->string('ED_RESPONSAVEL_TELEFONE')->nullable();
            $table->string('ED_RESPONSAVEL_EMAIL')->nullable();
            $table->string('CO_RESPONSAVEL_CPF')->nullable();
            $table->string('CO_RESPONSAVEL_CREA')->nullable();
            $table->string('ED_RESPONSAVEL_ENDERECO')->nullable();

            $table->foreignId('FK_TB_TIPO_ESTACOES_TB_ESTACOES')
                ->index('IX_TB_ESTACOES_03')
                ->constrained('CC_TB_TIPO_ESTACOES_ID_TIPO_ESTACOES')
                ->references('ID_TIPO_ESTACOES')->on('TB_TIPO_ESTACOES')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_TB_STATUS_TB_ESTACOES')
                ->index('IX_TB_ESTACOES_04')
                ->constrained('CC_TB_STATUS_ID_STATUS')
                ->references('ID_STATUS')->on('TB_STATUS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('FK_TB_OUTORGA_TB_ESTACOES')
                ->index('IX_TB_ESTACOES_05')
                ->constrained('CC_TB_OUTORGA_ID_OUTORGA')
                ->references('UUID_OUTORGAS')->on('TB_OUTORGAS')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignUuid('FK_TB_USUARIOS_TB_ESTACOES_INCLUSAO')
                ->index('IX_TB_ESTACOES_06')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_01')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->timestamp('DH_INCLUSAO')->useCurrent();
            $table->foreignUuid('FK_TB_USUARIOS_TB_ESTACOES_EXCLUSAO')
                ->nullable()
                ->index('IX_TB_ESTACOES_07')
                ->constrained('CC_TB_USUARIOS_ID_USUARIOS_02')
                 ->references('UUID_USUARIO')->on('TB_USUARIO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->foreignId('FK_MOTIVOS_EXCLUSOES_TB_ESTACOES')
                ->nullable()
                ->index('IX_TB_ESTACOES_08')
                ->constrained('CC_TB_MOTIVOS_EXCLUSOES_ID_MOTIVOS_EXCLUSOES_03')
                ->references('ID_MOTIVO_EXCLUSAO')->on('TB_MOTIVO_EXCLUSAO')
                // ->cascadeOnUpdate()
                // ->restrictOnDelete()
            ;
            $table->softDeletes('DH_EXCLUSAO');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TB_ESTACOES');
    }
};
