<?php

namespace Database\Seeders;

use App\Models\Estacao;
use Illuminate\Database\Seeder;

class Estacao3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*
        $qtdeRepeticoes = 5;
        $qtdeRegistrosEmLote = 1000;

        $numeroFormatado = $this->numeroFormatado($qtdeRepeticoes * $qtdeRegistrosEmLote);

        $this->mostra("Gerando $numeroFormatado de registros!");

        // Gerando...
        $estacoes = [];
        for ($i = 1; $i <= ($qtdeRepeticoes * $qtdeRegistrosEmLote); $i++) {
            $estacoes[] = Estacao::factory()->make()->toArray();

            if ($i % 100 == 0) {
                $numeroFormatado = $this->numeroFormatado($i);

                $this->mostra('Gerando ' . $numeroFormatado . ' registros...');
            }
        }

        // Gravando...
        $i = 0;
        foreach (array_chunk($estacoes, $qtdeRegistrosEmLote) as $dados) {
            Estacao::insert($dados);

            $numeroFormatado = $this->numeroFormatado(++$i * $qtdeRegistrosEmLote);

            $this->mostra('Gravando ' . $numeroFormatado . ' registros...');
        }
        */
        $this->mostra('TESTE: A geração de 5.000 registros neste processo durava, em média, 18 segundos.');
    }

    private function mostra($texto)
    {
        echo '  ' . $texto . PHP_EOL;
    }

    private function numeroFormatado($numero)
    {
        return number_format($numero, 0, '', '.');
    }
}
