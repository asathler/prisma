<?php

namespace Database\Seeders;

use Database\Seeders\Validacao\DatabaseSeederServico099;
use Database\Seeders\Validacao\EstacaoComunicacaoMultimidiaBrasnet;
use Database\Seeders\Validacao\EstacaoEspacialBrasilsat;
use Database\Seeders\Validacao\EstacaoEspacialStarOne;
use Database\Seeders\Validacao\EstacaoFixaMovelAeronautica;
use Database\Seeders\Validacao\EstacaoMA_PSFMD;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            // Usuários
            UsuarioSeeder::class,

            // Auxiliares
            MotivoExclusaoSeeder::class,
            // PlanoBasicoSeeder::class,
            StatusSeeder::class,
            TipoMovimentacaoSeeder::class,
            TipoEstacaoSeeder::class,

            EstacoesCandidatasAosTestesSeeder::class,
            EstacaoFixaMovelAeronautica::class,
            DatabaseSeederServico099::class,

            EstacaoEspacialStarOne::class,
            EstacaoEspacialBrasilsat::class,

            EstacaoMA_PSFMD::class,
            EstacaoComunicacaoMultimidiaBrasnet::class,
        ]);
    }

    public function default_run(): void
    {
        $this->call([
            // Usuários
            UsuarioSeeder::class,

            // Auxiliares
            MotivoExclusaoSeeder::class,
            PlanoBasicoSeeder::class,
            StatusSeeder::class,
            TipoMovimentacaoSeeder::class,
            TipoEstacaoSeeder::class,

            // Conferir
            EntidadeSeeder::class,
            ServicoSeeder::class,
            OutorgaSeeder::class,

            // Estação
            //            EstacaoSeeder::class,
            Validacao\EstacaoFixaMovelAeronautica::class,
            Validacao\EstacaoMaritima::class,

            /*
            Estacao1Seeder::class,
            Estacao2Seeder::class,
            Estacao3Seeder::class,
            Estacao4Seeder::class,
            Estacao5Seeder::class,
            */

            // Principais negócios
            EstacaoBlocoSeeder::class,
            // EstacaoFixaSeeder::class,
            EquipamentoSeeder::class,
            EmissaoSeeder::class,
            LicencaSeeder::class,
            TffSeeder::class,

            // Relacionamentos
            ConjuntoEmissaoSeeder::class,
            ConjuntoEmissaoDetalheSeeder::class,
            ConjuntoEquipamentoSeeder::class,
            ConjuntoEquipamentoDetalheSeeder::class,
            ConjuntoEstacaoSeeder::class,
        ]);
    }
}
