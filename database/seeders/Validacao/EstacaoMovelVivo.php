<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoPessoal;
use App\Models\Servico;

class EstacaoMovelVivo extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 010 - Móvel
        // Estação 690491980 - Vivo/Telefônica

        $servico = Servico::find(10);
        $entidade = $this->entidade('Vivo');
        $outorga = $this->outorga($servico, $entidade, 'Outorga (teste) da Vivo', null, '50441011179');

        $equipamentos = $this->equipamentos(2);
        $emissoes = $this->emissoes(2);

        $estacao = $this->estacao($outorga, 'móvel pessoal', '690491980', 'M');

        EstacaoPessoal::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'DE_TECNOLOGIA' => '5G'
        ]);

        // TODO: Incluir especialização para estacao_fixa (LAT-LONG, etc...)

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca1 = $this->licenca(
            '010-2', // 108/2024-PR',
            '504410111790001',
            '2005-01-01',
            '2015-12-31'
        );

        $licenca2 = $this->licenca(
            '010-2', // 108/2024-PR',
            '504410111790002',
            '2016-01-01',
            '2030-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca1, $conjuntoEstacao, $conjuntoEstacao);
        $this->conjuntoEstacaoLicenca($licenca2, $conjuntoEstacao, $conjuntoEstacao);
    }
}
