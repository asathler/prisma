<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoBloco as EstacoesEmBlocoModel;
use App\Models\Outorga;
use Illuminate\Support\Str;

class EstacoesEmBloco extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $outorga = Outorga::first('UUID_OUTORGAS');
        $i = 0;
        $CO_REFERENCIA = '2024-01';
        $letraDebitoTfi = 'C';

        /*
        for($x = 1; $x <= 12; $x++) {
            EstacoesEmBlocoModel::factory()->create([
                'UUID_OUTORGAS' => $outorga,
                'CO_SEQUENCIAL' => $i++,
                'CO_LETRA_DEBITO_TFI' => $letraDebitoTfi,
                'CO_REFERENCIA' => '2024-' . Str::padLeft($x, 2, '0')
            ]);
        }
        */

        // 224-010
        EstacoesEmBlocoModel::factory()->create([
            'FK_TB_OUTORGA_TB_ESTACAO_BLOCOS' => $outorga,
            'CO_SEQUENCIAL' => $i++,
            'CO_LETRA_DEBITO_TFI' => $letraDebitoTfi,
            'CO_REFERENCIA' => '2024-01',

            'NU_HABILITADAS' => 499822,
            'NU_DESABILITADAS' => 0,
            'NU_REABILITADAS' => 0,
            'NU_SALDO' => 499822,

            'NU_LICENCIADA' => 499822,
            'NU_CREDITO' => 0,
            'NU_CREDITO_CANCELADO' => 0
        ]);

        // 2024-02
        EstacoesEmBlocoModel::factory()->create([
            'FK_TB_OUTORGA_TB_ESTACAO_BLOCOS' => $outorga,
            'CO_SEQUENCIAL' => $i++,
            'CO_LETRA_DEBITO_TFI' => $letraDebitoTfi,
            'CO_REFERENCIA' => '2024-02',

            'NU_HABILITADAS' => 4901,
            'NU_DESABILITADAS' => 9976,
            'NU_REABILITADAS' => 0,
            'NU_SALDO' => -5075,

            'NU_LICENCIADA' => 499822 - 5075,
            'NU_CREDITO' => 5075,
            'NU_CREDITO_CANCELADO' => 0
        ]);

        // 2024-03
        EstacoesEmBlocoModel::factory()->create([
            'FK_TB_OUTORGA_TB_ESTACAO_BLOCOS' => $outorga,
            'CO_SEQUENCIAL' => $i++,
            'CO_LETRA_DEBITO_TFI' => $letraDebitoTfi,
            'CO_REFERENCIA' => '2024-03',

            'NU_HABILITADAS' => 5237,
            'NU_DESABILITADAS' => 7112,
            'NU_REABILITADAS' => 0,
            'NU_SALDO' => -1875,

            'NU_LICENCIADA' => 499822 - 5075 - 1875,
            'NU_CREDITO' => 5075 + 1875,
            'NU_CREDITO_CANCELADO' => 0
        ]);

        // 2024-04
        EstacoesEmBlocoModel::factory()->create([
            'FK_TB_OUTORGA_TB_ESTACAO_BLOCOS' => $outorga,
            'CO_SEQUENCIAL' => $i++,
            'CO_LETRA_DEBITO_TFI' => $letraDebitoTfi,
            'CO_REFERENCIA' => '2024-04',

            'NU_HABILITADAS' => 5819,
            'NU_DESABILITADAS' => 0,
            'NU_REABILITADAS' => 0,
            'NU_SALDO' => 5819,

            'NU_LICENCIADA' => 499822 - 5075 - 1875 + 5819,
            'NU_CREDITO' => 5075 + 1875 - 5819,
            'NU_CREDITO_CANCELADO' => 0
        ]);
    }
}
