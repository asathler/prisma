<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEstacao;
use App\Models\ConjuntoEstacaoLicenca;
use App\Models\Emissao;
use App\Models\Entidade;
use App\Models\Estacao;
use App\Models\EstacaoEspacial;
use App\Models\Licenca;
use App\Models\Outorga;
use App\Models\Servico;
use App\Models\Status;
use App\Models\TipoEstacao;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;


class EstacaoEspacialBrasilsat extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        try {
            $tipoEstacao = TipoEstacao::factory()->create([
                'NO_TIPO_ESTACOES' => 'Geoestacionário',
                'IC_TERRENA' => false
            ]);
        } catch (QueryException $e) {
            $tipoEstacao = TipoEstacao::where('NO_TIPO_ESTACOES', 'Geoestacionário')
                ->firstOrFail();
        }

        $status = Status::factory()->create([
            'DE_STATUS' => 'Licenciada'
        ]);

        try {
            $servico = Servico::factory()->create([
                'ID_SERVICOS' => 185,
                'DE_SERVICOS' => '185 - EXPLORACAO DE SATELITE E ESTACOES DE ACESSO'
            ]);
        } catch (QueryException $e) {
            $servico = Servico::where('ID_SERVICOS', 185)->firstOrFail();
        }

        $entidade = Entidade::factory()->create([
            'DE_ENTIDADE' => 'EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.'
        ]);

        try {
            $outorga = Outorga::factory()->create([
                'DE_OUTORGAS' => 'Outorga de Licenciamento',
                'NU_FISTEL' => '50419331034',
                'FK_TB_SERVICO_TB_OUTORGA' => $servico->ID_SERVICOS,
                'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade->UUID_ENTIDADE,
            ]);
        } catch (QueryException $e) {
            $outorga = Outorga::where('NU_FISTEL', '50419331034')->firstOrFail();
        }
        // TODO: NU_ESTACAO TEM QUE SER UNICO?
        $estacao = Estacao::factory()->create([
            'NO_ESTACOES' => 'BRASILSAT B4',
            'DE_ESTACOES' => '',
            'NU_ESTACOES' => 443173133,
            'SG_ESTACOES' => 'BB4',
            'DE_FOTO_ESTACOES' => '',
            'VR_ALTURA' => null,
            'CO_CLASSE' => '',
            'IC_CONFIDENCIAL' => false,
            'IC_ILHA_OCEANICA' => false,
            'IC_COMUNICACAO_SATELITE' => false,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => $tipoEstacao->ID_TIPO_ESTACOES,
            'FK_TB_STATUS_TB_ESTACOES' => $status->ID_STATUS,
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga->UUID_OUTORGAS,
        ]);

        //TODO: Verificar campo NU_CLASSE pode ser null.
        //TODO: Verificar DT_DIREITO_EXPLORACAO/Validade Especial.
        //TODO: Representante legal é relacionamento com Entidade.
        //TODO: DE_POSICAO_ORBITAL null?
        //TODO: O que é DE_FAIXA_ENTRADA, CO_CONCATENADO, VR_CODIGO_CONCATENADO?
        //TODO: DE_RESTRICAO é informação da canalização.
        //TODO: Verificar se campo ED_UF_COBERTURA deveria estar na emissão
        EstacaoEspacial::factory()->create([
            'NO_OPERADOR' => 'CLARO S.A.',
            'NO_UIT' => 'SBTS B4',
            'NU_CLASSE' => 0,
            'DT_DIREITO_EXPLORACAO' => '2022-11-13 00:00:00.000',
            'NU_LONGITUTE_ORBITAL' => 92,
            'IC_MERIDIANO' => 'W',
            'DE_POSICAO_ORBITAL' => 'Norte',
            'DE_FAIXA_ENTRADA' => 'o que e?',
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S10A',
            'NU_FREQUENCIA_INICIO_TX' => 6287,
            'NU_FREQUENCIA_TERMINO_TX' => 6323,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S10B',
            'NU_FREQUENCIA_INICIO_TX' => 6307,
            'NU_FREQUENCIA_TERMINO_TX' => 6343,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S11A',
            'NU_FREQUENCIA_INICIO_TX' => 6327,
            'NU_FREQUENCIA_TERMINO_TX' => 6363,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S11B',
            'NU_FREQUENCIA_INICIO_TX' => 6347,
            'NU_FREQUENCIA_TERMINO_TX' => 6383,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D10A',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 4062,
            'NU_FREQUENCIA_TERMINO_RX' => 4098,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D10B',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 4082,
            'NU_FREQUENCIA_TERMINO_RX' => 4200,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V'
        ]);
        
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D11A',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 4102,
            'NU_FREQUENCIA_TERMINO_RX' => 4138,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D11B',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 4122,
            'NU_FREQUENCIA_TERMINO_RX' => 4158,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V'
        ]);

        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }

        $conjuntoEstacao = ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES,
            'ID_CONJUNTO_EQUIPAMENTO' => null,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO
        ]);

        // TODO: NU_LICENCA/DATAS UNICAS?
        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000005/2020-RJ',
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504164931650027',
            'DH_INICIO' => '2020-10-21 15:44:01.180',
            'DH_TERMINO' => '2022-11-13 00:00:00.000'
        ]);

        ConjuntoEstacaoLicenca::factory()->create([
            'UUID_LICENCA' => $licenca->UUID_LICENCA,
            'ID_CONJUNTO_ESTACAO' => $conjuntoEstacao->ID_CONJUNTO_ESTACAO
        ]);

        echo "NumFistel: 50419331034\n";
        echo "NumEstacao: 443173133\n";

        //-----------------


    }


}
