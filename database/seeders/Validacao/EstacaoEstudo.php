<?php

namespace Database\Seeders\Validacao;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EstacaoEstudo extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     * @deprecated Estação de estudo, aparentemente, com a inclusão de histórico de licenças, não é mais necessária!
     */
    public function run(): void
    {
        // Estação de estudo:
        // Serviço: 213 - Ondas curtas
        // IdtEstacao: 41320 e 41321
        // Indicativo: ZYE727
    }
}
