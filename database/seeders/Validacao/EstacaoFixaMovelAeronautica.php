<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\Emissao;
use App\Models\Equipamento;
use App\Models\EstacaoFixa;
use App\Models\Licenca;
use App\Models\Servico;

class EstacaoFixaMovelAeronautica extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        echo '  Estação Fixa - Móvel Aeronáutico' . PHP_EOL;
        echo '  IdtEstacao = 3653842 - NumEstacao=1016356495' . PHP_EOL . PHP_EOL;
        //  IdtEstacao = 3653842	NumEstacao=1016356495

        $entidade = $this->entidade('SUBSEA7 DO BRASIL SERVICOS LTDA');
        $outorga = $this->outorga(Servico::find(507), $entidade, 'Estacao Fixa - MovelAeronautica', null, '50414855345');

        $estacao = $this->estacao($outorga, 'SEVEN MERLIN-VHF PORT', '1016356495', 'A');

        EstacaoFixa::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ED_ENDERECO' => 'PLATAFORMA P-62 - nº s/n',
            'NU_LATITUDE' => -25.4341666666666666,
            'NU_LONGITUDE' => -44.0016666666666666,
            'ED_ENDERECO' => 'SEVEN MERLIN-VHF - nº PORT'
        ]);

        $equipamentos[] = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '015950701922 - 18678',
            'NO_FABRICANTE' => 'Icom America Inc.',
            'NO_MODELO' => 'IC-A6',
            'TX_NUMERO_SERIE' => '',
            'TX_TIPO' => '',
            'TX_TECNOLOGIA' => '',
            'TX_CLASSE' => '',
            'TX_SETOR' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 0,
            'NU_POTENCIA' => 5.000,
            'NU_MEIA_POTENCIA' => 0,
            'NU_MAIOR_POTENCIA_RECEPCAO' => '',
            'NU_GANHO' => 0,
            'NU_PERDA' => '',
            'NU_PERDA_ADICIONAL' => '',
            'NU_TILT_ELETRICO' => '',
            'NU_TILT_MECANICO' => '',
            'NU_INTENSIDADE' => '',
            'NU_IMPEDANCIA' => '',
            'NU_ATENUACAO' => '',
            'NU_ALTURA' => 12,
            'NU_COMPRIMENTO' => '',
            'NU_DIAMETRO' => '',
            'NU_EFICIENCIA' => '',
            'NU_MODULACAO' => '',
            'NU_FEC' => '',
            'NU_AZIMUTE' => 0,
            'NU_ANGULO' => 0,
            'NU_RAIO' => 0,
            'NU_RADIAL' => '',
            'NU_FRENTE_COSTAS' => 0,
            'NU_POLARIZACAO' => 2,
            'NU_TAXA_INFORMACAO' => '',
            'NU_TEMPERATURA_RUIDO' => '',
            'NU_RELACAO_PORTADORA_RUIDO' => '',
            'NU_NMT' => '',
            'NU_HS_NMT' => '',
            'IC_TRANSMISSAO_SIMULTANEA' => '',
            'IC_RECEPCAO_SIMULTANEA' => '',
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 130.32500000,
            'NU_FREQUENCIA_TERMINO_TX' => 130.32500000,
            'DE_ATO_RF' => '9999',
            'DH_ATO_RF' => '2017'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_RX' => 130.32500000,
            'NU_FREQUENCIA_TERMINO_RX' => 130.32500000,
            'DE_ATO_RF' => '9999',
            'DH_ATO_RF' => '2017'
        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create([
            'NO_NOME_REDE' => '00019-CP'
        ]);
        // foreach equipamento
        foreach ($equipamentos as $equipamento) {
            $conjuntoEquipamentoDetalhe = ConjuntoEquipamentoDetalhe::factory()->create([
                'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento['ID_CONJUNTO_EQUIPAMENTO'],
                'UUID_EQUIPAMENTO' => $equipamento['UUID_EQUIPAMENTO']
            ]);
        }
        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamento, $conjuntoEmissao);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000349/2024-RJ',
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504148553450025',
            'DH_INICIO' => '2024-12-29 23:35:03.760',
            'DH_TERMINO' => '2037-04-07 00:00:00.000'
        ]);


        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);

        echo '  Estação Fixa - Móvel Aeronáutico' . PHP_EOL;
        echo '  IdtEstacao = 3653002 - NumEstacao=1016283455' . PHP_EOL . PHP_EOL;
        // Estação fixa IdtEstacao = 3653002	NumEstacao=1016283455

        $entidade = $this->entidade('AEROCLUBE DE ALEGRETE');
        $outorga = $this->outorga(Servico::find(507), $entidade, 'Estacao Fixa - MovelAeronautica', null, '03021740529');

        $estacao = $this->estacao($outorga, 'Estação Fixa do Aeroclube de Alegrete', '1016283455', 'A');

        EstacaoFixa::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NU_LATITUDE' => -29.8161111111110000,
            'NU_LONGITUDE' => -55.8936111111110000,
            'ED_ENDERECO' => 'Rod. BR 290 - nº s/n'
        ]);

        $equipamentos[] = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '115092000657 - 118416',
            'NO_FABRICANTE' => 'Icom Incorporated',
            'NO_MODELO' => 'IC-A16',
            'TX_NUMERO_SERIE' => '',
            'TX_TIPO' => '',
            'TX_TECNOLOGIA' => '',
            'TX_CLASSE' => '',
            'TX_SETOR' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 0,
            // 'NU_POTENCIA' => 1.800, TODO: AJustar NU_POTENCIA para aceitar ponto
            'NU_POTENCIA' => 2,
            'NU_MEIA_POTENCIA' => '',
            'NU_MAIOR_POTENCIA_RECEPCAO' => '',
            'NU_GANHO' => '',
            'NU_PERDA' => '',
            'NU_PERDA_ADICIONAL' => '',
            'NU_TILT_ELETRICO' => '',
            'NU_TILT_MECANICO' => '',
            'NU_INTENSIDADE' => '',
            'NU_IMPEDANCIA' => '',
            'NU_ATENUACAO' => '',
            // 'NU_ALTURA' => 1.5, // TODO: AJustar NU_ALTURA para aceitar ponto
            'NU_ALTURA' => 1,
            'NU_COMPRIMENTO' => '',
            'NU_DIAMETRO' => '',
            'NU_EFICIENCIA' => '',
            'NU_MODULACAO' => '',
            'NU_FEC' => '',
            'NU_AZIMUTE' => '',
            'NU_ANGULO' => '',
            'NU_RAIO' => '',
            'NU_RADIAL' => '',
            'NU_FRENTE_COSTAS' => 0,
            'NU_POLARIZACAO' => 2,
            'NU_TAXA_INFORMACAO' => '',
            'NU_TEMPERATURA_RUIDO' => '',
            'NU_RELACAO_PORTADORA_RUIDO' => '',
            'NU_NMT' => '',
            'NU_HS_NMT' => '',
            'IC_TRANSMISSAO_SIMULTANEA' => '',
            'IC_RECEPCAO_SIMULTANEA' => '',
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 130.35000000,
            'NU_FREQUENCIA_TERMINO_TX' => 130.35000000,
            'DE_ATO_RF' => '99999',
            'DH_ATO_RF' => '2024'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_RX' => 130.35000000,
            'NU_FREQUENCIA_TERMINO_RX' => 130.35000000,
            'DE_ATO_RF' => '99999',
            'DH_ATO_RF' => '2024'
        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create([
            'NO_NOME_REDE' => '00001-CP'
        ]);
        // foreach equipamento
        foreach ($equipamentos as $equipamento) {
            $conjuntoEquipamentoDetalhe = ConjuntoEquipamentoDetalhe::factory()->create([
                'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento['ID_CONJUNTO_EQUIPAMENTO'],
                'UUID_EQUIPAMENTO' => $equipamento['UUID_EQUIPAMENTO']
            ]);
        }
        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamento, $conjuntoEmissao);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000005/2025-RS',
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '030217405290067',
            'DH_INICIO' => '2025-01-17 15:27:33.350',
            'DH_TERMINO' => '2044-11-19 00:00:00.000'
        ]);


        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
