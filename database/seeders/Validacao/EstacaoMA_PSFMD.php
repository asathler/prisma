<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\ConjuntoEstacao;
use App\Models\ConjuntoEstacaoLicenca;
use App\Models\Emissao;
use App\Models\Entidade;
use App\Models\Equipamento;
use App\Models\Estacao;
use App\Models\EstacaoAeronautica;
use App\Models\Licenca;
use App\Models\Outorga;
use App\Models\Servico;
use App\Models\Status;
use App\Models\TipoEstacao;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;


class EstacaoMA_PSFMD extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        try {
            $tipoEstacao = TipoEstacao::factory()->create([
                'NO_TIPO_ESTACOES' => 'Estação Móvel',
                'IC_TERRENA' => false
            ]);
        } catch (QueryException $e) {
            $tipoEstacao = TipoEstacao::where('NO_TIPO_ESTACOES', 'Estação Móvel')
                ->firstOrFail();
        }

//        $status = Status::factory()->create([
//            'DE_STATUS' => 'Licenciada'
//        ]);

        $status = Status::where('DE_STATUS', 'Licenciada')
            ->firstOrFail();

        try {
            $servico = Servico::factory()->create([
                'ID_SERVICOS' => 507,
                'DE_SERVICOS' => '507 - SERVIÇO MÓVEL AERONÁUTICO'
            ]);
        } catch (QueryException $e) {
            $servico = Servico::where('ID_SERVICOS', 507)->firstOrFail();
        }

        $entidade = Entidade::factory()->create([
            'DE_ENTIDADE' => 'JOAO FELIPE MIRANDA DEMITO'
        ]);

        try {
            $outorga = Outorga::factory()->create([
                'DE_OUTORGAS' => 'Outorga de Licenciamento',
                'NU_FISTEL' => '50418571724',
                'FK_TB_SERVICO_TB_OUTORGA' => $servico->ID_SERVICOS,
                'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade->UUID_ENTIDADE,
            ]);
        } catch (QueryException $e) {
            $outorga = Outorga::where('NU_FISTEL', '50418571724')->firstOrFail();
        }
        // TODO: NU_ESTACAO TEM QUE SER UNICO?
        $estacao = Estacao::factory()->create([
            'NO_ESTACOES' => '',
            'DE_ESTACOES' => '',
            //'NU_ESTACOES' => ,
            'SG_ESTACOES' => '',
            'DE_FOTO_ESTACOES' => '',
            //'VR_ALTURA' => ,
            //'CO_CLASSE' => '',
            'IC_CONFIDENCIAL' => false,
            'IC_ILHA_OCEANICA' => false,
            'IC_COMUNICACAO_SATELITE' => false,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => $tipoEstacao->ID_TIPO_ESTACOES,
            'FK_TB_STATUS_TB_ESTACOES' => $status->ID_STATUS,
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga->UUID_OUTORGAS,
        ]);

        //TODO: IC_ARRENDAMENTO??
        EstacaoAeronautica::factory()->create([
            'NO_INDICATIVO' => 'PSFMD',
            'NO_CATEGORIA' => 'TPP',
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES
        ]);

        // TODO: Falta Emissão (DesignaçãoEmissão) - nova tabela?
        // TODO: No MMAR há uma ligação entre EQUIPAMENTO->EMISSÃO(freq)->POTENCIA->(designação)EMISSÃO
        $equipamento1 = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '079311703591',
            'NO_FABRICANTE' => 'GARMIN BRASIL TECNOLOGIAS PARA AVIACAO LTDA',
            'NO_MODELO' => 'GIA 64H',
            'TX_TIPO' => 'Transceptor Móvel AM',
            'TX_CLASSE' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 16.7

        ]);

        $equipamento2 = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '016331103330',
            'NO_FABRICANTE' => 'GARMIN INTERNATIONAL INC.',
            'NO_MODELO' => 'GTR 225B',
            'TX_TIPO' => 'Transceptor Móvel AM',
            'TX_CLASSE' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 19

        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create();

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento1->UUID_EQUIPAMENTO,
        ]);

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento2->UUID_EQUIPAMENTO,
        ]);

        //TODO: No MMAR não há informação de TX e RX.
        $emissaoTx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 118,
            'NU_FREQUENCIA_TERMINO_TX' => 136.975
        ]);

        $emissaoRx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_RX' => 118,
            'NU_FREQUENCIA_TERMINO_RX' => 136.992
        ]);

        $conjuntoEmissao = ConjuntoEmissao::factory()->create();

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoTx->ID_EMISSAO,
        ]);

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoRx->ID_EMISSAO
        ]);

        $conjuntoEstacao = ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES,
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO
        ]);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000004/2025-TO',
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504185717240010',
            'DH_INICIO' => '2019-09-26',
            'DH_TERMINO' => '2039-09-26'
        ]);

        ConjuntoEstacaoLicenca::factory()->create([
            'UUID_LICENCA' => $licenca->UUID_LICENCA,
            'ID_CONJUNTO_ESTACAO' => $conjuntoEstacao->ID_CONJUNTO_ESTACAO
        ]);

        echo "NumFistel: 50418571724\n";
        echo "Indicativo: PSFMD\n";

        //-----------------

        $entidade = Entidade::factory()->create([
            'DE_ENTIDADE' => 'C.j.o. Participacoes Ltda'
        ]);

        try {
            $outorga = Outorga::factory()->create([
                'DE_OUTORGAS' => 'Outorga de Licenciamento',
                'NU_FISTEL' => '50450194493',
                'FK_TB_SERVICO_TB_OUTORGA' => $servico->ID_SERVICOS,
                'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade->UUID_ENTIDADE,
            ]);
        } catch (QueryException $e) {
            $outorga = Outorga::where('NU_FISTEL', '50450194493')->firstOrFail();
        }
        // TODO: NU_ESTACAO TEM QUE SER UNICO?
        $estacao = Estacao::factory()->create([
            'NO_ESTACOES' => '',
            'DE_ESTACOES' => '',
            //'NU_ESTACOES' => ,
            'SG_ESTACOES' => '',
            'DE_FOTO_ESTACOES' => '',
            //'VR_ALTURA' => ,
            //'CO_CLASSE' => '',
            'IC_CONFIDENCIAL' => false,
            'IC_ILHA_OCEANICA' => false,
            'IC_COMUNICACAO_SATELITE' => false,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => $tipoEstacao->ID_TIPO_ESTACOES,
            'FK_TB_STATUS_TB_ESTACOES' => $status->ID_STATUS,
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga->UUID_OUTORGAS,
        ]);

        //TODO: IC_ARRENDAMENTO??
        EstacaoAeronautica::factory()->create([
            'NO_INDICATIVO' => 'PTOZN',
            'NO_CATEGORIA' => 'TPP',
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES
        ]);

        // TODO: Falta Emissão (DesignaçãoEmissão) - nova tabela?
        // TODO: No MMAR há uma ligação entre EQUIPAMENTO->EMISSÃO(freq)->POTENCIA->(designação)EMISSÃO
        $equipamento1 = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '012060703591',
            'NO_FABRICANTE' => 'GARMIN BRASIL TECNOLOGIAS PARA AVIACAO LTDA',
            'NO_MODELO' => 'GGNS 430',
            'TX_TIPO' => 'Transceptor Móvel',
            'TX_CLASSE' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 15

        ]);

        $equipamento2 = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '012060703591',
            'NO_FABRICANTE' => 'GARMIN BRASIL TECNOLOGIAS PARA AVIACAO LTDA',
            'NO_MODELO' => 'GNS 530W TAWS',
            'TX_TIPO' => 'Transceptor Móvel',
            'TX_CLASSE' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 15

        ]);

        $equipamento3 = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '021700803702',
            'NO_FABRICANTE' => 'MARTEC Serpe - Iesm',
            'NO_MODELO' => 'KANNAD 406 AF-COMPACT',
            'TX_TIPO' => 'Transmissor de Radiobaliza',
            'TX_CLASSE' => '',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 0.4

        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create();

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento1->UUID_EQUIPAMENTO,
        ]);

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento2->UUID_EQUIPAMENTO,
        ]);

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento3->UUID_EQUIPAMENTO,
        ]);

        //TODO: No MMAR não há informação de TX e RX.
        $emissao1 = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 118,
            'NU_FREQUENCIA_TERMINO_TX' => 136.99
        ]);

        $emissao2 = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 121.5,
            'NU_FREQUENCIA_TERMINO_TX' => 406.028
        ]);

        $conjuntoEmissao = ConjuntoEmissao::factory()->create();

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissao1->ID_EMISSAO,
        ]);

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissao2->ID_EMISSAO
        ]);

        $conjuntoEstacao = ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES,
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO
        ]);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000256/2021-GO',
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504501944930002',
            'DH_INICIO' => '2021-06-10',
            'DH_TERMINO' => '2041-06-10'
        ]);

        ConjuntoEstacaoLicenca::factory()->create([
            'UUID_LICENCA' => $licenca->UUID_LICENCA,
            'ID_CONJUNTO_ESTACAO' => $conjuntoEstacao->ID_CONJUNTO_ESTACAO
        ]);

        echo "NumFistel: 50450194493\n";
        echo "Indicativo: PTOZN\n";
    }


}
