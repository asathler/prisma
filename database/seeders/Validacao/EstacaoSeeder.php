<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\ConjuntoEstacao;
use App\Models\ConjuntoEstacaoLicenca;
use App\Models\Emissao;
use App\Models\Entidade;
use App\Models\Equipamento;
use App\Models\Estacao as EstacaoModel;
use App\Models\Licenca;
use App\Models\Outorga;
use App\Models\Servico;
use Illuminate\Database\Seeder;

class EstacaoSeeder extends Seeder
{
    protected function servico($servico, $descricao)
    {
        return Servico::factory()->create([
            'ID_SERVICOS' => $servico,
            'DE_SERVICOS' => $descricao
        ]);
    }

    protected function entidade($descricao)
    {
        return Entidade::factory()->create([
            'DE_ENTIDADE' => $descricao
        ]);
    }

    protected function outorga($servico, $entidade, $descricao, $certificado, $fistel)
    {
        return Outorga::factory()->create([
            'DE_OUTORGAS' => $descricao,
            'DE_CERTIFICADO' => $certificado,
            'NU_FISTEL' => $fistel,
            'FK_TB_SERVICO_TB_OUTORGA' => $servico,
            'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade
        ]);
    }

    protected function equipamentos($quantidade = 1)
    {
        return Equipamento::factory($quantidade)->create();
    }

    protected function emissoes($quantidade = 1)
    {
        return Emissao::factory($quantidade)->create();
    }

    protected function estacao($outorga, $descricao, $numero, $debitoTfi = 'A')
    {
        $estacao = EstacaoModel::factory()->create([
            'DE_ESTACOES' => $descricao,
            'NU_ESTACOES' => $numero,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => 1, // TODO: Conferir pós testes
            'FK_TB_STATUS_TB_ESTACOES' => 1, // TODO: Conferir pós testes
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga,
            'CO_LETRA_DEBITO_TFI' => $debitoTfi
        ]);

        return $estacao;
    }

    protected function conjuntoEquipamentos($equipamentos, ...$items)
    {
        $conjuntoEquipamentos = ConjuntoEquipamento::factory()->create();

        foreach ($items as $item) {
            ConjuntoEquipamentoDetalhe::factory()->create([
                'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamentos['ID_CONJUNTO_EQUIPAMENTO'],
                'UUID_EQUIPAMENTO' => $equipamentos[$item]['UUID_EQUIPAMENTO']
            ]);
        }

        return $conjuntoEquipamentos;
    }

    protected function conjuntoEmissoes($emissoes, ...$items)
    {
        $conjuntoEmissoes = ConjuntoEmissao::factory()->create();

        foreach ($items as $item) {
            ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissoes['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissoes[$item]['ID_EMISSAO']
            ]);
        }

        return $conjuntoEmissoes;
    }

    protected function conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes)
    {
        $conjuntoEquipamento = is_null($conjuntoEquipamentos) ? null : $conjuntoEquipamentos['ID_CONJUNTO_EQUIPAMENTO'];
        $conjuntoEmissao = is_null($conjuntoEmissoes) ? null : $conjuntoEmissoes['ID_CONJUNTO_EMISSAO'];

        return ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao
        ]);
    }

    protected function licenca($numero, $tributo, $dataInicio, $dataTermino)
    {
        return Licenca::factory()->create([
            'NU_LICENCA' => $numero,
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => $tributo,
            'DH_INICIO' => $dataInicio,
            'DH_TERMINO' => $dataTermino,
        ]);
    }

    protected function conjuntoEstacaoLicenca($licenca, ...$conjuntoEstacao)
    {
        foreach ($conjuntoEstacao as $item) {
            ConjuntoEstacaoLicenca::factory()->create([
                'UUID_LICENCA' => $licenca,
                'ID_CONJUNTO_ESTACAO' => $item->ID_CONJUNTO_ESTACAO
            ]);
        }
    }
}
