<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoRadioAmador as RadioAmadorModel;

class EstacaoRadioAmador extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 302 - Rádio amador
        // Estação: 002320/2007-SP
        // Indicativo: PY2GC-002

        $servico = $this->servico(302, 'Rádio amador');
        $entidade = $this->entidade('Pedro da Silva Reis Neto');
        $outorga = $this->outorga($servico, $entidade, 'ZZZZZ não pode ser nulo', null, '02000159230');

        $equipamentos = $this->equipamentos(5);
        $emissoes = $this->emissoes(5);

        $estacao = $this->estacao($outorga, 'radio amador', '683283634', 'Z');

        RadioAmadorModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_INDICATIVO' => 'PY2GC'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '302-1', //002320/2007-SP',
            '020001592300031',
            '2007-01-01',
            '2099-12-31' // 'data_termino' => '2026-12-31',
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
