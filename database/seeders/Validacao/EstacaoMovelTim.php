<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoPessoal;

class EstacaoMovelTim extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 010 - Móvel
        // Estação: 2119 - Tim

        $servico = $this->servico(10, 'Móvel pessoal');
        $entidade = $this->entidade('Tim');
        $outorga = $this->outorga($servico, $entidade, 'Outorga (teste) da Tim', null, '50409428698');

        $equipamentos = $this->equipamentos(2);
        $emissoes = $this->emissoes(2);

        $estacao = $this->estacao($outorga, 'móvel pessoal', '2119', 'M');

        EstacaoPessoal::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'DE_TECNOLOGIA' => 'GSM'
        ]);

        // TODO: Incluir especialização para estacao_fixa (LAT-LONG, etc...)

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca1 = $this->licenca(
            '010-1', // 108/2024-PR',
            '50409428698001',
            '2001-01-01',
            '2020-12-31'
        );

        $licenca2 = $this->licenca(
            '010-1', // 108/2024-PR',
            '50409428698002',
            '2021-01-01',
            '2040-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca1, $conjuntoEstacao, $conjuntoEstacao);
        $this->conjuntoEstacaoLicenca($licenca2, $conjuntoEstacao, $conjuntoEstacao);
    }
}
