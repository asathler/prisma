<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoAeronautica as EstacaoAeronauticaModel;
use Illuminate\Support\Carbon;

class EstacaoAeronautica extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 507 - Aeronáutico
        // Estação: 686929390 (Tam)
        // Indicativo: PTMVC

        $servico = $this->servico(507, 'Aeronautico');
        $entidade = $this->entidade('TAM');
        $outorga = $this->outorga($servico, $entidade, 'Outorga (teste) da TAM', null, '02021261450');

        $equipamentos = $this->equipamentos(10);
        $emissoes = $this->emissoes(10);

        // Estação 1 - PTMVC
        $estacao = $this->estacao($outorga, 'aeronáutica 1', '686929390', 'C');

        EstacaoAeronauticaModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_INDICATIVO' => 'PTMVC'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao1 = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 2, 3);
        $conjuntoEmissoes = null;
        $conjuntoEstacao2 = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '507-1', // 108/2024-PR',
            '020212614500671',
            new Carbon('2024-01-01'),
            new Carbon('2025-12-31')
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao1, $conjuntoEstacao2);

        // Estação 2 - PRMAA
        $estacao = $this->estacao($outorga, 'aeronáutica 2', '686929616', 'C');

        EstacaoAeronauticaModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_INDICATIVO' => 'PRMAA'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 5, 6);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 5, 6);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '507-2', // 005/2024-PR',
            '020212614500586',
            new Carbon('2024-01-01'),
            new Carbon('2025-03-02')
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);

        // Estação 3 - PRMAG
        $estacao = $this->estacao($outorga, 'aeronáutica 3', '686929764', 'C');

        EstacaoAeronauticaModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_INDICATIVO' => 'PRMAG'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 8, 9);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 8, 9);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '507-3', // 006/2024-PR',
            '020212614500523',
            new Carbon('2024-01-01'),
            new Carbon('2025-03-02')
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
