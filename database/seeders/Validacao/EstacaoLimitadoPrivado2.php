<?php

namespace Database\Seeders\Validacao;

use App\Models\Servico;

class EstacaoLimitadoPrivado2 extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 019 - Limitado privado
        // Estação: 684898624
        // licenciada
        // (licenças anteriores, TALVEZ, nos histórico)

        $servico = Servico::find(19);
        $entidade = $this->entidade('Distrito Sanitário Especial Indígena Leste - RR');
        $outorga = $this->outorga($servico, $entidade, 'ZZZZZ não pode ser nulo ZZZZZ', null, '50409121029');

        $estacao = $this->estacao($outorga, 'limitado privado 2','684898624', 'C');

        // TODO: Incluir especialização para estacao_fixa (LAT-LONG, etc...)

        $conjuntoEstacao = $this->conjuntoEstacao($estacao, null, null);

        $licenca1 = $this->licenca(
            '019-1', // 62/2021-RR
            '504091210290001',
            '2001-01-01',
            '2010-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca1, $conjuntoEstacao);

        $licenca2 = $this->licenca(
            '019-2', // 62/2021-RR
            '504091210290002',
            '2011-01-01',
            '2020-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca2, $conjuntoEstacao);

        $licenca3 = $this->licenca(
            '019-3', // 62/2021-RR
            '504091210290003',
            '2021-01-01',
            '2030-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca3, $conjuntoEstacao);
    }
}
