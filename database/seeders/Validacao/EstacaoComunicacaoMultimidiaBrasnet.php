<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEquipamento;
use App\Models\EstacaoComunicacaoMultimidia;
use App\Models\Licenca;


class EstacaoComunicacaoMultimidiaBrasnet extends EstacaoSeeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $servico = $this->servico(045, 'Serviço de Comunicação Multimídia');
        $entidades = [
            $this->entidade('BRASNET ONLINE TELECOMUNICACOES LTDA'),
            $this->entidade('SECRELNET INFORMATICA EIRELI'),
        ];

        $infoEstacao = [
            [
                'fistel' => '50012256633',
                'numEstacao' => '697623580',
                'idtEstacao' => '2358140',
                'codDebitoTfi' => 'A',
                'nomeEstacao' => 'BNO-BELEM',
                'numeroLicenca' => '000003/2012-AP',
                'tributo' => '500122566330023',
                'inicioLicenca' => '2012-08-22',

            ],
            [
                'fistel' => '50403185610',
                'numEstacao' => '691252157',
                'idtEstacao' => '1666073',
                'codDebitoTfi' => 'A',
                'nomeEstacao' => 'SECRELNET-MARACANAU',
                'numeroLicenca' => '000020/2009-CE',
                'tributo' => '504031856100012',
                'inicioLicenca' => '2009-03-24',

            ]
        ];
        $outorga = [];

        foreach ($entidades as $key => $entidade) {
            echo 'Estacao Comunicacao Multimidia - '.$entidade->DE_ENTIDADE.PHP_EOL;

            $outorga = $this->outorga(
                $servico,
                $entidade,
                'Outorga '.$entidade->DE_ENTIDADE,
                null,
                $infoEstacao[$key]['fistel']
            );
            $estacao = $this->estacao(
                $outorga,
                $infoEstacao[$key]['nomeEstacao'],
                $infoEstacao[$key]['numEstacao'],
                $infoEstacao[$key]['codDebitoTfi']
            );
            $conjuntoEquipamentos = ConjuntoEquipamento::factory()->create();
            $conjuntoEmissoes = ConjuntoEmissao::factory()->create();
            $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

            EstacaoComunicacaoMultimidia::factory()->create([
                'UUID_ESTACAO' => $estacao,
            ]);


            $licenca = Licenca::factory()->create([
                'NU_LICENCA' => $infoEstacao[$key]['numeroLicenca'],
                'DE_LICENCA' => null,
                'DE_NOSSO_NUMERO_TRIBUTO_TFI' => $infoEstacao[$key]['tributo'],
                'DH_INICIO' => $infoEstacao[$key]['inicioLicenca'],
                'DH_TERMINO' => $infoEstacao[$key]['fimLicenca'] ?? null,
            ]);


            $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
        }
    }

}
