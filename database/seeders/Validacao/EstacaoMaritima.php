<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\Emissao;
use App\Models\Equipamento;
use App\Models\EstacaoMaritima as EstacaoMaritimaModel;
use App\Models\Licenca;

class EstacaoMaritima extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 604 - Marítimo
        // Estação: 687009014 // TODO: Questionar áreHá diversos registros sem NumEstação?!
        // Indicativo: PQ8170

        $servico = $this->servico(604, 'Marítimo');
        $entidades = [
            $this->entidade('PETROLEO BRASILEIRO S A PETROBRAS'),
            $this->entidade('Iate Clube de Caioba'),
            $this->entidade('Cooperativa Unipilot - Cooperativa de Apoio e Logistica Aos Praticos da Zp 1 Ltda'),
            $this->entidade('Prosafe Servicos Maritimos Ltda'),
        ];
        $infoEstacao = [
            [
                'fistel' => '50411168908',
                'numEstacao' => '1004184325',
                'idtEstacao' => '3651336',
                'codDebitoTfi' => 'A',
                'nomeEstacao' => 'P-74 VHF FIXO 2',
                'indicativo' => 'ZXN97',
                'numeroLicenca' => '000509/2024-RJ',
                'tributo' => '504111689080128',
                'inicioLicenca' => '2024-11-05',
                'fimLicenca' => '2033-12-03',
                'inicioLicenca2' => '2023-10-05',
                'fimLicenca2' => '2022-12-03',
                'inicioLicenca3' => '2021-11-05',
                'fimLicenca3' => '2022-12-03',
                'txtLicenca' => 'Estação licenciada, nova licença deve ser impressa. - D || ',

            ],
            [
                'fistel' => '50439142890',
                'numEstacao' => '1015987548',
                'idtEstacao' => '3649126',
                'nomeEstacao' => 'Caioba7',
                'codDebitoTfi' => 'A',
                'indicativo' => 'ZXA98',
                'numeroLicenca' => '000121/2024-PR',
                'tributo' => '504391428900002',
                'inicioLicenca' => '2024-10-30',
                'fimLicenca' => '2041-07-23',
                'inicioLicenca2' => '2023-10-05',
                'fimLicenca2' => '2024-10-29',
                'inicioLicenca3' => '2022-10-04',
                'fimLicenca3' => '2023-10-04',
                'txtLicenca' => 'Estação licenciada, nova licença deve ser impressa. - D ||',
            ],
            [
                'fistel' => '50439872430',
                'numEstacao' => '1015263370',
                'idtEstacao' => '3635156',
                'nomeEstacao' => 'cost 5',
                'codDebitoTfi' => 'A',
                'indicativo' => 'ZXB75',
                'numeroLicenca' => '000100/2023-PA',
                'tributo' => '504398724300010',
                'inicioLicenca' => '2023-10-02',
                'fimLicenca' => '2041-09-28',
                'inicioLicenca2' => '2022-10-05',
                'fimLicenca2' => '2023-10-01',
                'inicioLicenca3' => '2021-10-04',
                'fimLicenca3' => '2022-10-03',
                'txtLicenca' => 'Estação licenciada, nova licença deve ser impressa. - D || ',
            ],
            [
                'fistel' => '50445289198',
                'numEstacao' => '1015112673',
                'idtEstacao' => '3633416',
                'nomeEstacao' => 'Fixa 6 VHF - Safe Zephyrus',
                'codDebitoTfi' => 'A',
                'indicativo' => '',
                'numeroLicenca' => '000367/2023-RJ',
                'tributo' => '504452891980001',
                'inicioLicenca' => '2023-08-13',
                'fimLicenca' => '2043-05-16',
                'inicioLicenca2' => '2023-10-05',
                'fimLicenca2' => '2022-12-03',
                'inicioLicenca3' => '2021-11-05',
                'fimLicenca3' => '2022-12-03',
                'txtLicenca' => 'Estação licenciada, nova licença deve ser impressa. - D || ',
            ],
        ];
        $outorga = [];
        $equipamentos = $this->criaEquipamentos();
        $emissoes = $this->criaEmissoes();

        foreach ($entidades as $key => $entidade) {
            $outorga = $this->outorga(
                $servico,
                $entidade,
                'Outorga '.$entidade->DE_ENTIDADE,
                null,
                $infoEstacao[$key]['fistel']
            );
            $estacao = $this->estacao(
                $outorga,
                $infoEstacao[$key]['nomeEstacao'],
                $infoEstacao[$key]['numEstacao'],
                $infoEstacao[$key]['codDebitoTfi']
            );
            $conjuntoEquipamentos = ConjuntoEquipamento::factory()->create();
            $conjuntoEmissoes = ConjuntoEmissao::factory()->create();
            $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

            EstacaoMaritimaModel::factory()->create([
                'UUID_ESTACAO' => $estacao,
                'NO_INDICATIVO' => $infoEstacao[$key]['indicativo'],
            ]);


            ConjuntoEquipamentoDetalhe::factory()->create([
                'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamentos['ID_CONJUNTO_EQUIPAMENTO'],
                'UUID_EQUIPAMENTO' => $equipamentos[$key]['UUID_EQUIPAMENTO']
            ]);

            for ($i = 0; $i < 3; $i++) {
                ConjuntoEmissaoDetalhe::factory()->create([
                    'ID_CONJUNTO_EMISSAO' => $conjuntoEmissoes['ID_CONJUNTO_EMISSAO'],
                    'ID_EMISSAO' => $emissoes[$i]['ID_EMISSAO']
                ]);
            }

            $licenca = Licenca::factory()->create([
                'NU_LICENCA' => $infoEstacao[$key]['numeroLicenca'],
                'DE_LICENCA' => null,
                'TX_OBSERVACAO' => $infoEstacao[$key]['txtLicenca'],
                'DE_NOSSO_NUMERO_TRIBUTO_TFI' => $infoEstacao[$key]['tributo'],
                'DH_INICIO' => $infoEstacao[$key]['inicioLicenca'],
                'DH_TERMINO' => $infoEstacao[$key]['fimLicenca'],
            ]);

            Licenca::factory()->create([
                'NU_LICENCA' => $infoEstacao[$key]['numeroLicenca'],
                'DE_LICENCA' => null,
                'TX_OBSERVACAO' => $infoEstacao[$key]['txtLicenca'],
                'DE_NOSSO_NUMERO_TRIBUTO_TFI' => $infoEstacao[$key]['tributo'],
                'DH_INICIO' => $infoEstacao[$key]['inicioLicenca2'],
                'DH_TERMINO' => $infoEstacao[$key]['fimLicenca2'],
            ]);

            Licenca::factory()->create([
                'NU_LICENCA' => $infoEstacao[$key]['numeroLicenca'],
                'DE_LICENCA' => null,
                'TX_OBSERVACAO' => $infoEstacao[$key]['txtLicenca'],
                'DE_NOSSO_NUMERO_TRIBUTO_TFI' => $infoEstacao[$key]['tributo'],
                'DH_INICIO' => $infoEstacao[$key]['inicioLicenca3'],
                'DH_TERMINO' => $infoEstacao[$key]['fimLicenca3'],
            ]);

            $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
        }
    }

    /**
     * @return array
     */
    public function criaEquipamentos(): array
    {
        return [
            //PETROLEO BRASILEIRO S A PETROBRAS
            Equipamento::factory()->create([
                'DE_EQUIPAMENTO' => '018801000880 - 33121 ',
                'NO_FABRICANTE' => 'Cobham SATCOM',
                'NO_MODELO' => '6210 VHF',
                'TX_TIPO' => 'Principal',
                'TX_CLASSE' => 'FC',
                'TX_POTENCIA_UNIDADE_MEDIDA' => '25,000 W',
                'NU_POTENCIA' => '25',
                'NU_ALTURA' => '33',
                'NU_RAIO' => '20',
            ]),

            // Iate Clube de Caioba
            Equipamento::factory()->create([
                'DE_EQUIPAMENTO' => '182612003131 - 123900 ',
                'NO_FABRICANTE' => 'Icom Inc.',
                'NO_MODELO' => 'IC-M330',
                'TX_TIPO' => 'Principal',
                'TX_CLASSE' => 'FC',
                'TX_POTENCIA_UNIDADE_MEDIDA' => '25,000 W',
                'NU_POTENCIA' => '25',
                'NU_ALTURA' => '27',
                'NU_RAIO' => '100',
            ]),

            //Cooperativa Unipilot - Cooperativa de Apoio e Logistica Aos Praticos da Zp 1 Ltda
            Equipamento::factory()->create([
                'DE_EQUIPAMENTO' => '213042210291 - 23056 ',
                'NO_FABRICANTE' => 'Logitech Far East Ltd',
                'NO_MODELO' => 'Y-RBP-DEL4',
                'TX_TIPO' => 'Principal',
                'TX_CLASSE' => 'BP',
                'TX_POTENCIA_UNIDADE_MEDIDA' => '25,000 W',
                'NU_POTENCIA' => '25',
                'NU_ALTURA' => '15',
                'NU_RAIO' => '100',
            ]),

//            Prosafe Servicos Maritimos Ltda
            Equipamento::factory()->create([
                'DE_EQUIPAMENTO' => '001211601699 - 82366 ',
                'NO_FABRICANTE' => 'Motorola Solutions, Inc.',
                'NO_MODELO' => 'LAM28JQN9SA1AN',
                'TX_TIPO' => 'Principal',
                'TX_CLASSE' => 'BP',
                'TX_POTENCIA_UNIDADE_MEDIDA' => '25,000 W',
                'NU_POTENCIA' => '25',
                'NU_ALTURA' => '49',
            ]),

        ];
    }

    /**
     * @return array
     */
    public function criaEmissoes(): array
    {
        $listaEmissoes = [
            [
                'inicio' => 156,
                37500000,
                'fim' => 156,
                37500000,
            ],
            [
                'inicio' => 156,
                42500000,
                'fim' => 156,
                45000000,
            ],
            [
                'inicio' => 156,
                47500000,
                'fim' => 156,
                47500000,
            ],
            [
                'inicio' => 156,
                42500000,
                'fim' => 156,
                42500000,
            ],
            [
                'inicio' => 156,
                37500000,
                'fim' => 156,
                37500000,
            ],
            [
                'inicio' => 156,
                47500000,
                'fim' => 156,
                47500000,
            ],
            [
                'inicio' => 156,
                55000000,
                'fim' => 156,
                55000000,
            ],
            [
                'inicio' => 156,
                60000000,
                'fim' => 156,
                60000000,
            ],
            [
                'inicio' => 156,
                65000000,
                'fim' => 156,
                65000000,
            ],
            [
                'inicio' => 156,
                30000000,
                'fim' => 156,
                30000000,
            ],
            [
                'inicio' => 156,
                37500000,
                'fim' => 156,
                37500000,
            ],
            [
                'inicio' => 156,
                40000000,
                'fim' => 156,
                40000000,
            ]

        ];
        $emissoes = [];

        foreach ($listaEmissoes as $emis) {
            $emissoes[] = Emissao::factory()->create([
                'ID_EMISSAO' => fake()->unique()->numberBetween(1, 300),
                'NU_FREQUENCIA_INICIO_TX' => $emis['inicio'],
                'NU_FREQUENCIA_TERMINO_TX' => $emis['fim'],
            ]);
        }

        return $emissoes;
    }
}
