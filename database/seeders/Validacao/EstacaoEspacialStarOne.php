<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\Emissao;
use App\Models\Equipamento;
use App\Models\EstacaoEspacial;
use App\Models\EstacaoFixa;
use App\Models\Licenca;
use App\Models\Servico;

class EstacaoEspacialStarOne extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        echo '  Estacao Espacial StarOne C1' . PHP_EOL;
        echo '  IdtEstacao = 3583532 - NumEstacao=1012312230' . PHP_EOL . PHP_EOL;

        $servico = Servico::factory()->create([
            'ID_SERVICOS' => 181,
            'DE_SERVICOS' => '181 - LIMITADO PRIVADO POR SATELITE'
        ]);

        $entidade = $this->entidade('EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.');
        $outorga = $this->outorga($servico, $entidade, 'Estacao Espacial StarOne', null, '50419331034');

        $estacao = $this->estacao($outorga, 'SEVEN MERLIN-VHF PORT', '1016356495', 'A');

        EstacaoEspacial::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_OPERADOR' => 'EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.',
            'NO_UIT' => 'SBTS B4',
            'NU_CLASSE' => 17,
            'DT_DIREITO_EXPLORACAO' => '2022-11-13',
            'NO_REPRESENTANTE_LEGAL' => 'EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.',
            'NU_CPF_REPRESENTANTE_LEGAL' => '09132659000176',
            'NU_LONGITUTE_ORBITAL' => 92,
            'IC_MERIDIANO' => 'W',
            'DE_POSICAO_ORBITAL' => '',
            'DE_FAIXA_ENTRADA' => '',
            'CO_CONCATENADO' => '',
            'VR_CODIGO_CONCATENADO' => '',
            'DE_TRANSPONDER' => '',
            'DE_RESTRICAO' => '',
            'ED_UF_COBERTURA' => '',
            'DE_SUBIDA_DESCIDA' => '',
            'DE_FAIXA_SUBIDA_DISPONIVEL' => '',
            'DE_FAIXA_SUBIDA_UTILIZADA' => '',
            'DE_FAIXA_DESCIDA_DISPONIVEL' => '',
            'DE_FAIXA_DESCIDA_UTILIZADA' => '',
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01AEC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5850,
            'NU_FREQUENCIA_TERMINO_RX' => 5883,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S10ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 6287,
            'NU_FREQUENCIA_TERMINO_RX' => 6323,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S10BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 6307,
            'NU_FREQUENCIA_TERMINO_RX' => 6343,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S11ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 6327,
            'NU_FREQUENCIA_TERMINO_RX' => 6363,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S12ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 6367,
            'NU_FREQUENCIA_TERMINO_RX' => 6403,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S12BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 6387,
            'NU_FREQUENCIA_TERMINO_RX' => 6423,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S1ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5927,
            'NU_FREQUENCIA_TERMINO_RX' => 5963,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S1BEC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5867,
            'NU_FREQUENCIA_TERMINO_RX' => 5903,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S1BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5947,
            'NU_FREQUENCIA_TERMINO_RX' => 5983,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);


        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01AEC0',
            'NU_FREQUENCIA_INICIO_TX' => 3625,
            'NU_FREQUENCIA_TERMINO_TX' => 3658,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D10ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 4062,
            'NU_FREQUENCIA_TERMINO_TX' => 4098,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D10BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 4082,
            'NU_FREQUENCIA_TERMINO_TX' => 4118,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D11ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 4102,
            'NU_FREQUENCIA_TERMINO_TX' => 4138,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D11BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 4122,
            'NU_FREQUENCIA_TERMINO_TX' => 4158,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D12ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 4142,
            'NU_FREQUENCIA_TERMINO_TX' => 4178,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D12BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 4162,
            'NU_FREQUENCIA_TERMINO_TX' => 4198,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D1ANC0',
            'NU_FREQUENCIA_INICIO_TX' => 3702,
            'NU_FREQUENCIA_TERMINO_TX' => 3738,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D1BEC0',
            'NU_FREQUENCIA_INICIO_TX' => 3642,
            'NU_FREQUENCIA_TERMINO_TX' => 3678,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D1BNC0',
            'NU_FREQUENCIA_INICIO_TX' => 3722,
            'NU_FREQUENCIA_TERMINO_TX' => 3758,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);


        /*
        NO_SUBFAIXA Subida/Descida  Restrição       Freqüência Inicial  Freqüência Final    Polarização
        S01AEC0	    Subida	        Permitido	BR	    5850	            5883	MHz	        V
        S10ANC0	    Subida	        Permitido	BR	    6287	            6323	MHz	        V
        S10BNC0	    Subida	        Permitido	BR	    6307	            6343	MHz	        H
        S11ANC0	    Subida	        Permitido	BR	    6327	            6363	MHz	        V
        S11BNC0	    Subida	        Permitido	BR	    6347	            6383	MHz	        H
        S12ANC0	    Subida	        Permitido	BR	    6367	            6403	MHz	        V
        S12BNC0	    Subida	        Permitido	BR	    6387	            6423	MHz	        H
        S1ANC0	    Subida	        Permitido	BR	    5927	            5963	MHz	        V
        S1BEC0	    Subida	        Permitido	BR	    5867	            5903	MHz	        H
        S1BNC0	    Subida	        Permitido	BR	    5947	            5983	MHz	        H

        D01AEC0	    Descida	        Permitido	BR	    3625	            3658	MHz	        H
        D10ANC0	    Descida     	Permitido	BR	    4062	            4098	MHz	        H
        D10BNC0	    Descida     	Permitido	BR	    4082	            4118	MHz	        V
        D11ANC0 	Descida     	Permitido	BR	    4102	            4138	MHz	        H
        D11BNC0	    Descida 	    Permitido	BR	    4122	            4158	MHz	        V
        D12ANC0 	Descida 	    Permitido	BR	    4142	            4178	MHz	        H
        D12BNC0 	Descida 	    Permitido	BR	    4162	            4198	MHz	        V
        D1ANC0  	Descida 	    Permitido	BR	    3702	            3738	MHz	        H
        D1BEC0  	Descida 	    Permitido	BR	    3642	            3678	MHz	        V
        D1BNC0  	Descida 	    Permitido	BR	    3722	            3758	MHz	        V

         */

        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, null, $conjuntoEmissao);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000016/2022-RO',
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504193310340006',
            'DH_INICIO' => '2022-11-01 10:19:25.020',
            'DH_TERMINO' => null
        ]);

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);

//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------

        //  IdtEstacao = 3619813	NumEstacao=1014251386
        $entidade = $this->entidade('IMAGEM ASSESSORIA PROPAGANDA E PRODUCOES LTDA');
        $outorga = $this->outorga($servico, $entidade, ' LIMITADO PRIVADO POR SATELITE', null, '50405323425');

        $estacao = $this->estacao($outorga, 'IAPCerejeiras', '1014251386', 'A');

        EstacaoFixa::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ED_ENDERECO' => 'PLATAFORMA P-62 - nº s/n',
            'NU_LATITUDE' => -13.1847222222221666,
            'NU_LONGITUDE' => -60.8169444444443333,
            'ED_ENDERECO' => 'Rua José de Souza Neiva - nº 1889'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => '6M00G1FDN',
            'NU_FREQUENCIA_INICIO_RX' => 3874,
            'NU_FREQUENCIA_TERMINO_RX' => 3874,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '4773',
            'DH_ATO_RF' => '2008'
        ]);


        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '6M00G1FDN']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, null, $conjuntoEmissao);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000016/2022-RO',
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504053234250001',
            'DH_INICIO' => '2022-11-01 10:19:25.020',
            'DH_TERMINO' => null
        ]);

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);

//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------
//      ---------------------------------------------------------------------------------------------------------------------------

        echo '  Estacao Espacial STARONE C2' . PHP_EOL;
        echo '  IdtEstacao = 1466365 - NumEstacao=689954506' . PHP_EOL . PHP_EOL;

        $entidade = $this->entidade('EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.');
        // $outorga = $this->outorga($servico, $entidade, 'Estacao Espacial StarOne C2', null, '50419331034');

        $estacao = $this->estacao($outorga, 'STARONE C2', '1016356495', 'A');

        EstacaoEspacial::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'NO_OPERADOR' => 'EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.',
            'NO_UIT' => 'SBTS C1 / SBTS B1',
            'NU_CLASSE' => 17,
            'DT_DIREITO_EXPLORACAO' => '2022-11-13',
            'NO_REPRESENTANTE_LEGAL' => 'EMBRATEL TVSAT TELECOMUNICAÇÕES S.A.',
            'NU_CPF_REPRESENTANTE_LEGAL' => '09132659000176',
            'NU_LONGITUTE_ORBITAL' => 65,
            'IC_MERIDIANO' => 'W',
            'DE_POSICAO_ORBITAL' => '',
            'DE_FAIXA_ENTRADA' => '',
            'CO_CONCATENADO' => '',
            'VR_CODIGO_CONCATENADO' => '',
            'DE_TRANSPONDER' => '',
            'DE_RESTRICAO' => '',
            'ED_UF_COBERTURA' => '',
            'DE_SUBIDA_DESCIDA' => '',
            'DE_FAIXA_SUBIDA_DISPONIVEL' => '',
            'DE_FAIXA_SUBIDA_UTILIZADA' => '',
            'DE_FAIXA_DESCIDA_DISPONIVEL' => '',
            'DE_FAIXA_DESCIDA_UTILIZADA' => '',
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01AEC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5850,
            'NU_FREQUENCIA_TERMINO_RX' => 5883,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01AEK',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 13761,
            'NU_FREQUENCIA_TERMINO_RX' => 13797,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01ANC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5927,
            'NU_FREQUENCIA_TERMINO_RX' => 5963,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01ANK',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 14022,
            'NU_FREQUENCIA_TERMINO_RX' => 14058,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01BEC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5867,
            'NU_FREQUENCIA_TERMINO_RX' => 5903,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S01BNC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5947,
            'NU_FREQUENCIA_TERMINO_RX' => 5983,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S02AEC',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 5887,
            'NU_FREQUENCIA_TERMINO_RX' => 5923,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'S02AEK',
            'NU_FREQUENCIA_INICIO_TX' => 0,
            'NU_FREQUENCIA_TERMINO_TX' => 0,
            'NU_FREQUENCIA_INICIO_RX' => 13801,
            'NU_FREQUENCIA_TERMINO_RX' => 13837,
            'IC_SUBIDA' => true,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);

        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01AEC',
            'NU_FREQUENCIA_INICIO_TX' => 3625,
            'NU_FREQUENCIA_TERMINO_TX' => 3658,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01AEK',
            'NU_FREQUENCIA_INICIO_TX' => 10956,
            'NU_FREQUENCIA_TERMINO_TX' => 10992,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01ANC',
            'NU_FREQUENCIA_INICIO_TX' => 3702,
            'NU_FREQUENCIA_TERMINO_TX' => 3738,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01ANK',
            'NU_FREQUENCIA_INICIO_TX' => 11722,
            'NU_FREQUENCIA_TERMINO_TX' => 11758,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01BEC',
            'NU_FREQUENCIA_INICIO_TX' => 3642,
            'NU_FREQUENCIA_TERMINO_TX' => 3678,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D01BNC',
            'NU_FREQUENCIA_INICIO_TX' => 3722,
            'NU_FREQUENCIA_TERMINO_TX' => 3758,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'V',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D02AEC',
            'NU_FREQUENCIA_INICIO_TX' => 3662,
            'NU_FREQUENCIA_TERMINO_TX' => 3698,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D02AEK',
            'NU_FREQUENCIA_INICIO_TX' => 10996,
            'NU_FREQUENCIA_TERMINO_TX' => 11032,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);
        $emissoes[] = Emissao::factory()->create([
            'NO_SUBFAIXA' => 'D02ANC',
            'NU_FREQUENCIA_INICIO_TX' => 3742,
            'NU_FREQUENCIA_TERMINO_TX' => 3778,
            'NU_FREQUENCIA_INICIO_RX' => 0,
            'NU_FREQUENCIA_TERMINO_RX' => 0,
            'IC_SUBIDA' => false,
            'DE_POLARIZACAO' => 'H',
            'DE_ATO_RF' => '000016',
            'DH_ATO_RF' => '2022'
        ]);

        $conjuntoEmissao = ConjuntoEmissao::factory()->create(['NO_CONJUNTO_EMISSAO' => '']);
        // foreach emissoes
        foreach ($emissoes as $emissao) {
            $conjuntoEmissaoDetalhe = ConjuntoEmissaoDetalhe::factory()->create([
                'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao['ID_CONJUNTO_EMISSAO'],
                'ID_EMISSAO' => $emissao['ID_EMISSAO']
            ]);
        }
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, null, $conjuntoEmissao);

        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => '000004/2021-RJ',
            'DE_LICENCA' => null,
            'TX_OBSERVACAO' => null,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504193310340004',
            'DH_INICIO' => '2022-01-17 08:43:00.903',
            'DH_TERMINO' => '2022-11-13 00:00:00.000'
        ]);


        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
