<?php

namespace Database\Seeders\Validacao;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEmissaoDetalhe;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEquipamentoDetalhe;
use App\Models\ConjuntoEstacao;
use App\Models\ConjuntoEstacaoLicenca;
use App\Models\Emissao;
use App\Models\Entidade;
use App\Models\Equipamento;
use App\Models\Estacao;
use App\Models\EstacaoFixa;
use App\Models\Licenca;
use App\Models\Outorga;
use App\Models\Servico;
use App\Models\Status;
use App\Models\TipoEstacao;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;


class DatabaseSeederServico099 extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        try {
            $tipoEstacao = TipoEstacao::factory()->create([
                'NO_TIPO_ESTACOES' => 'Estação com radiação restrita',
                'IC_TERRENA' => true
            ]);
        } catch (QueryException $e) {
            $tipoEstacao = TipoEstacao::where('NO_TIPO_ESTACOES', 'Estação com radiação restrita')
                ->firstOrFail();
        }

        $status = Status::factory()->create([
            'DE_STATUS' => 'Licenciada'
        ]);

        try {
            $servico = Servico::factory()->create([
                'ID_SERVICOS' => 99,
                'DE_SERVICOS' => '099 - SERVIÇO DE RADIAÇÃO RESTRITA'
            ]);
        } catch (QueryException $e) {
            $servico = Servico::where('ID_SERVICOS', 99)->firstOrFail();
        }

        $entidade = Entidade::factory()->create([
            'DE_ENTIDADE' => 'WKVE-ASSES. EM SERV. DE INF. E TELECOMUNICACOES LTDA'
        ]);

        try {
            $outorga = Outorga::factory()->create([
                'DE_OUTORGAS' => 'Outorga de Licenciamento',
                'NU_FISTEL' => '50420261699',
                'FK_TB_SERVICO_TB_OUTORGA' => $servico->ID_SERVICOS,
                'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade->UUID_ENTIDADE,
            ]);
        } catch (QueryException $e) {
            $outorga = Outorga::where('NU_FISTEL', '50420261699')->firstOrFail();
        }
        // TODO: NU_ESTACAO TEM QUE SER UNICO?
        $estacao = Estacao::factory()->create([
            'NO_ESTACOES' => 'MG-IIG-TORRE.TERRACOPMI',
            'DE_ESTACOES' => '',
            'NU_ESTACOES' => 1012344000,
            'SG_ESTACOES' => '',
            'DE_FOTO_ESTACOES' => '',
            'VR_ALTURA' => 272,
            'CO_CLASSE' => 'FX',
            'IC_CONFIDENCIAL' => false,
            'IC_ILHA_OCEANICA' => false,
            'IC_COMUNICACAO_SATELITE' => false,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => $tipoEstacao->ID_TIPO_ESTACOES,
            'FK_TB_STATUS_TB_ESTACOES' => $status->ID_STATUS,
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga->UUID_OUTORGAS,
        ]);

        EstacaoFixa::factory()->create([
            'NU_LATITUDE' => -19.47921666,
            'NU_LONGITUDE' => -42.52757777,
            'ED_ENDERECO' => 'Maria Jorge Selim de Sales - nº 100, Centro, Ipatinga-MG',
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES
        ]);

        // TODO: valores não podem ser int
        // TODO: polarização pode ser string
        $equipamento = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '',
            'NO_FABRICANTE' => 'Orchard Parkway',
            'NO_MODELO' => 'NBE-M5-16 (*)',
            'TX_TIPO' => 'Transceptor de Radiação Restrita',
            'TX_CLASSE' => 'FX',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 0,
            'NU_MEIA_POTENCIA' => 4,
            'NU_GANHO' => 16,
            'NU_ALTURA' => 24,
            'NU_AZIMUTE' => 444,
            'NU_ANGULO' => 4,
            'NU_RAIO' => 350,
            'NU_FRENTE_COSTAS' => 31,
            //'NU_POLARIZAO' => 'X', Deve ser tipo
            'NU_POLARIZACAO' => 0,

        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create();

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento->UUID_EQUIPAMENTO,
        ]);

        $emissaoTx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 5470,
            'NU_FREQUENCIA_TERMINO_TX' => 5470
        ]);

        $emissaoRx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_RX' => 5470,
            'NU_FREQUENCIA_TERMINO_RX' => 5470
        ]);

        // TODO: Falta identificação de Recepção e transmissão
        $conjuntoEmissao = ConjuntoEmissao::factory()->create();

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoTx->ID_EMISSAO,
        ]);

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoRx->ID_EMISSAO
        ]);

        $conjuntoEstacao = ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES,
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO
        ]);

        // TODO: NU_LICENCA/DATAS UNICAS?
        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => 11111,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504202616990001',
            'DH_INICIO' => '2021-09-02 09:43:28.190',
            'DH_TERMINO' => '2041-09-02 09:43:28.190'
        ]);

        ConjuntoEstacaoLicenca::factory()->create([
            'UUID_LICENCA' => $licenca->UUID_LICENCA,
            'ID_CONJUNTO_ESTACAO' => $conjuntoEstacao->ID_CONJUNTO_ESTACAO
        ]);

        echo "NumFistel: 50420261699\n";
        echo "NumEstacao: 1012344000\n";

        //-----------------

        $entidade = Entidade::factory()->create([
            'DE_ENTIDADE' => 'Sbs-net Telecomunicacoes Ltda'
        ]);

        try {
            $outorga = Outorga::factory()->create([
                'DE_OUTORGAS' => 'Outorga de Licenciamento',
                'NU_FISTEL' => '50414853300',
                'FK_TB_SERVICO_TB_OUTORGA' => $servico->ID_SERVICOS,
                'FK_TB_ENTIDADE_TB_OUTORGA' => $entidade->UUID_ENTIDADE
            ]);
        } catch (QueryException $e) {
            $outorga = Outorga::where('NU_FISTEL', '50414853300')->firstOrFail();
        }
        // TODO: NU_ESTACAO TEM QUE SER UNICO?
        $estacao = Estacao::factory()->create([
            'NO_ESTACOES' => 'Trincheira',
            'DE_ESTACOES' => '',
            'NU_ESTACOES' => 1012340322,
            'SG_ESTACOES' => '',
            'DE_FOTO_ESTACOES' => '',
            'VR_ALTURA' => 951,
            'CO_CLASSE' => 'XR',
            'IC_CONFIDENCIAL' => false,
            'IC_ILHA_OCEANICA' => false,
            'IC_COMUNICACAO_SATELITE' => false,
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => $tipoEstacao->ID_TIPO_ESTACOES,
            'FK_TB_STATUS_TB_ESTACOES' => $status->ID_STATUS,
            'FK_TB_OUTORGA_TB_ESTACOES' => $outorga->UUID_OUTORGAS
        ]);

        EstacaoFixa::factory()->create([
            'NU_LATITUDE' => -22.68077222,
            'NU_LONGITUDE' => -45.73115000,
            'ED_ENDERECO' => 'Rodovia Candido Ribeiro - nº S/N	, Quilombo, São Bento do Sapucaí-SP',
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES
        ]);

        // TODO: valores não podem ser int
        // TODO: polarização pode ser string
        $equipamento = Equipamento::factory()->create([
            'DE_EQUIPAMENTO' => '',
            'NO_FABRICANTE' => '2580 Orchard Parkway',
            'NO_MODELO' => 'AirMax Sector 5G-20-90',
            'TX_TIPO' => 'Antena Direcional',
            'TX_CLASSE' => 'XR',
            'TX_POTENCIA_UNIDADE_MEDIDA' => 'W',
            'NU_POTENCIA' => 0,
            'NU_ALTURA' => 8,
            'NU_RAIO' => 3000,
            //'NU_POLARIZAO' => 'X', Deve ser tipo
            'NU_POLARIZACAO' => 0
        ]);

        $conjuntoEquipamento = ConjuntoEquipamento::factory()->create();

        ConjuntoEquipamentoDetalhe::factory()->create([
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'UUID_EQUIPAMENTO' => $equipamento->UUID_EQUIPAMENTO
        ]);

        $emissaoTx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_TX' => 5725,
            'NU_FREQUENCIA_TERMINO_TX' => 5725
        ]);

        $emissaoRx = Emissao::factory()->create([
            'NU_FREQUENCIA_INICIO_RX' => 5725,
            'NU_FREQUENCIA_TERMINO_RX' => 5725
        ]);

        // TODO: Falta identificação de Recepção e transmissão
        $conjuntoEmissao = ConjuntoEmissao::factory()->create();

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoTx->ID_EMISSAO
        ]);

        ConjuntoEmissaoDetalhe::factory()->create([
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO,
            'ID_EMISSAO' => $emissaoRx->ID_EMISSAO
        ]);

        $conjuntoEstacao = ConjuntoEstacao::factory()->create([
            'UUID_ESTACAO' => $estacao->UUID_ESTACOES,
            'ID_CONJUNTO_EQUIPAMENTO' => $conjuntoEquipamento->ID_CONJUNTO_EQUIPAMENTO,
            'ID_CONJUNTO_EMISSAO' => $conjuntoEmissao->ID_CONJUNTO_EMISSAO
        ]);

        // TODO: NU_LICENCA/DATAS UNICAS?
        $licenca = Licenca::factory()->create([
            'NU_LICENCA' => 22222,
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '504148533000001',
            'DH_INICIO' => '2021-09-01 12:32:50.603'
        ]);

        ConjuntoEstacaoLicenca::factory()->create([
            'UUID_LICENCA' => $licenca->UUID_LICENCA,
            'ID_CONJUNTO_ESTACAO' => $conjuntoEstacao->ID_CONJUNTO_ESTACAO,
        ]);

        echo "NumFistel: 50420261699\n";
        echo "NumEstacao: 1012344000\n";

    }


}
