<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoFM as EstacaoFMModel;
use App\Models\EstacaoFixa;
use App\Models\PlanoBasico;

class EstacaoFM extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 230 - FM
        // Estação:
        // UF: BA
        // Cidade: Salvador
        // Canal: 295
        // idCanal: 57dbac1340859
        // Transamérica FM da Bahia LTDA

        $servico = $this->servico(230, 'FM');
        $entidade = $this->entidade('Bahia');
        $outorga = $this->outorga($servico, $entidade, 'Rádio (teste) Transamérica FM da Bahia LTDA', null, '06008006364');

        $equipamentos = $this->equipamentos(2);
        $emissoes = $this->emissoes(2);

        $estacao = $this->estacao($outorga, 'rádio FM', '322610044', 'R');

        $planoBasico = PlanoBasico::factory()->create([
            'FK_TB_SERVICOS_TB_PLANOS_BASICOS' => $servico,
            'SG_UF' => 'BA',
            'NO_MUNICIPIO' => '2900801',
            // 'IdtCanalizacao' => '57dbac1340859',
        ]);

        EstacaoFMModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ID_PLANO_BASICO' => $planoBasico
        ]);

        EstacaoFixa::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ED_ENDERECO' => 'Endereço da Bahia'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '230-1', // 108/2024-PR',
            '060080063640001',
            '2016-01-01',
            '2025-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
