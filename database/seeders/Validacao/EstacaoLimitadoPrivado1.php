<?php

namespace Database\Seeders\Validacao;

class EstacaoLimitadoPrivado1 extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 019 - Limitado privado
        // Estação: 1010851150
        // não licenciada

        $servico = $this->servico(19, 'Limitado privado');
        $entidade = $this->entidade('José Carlos Guimarães');
        $outorga = $this->outorga($servico, $entidade, 'ZZZZZ não pode ser nulo ZZZZZ', null, '50005709792');
        $estacao = $this->estacao($outorga, 'limitado privado 1', '1010851150', 'A');

        // TODO: Incluir especialização para estacao_fixa (LAT-LONG, etc...)

        $this->conjuntoEstacao($estacao, null, null);
    }
}
