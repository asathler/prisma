<?php

namespace Database\Seeders\Validacao;

use App\Models\EstacaoEspacial as EstacaoEspacialModel;
use App\Models\EstacaoFixa;

class EstacaoEspacial extends EstacaoSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Serviço: 185 - Espacial
        // Estação: 1012808979 // 1012851904
        // Nome: Sistema STARLINK

        $servico185 = $this->servico(185, 'Exploração de satélite e estações de acesso');
        $entidade = $this->entidade('StarLink do Ellon Musk');
        $outorga = $this->outorga($servico185, $entidade, 'Outorga (teste) do Ellon Musk', null, '50407303650');

        $equipamentos = $this->equipamentos(4);
        $emissoes = $this->emissoes(4);

        // Satélite
        $estacao = $this->estacao($outorga, 'Ellon Musk - Satélite', '1012808979', 'E');

        EstacaoEspacialModel::factory()->create([
            'UUID_ESTACAO' => $estacao,
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 0, 1);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 0, 1);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '185-1',
            '504073036500001',
            '2020-01-01',
            '2029-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);

        // Estação fixa
        $servico47 = $this->servico(47, 'SCM Terrena');
        $outorga = $this->outorga($servico47, $entidade, 'Outorga (teste) do Ellon Musk', null, '50439284767');

        $estacao = $this->estacao($outorga, 'Ellon Musk - Fixa', '1012851904', 'F');

        EstacaoFixa::factory()->create([
            'UUID_ESTACAO' => $estacao,
            'ED_ENDERECO' => 'Endereço do Ellon Musk'
        ]);

        $conjuntoEquipamentos = $this->conjuntoEquipamentos($equipamentos, 2, 3);
        $conjuntoEmissoes = $this->conjuntoEmissoes($emissoes, 2, 3);
        $conjuntoEstacao = $this->conjuntoEstacao($estacao, $conjuntoEquipamentos, $conjuntoEmissoes);

        $licenca = $this->licenca(
            '047-1',
            '504392847679999',
            '2020-01-01',
            '2029-12-31'
        );

        $this->conjuntoEstacaoLicenca($licenca, $conjuntoEstacao);
    }
}
