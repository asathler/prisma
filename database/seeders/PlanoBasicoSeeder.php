<?php

namespace Database\Seeders;

use App\Models\PlanoBasico;
use Illuminate\Database\Seeder;

class PlanoBasicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PlanoBasico::factory()->count(20)->create();
    }
}
