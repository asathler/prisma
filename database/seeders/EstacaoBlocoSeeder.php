<?php

namespace Database\Seeders;

use App\Models\EstacaoBloco;
use Illuminate\Database\Seeder;

class EstacaoBlocoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EstacaoBloco::factory()->count(80)->create();
    }
}
