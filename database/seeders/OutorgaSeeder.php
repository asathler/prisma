<?php

namespace Database\Seeders;

use App\Models\Outorga;
use Illuminate\Database\Seeder;

class OutorgaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Outorga::factory()->count(10)->create();
    }
}
