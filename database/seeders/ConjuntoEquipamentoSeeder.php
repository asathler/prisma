<?php

namespace Database\Seeders;

use App\Models\ConjuntoEquipamento;
use Illuminate\Database\Seeder;

class ConjuntoEquipamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ConjuntoEquipamento::factory()->count(20)->create();
    }
}
