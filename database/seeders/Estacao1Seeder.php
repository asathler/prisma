<?php

namespace Database\Seeders;

use App\Models\Estacao;
use Illuminate\Database\Seeder;

class Estacao1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*
        $qtdeRepeticoes = 5;
        $qtdeRegistrosEmLote = 1000;

        $numeroFormatado = $this->numeroFormatado($qtdeRepeticoes * $qtdeRegistrosEmLote);

        $this->mostra("Gerando $numeroFormatado de registros!");

        for ($i = 1; $i <= $qtdeRepeticoes; $i++) {
            $estacoes = Estacao::factory()->count($qtdeRegistrosEmLote)->create();

            $numeroFormatado = $this->numeroFormatado($i * $qtdeRegistrosEmLote);

            $this->mostra($numeroFormatado . ' registros...');
        }
        */
        $this->mostra('TESTE: A geração de 5.000 registros neste processo durava, em média, 30~35 segundos.');
    }

    private function mostra($texto)
    {
        echo '  ' . $texto . PHP_EOL;
    }

    private function numeroFormatado($numero)
    {
        return number_format($numero, 0, '', '.');
    }
}
