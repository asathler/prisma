<?php

namespace Database\Seeders;

use App\Models\TipoEstacao;
use Illuminate\Database\Seeder;

class TipoEstacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TipoEstacao::insert($this->tipos());

        TipoEstacao::factory()->count(13)->create();
    }

    private function tipos()
    {
        return [
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => true,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => false,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => true,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => false,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => true,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => false,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => true,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => false,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => true,
                'IC_ESPACIAL' => false,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => true,
            ],
            [
                'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
                'IC_TERRENA' => false,
                'IC_MOVEL' => false,
                'IC_PESSOAL' => false,
                'IC_MARITIMA' => false,
                'IC_AERONAUTICA' => false,
                'IC_ESPACIAL' => false,
            ],
        ];
    }
}
