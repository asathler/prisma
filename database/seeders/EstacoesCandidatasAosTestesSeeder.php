<?php

namespace Database\Seeders;

use Database\Seeders\Validacao\EstacaoEspacial;
use Database\Seeders\Validacao\EstacaoFM;
use Database\Seeders\Validacao\EstacaoLimitadoPrivado1;
use Database\Seeders\Validacao\EstacaoLimitadoPrivado2;
use Database\Seeders\Validacao\EstacaoMaritima;
use Database\Seeders\Validacao\EstacaoMovelTim;
use Database\Seeders\Validacao\EstacaoMovelVivo;
use Database\Seeders\Validacao\EstacaoRadioAmador;
use Database\Seeders\Validacao\EstacaoAeronautica;
use Database\Seeders\Validacao\EstacoesEmBloco;
use Illuminate\Database\Seeder;

class EstacoesCandidatasAosTestesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        echo '  Validações com dados reais:' . PHP_EOL . PHP_EOL;

        $this->call([
            EstacaoLimitadoPrivado1::class,
            EstacaoLimitadoPrivado2::class,

            EstacaoRadioAmador::class,
            EstacaoAeronautica::class,
            EstacaoMaritima::class,

            EstacaoMovelTim::class,
            EstacaoMovelVivo::class,

            EstacaoFM::class,

            EstacaoEspacial::class,

            EstacoesEmBloco::class
        ]);

        // TODO: Revisar NECESSIDADE dos campos nullable()... melhor local e sua permanência!

        // TODO: Revisar nomenclatura de TODAS as estruturas!

        // TODO: Montar MINI script de carga (por serviço / por banco / por outro fator??)

        // TODO: ANTES DE APRESENTAÇÕES...
        // Documentar estruturas:
        // NO_ESTACAO, string, 250
        // NO_LICENCA, string, 250
        // Incluir comentários (tabelas, campos, índices, etc...):
        //
        /// Documentar regras:
        // Padrões adotados;
        // Conceitos / Literatura seguidos;
        // Convenções do novo modelo;
        // ...;
    }
}
