<?php

namespace Database\Seeders;

use App\Models\Estacao;
use App\Models\Outorga;
use App\Models\PlanoBasico;
use App\Models\Status;
use App\Models\TipoEstacao;
use App\Models\Usuario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class Estacao4Seeder extends Seeder
{
    protected $tipoEstacao;
    protected $planoBasico;
    protected $status;
    protected $outorga;
    protected $usuario;
    protected $imgs;
    protected $letrasDebito;
    private $memoria;

    public function __construct()
    {
        /*
        // Preparando relações ou listas
        $this->tipoEstacao = TipoEstacao::all('id')->pluck('id')->toArray();
        $this->planoBasico = PlanoBasico::all('id')->pluck('id')->toArray();
        $this->status = Status::all('id')->pluck('id')->toArray();
        $this->outorga = Outorga::all('uuid')->pluck('uuid')->toArray();
        $this->usuario = Usuario::all('uuid')->pluck('uuid')->toArray();
        $this->imgs = [1, 2, 3, 4];
        $this->letrasDebito = ['A', 'B', 'C', 'D'];

        $this->memoria = ini_get('memory_limit');

        ini_set('memory_limit', '1024M');
        */
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*
        $total = 5000;
        $qtde = 5000;

        $this->mostra('Populando o total de ' . $this->numeroFormatado($total) . ' registros!');

        for ($i = 1; $i <= ($total / $qtde); $i++) {
            $estacoes = $this->estacoes($qtde);

            $this->gravaEstacoes($estacoes, 2500);
        }
        */
        $this->mostra('TESTE: A geração de 5.000 registros neste processo durava, em média, menos de 2 segundos!');
    }

    private function estacoes($qtde = 1000)
    {
        $this->mostra('Gerando ' . $this->numeroFormatado($qtde) . ' registros...');

        $estacoes = [];

        // Gerando...
        for ($i = 1; $i <= $qtde; $i++) {
            $nome = fake()->name();

            $estacoes[] = [
                'UUID_ESTACOES' => fake()->uuid(),
                'nome' => $nome,
                'descricao' => fake()->sentence(),
                'numero' => fake()->numerify('#######'),
                'nome_indicativo' => Str::slug($nome),
                'sigla' => $this->sigla($nome),
                'arquivo_foto' => fake()->randomElement($this->imgs) . '.png',
                'tipo_estacoes_id' => fake()->randomElement($this->tipoEstacao),
                'planos_basicos_id' => fake()->randomElement($this->planoBasico),
                'status_id' => fake()->randomElement($this->status),
                'UUID_OUTORGAS' => fake()->randomElement($this->outorga),
                'letra_debito_tfi' => fake()->randomElement($this->letrasDebito),
                'usuario_inclusao' => fake()->randomElement($this->usuario),
            ];
        }

        return $estacoes;
    }

    private function gravaEstacoes($estacoes, $qtde = 1000): void
    {
        // Gravando...
        foreach (array_chunk($estacoes, $qtde) as $dados) {
            Estacao::insert($dados);

            $this->mostra('Gravando ' . $this->numeroFormatado(count($dados)) . ' registros...');

            memory_get_usage(true);
            gc_collect_cycles();
        }
    }

    private function sigla($nome)
    {
        $nomes = Str::ucsplit($nome);

        $letras = array_map(
            fn($n) => Str::of($n)->substr(0, 1)->ascii(),
            $nomes
        );

        return implode('', $letras);
    }

    private function mostra($texto)
    {
        echo '  ' . $texto . PHP_EOL;
    }

    private function numeroFormatado($numero)
    {
        return number_format($numero, 0, '', '.');
    }
}
