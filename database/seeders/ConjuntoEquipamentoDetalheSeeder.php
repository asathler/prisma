<?php

namespace Database\Seeders;

use App\Models\ConjuntoEquipamentoDetalhe;
use Illuminate\Database\Seeder;

class ConjuntoEquipamentoDetalheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ConjuntoEquipamentoDetalhe::factory()->count(15)->create();
    }
}
