<?php

namespace Database\Seeders;

use App\Models\MotivoExclusao;
use Illuminate\Database\Seeder;

class MotivoExclusaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MotivoExclusao::insert($this->motivos());
    }

    private function motivos()
    {
        return [
            ['SG_MOTIVO_EXCLUSAO' => 'AD', 'DE_MOTIVO_EXCLUSAO' => 'Adaptação de Serviço'],
            ['SG_MOTIVO_EXCLUSAO' => 'AN', 'DE_MOTIVO_EXCLUSAO' => 'Anulação'],
            ['SG_MOTIVO_EXCLUSAO' => 'AU', 'DE_MOTIVO_EXCLUSAO' => 'Renúncia Automática pelo sistema - SCPX'],
            ['SG_MOTIVO_EXCLUSAO' => 'CD', 'DE_MOTIVO_EXCLUSAO' => 'Caducidade'],
            ['SG_MOTIVO_EXCLUSAO' => 'CO', 'DE_MOTIVO_EXCLUSAO' => 'Consolidação de Permissão'],
            ['SG_MOTIVO_EXCLUSAO' => 'CS', 'DE_MOTIVO_EXCLUSAO' => 'Cassação'],
            ['SG_MOTIVO_EXCLUSAO' => 'DE', 'DE_MOTIVO_EXCLUSAO' => 'Decaimento'],
            [
                'SG_MOTIVO_EXCLUSAO' => 'DU',
                'DE_MOTIVO_EXCLUSAO' => 'Duplicado para Serviço/Entidade(outro Fistel ativo)'
            ],
            ['SG_MOTIVO_EXCLUSAO' => 'EX', 'DE_MOTIVO_EXCLUSAO' => 'Extinção'],
            ['SG_MOTIVO_EXCLUSAO' => 'FU', 'DE_MOTIVO_EXCLUSAO' => 'Falecimento do Usuário'],
            ['SG_MOTIVO_EXCLUSAO' => 'MM', 'DE_MOTIVO_EXCLUSAO' => 'Estação Migrada para o Mosaico'],
            ['SG_MOTIVO_EXCLUSAO' => 'PD', 'DE_MOTIVO_EXCLUSAO' => 'Renúncia por não pagamento do PPDESS(RES 386)'],
            ['SG_MOTIVO_EXCLUSAO' => 'PP', 'DE_MOTIVO_EXCLUSAO' => 'Parecer da Procuradoria'],
            ['SG_MOTIVO_EXCLUSAO' => 'PU', 'DE_MOTIVO_EXCLUSAO' => 'Renúncia'],
            ['SG_MOTIVO_EXCLUSAO' => 'RD', 'DE_MOTIVO_EXCLUSAO' => 'Outorga Vencida'],
            [
                'SG_MOTIVO_EXCLUSAO' => 'SE',
                'DE_MOTIVO_EXCLUSAO' => 'SEM Estação: todas excluídas, última transferida ou nenhuma vinculada/cadastrada'
            ],
            ['SG_MOTIVO_EXCLUSAO' => 'TO', 'DE_MOTIVO_EXCLUSAO' => 'Transferência de Outorga'],
        ];
    }
}
