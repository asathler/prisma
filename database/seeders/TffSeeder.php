<?php

namespace Database\Seeders;

use App\Models\Tff;
use Illuminate\Database\Seeder;

class TffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tff::factory()->count(20)->create();
    }
}
