<?php

namespace Database\Seeders;

use App\Models\ConjuntoEmissao;
use Illuminate\Database\Seeder;

class ConjuntoEmissaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ConjuntoEmissao::factory()->count(20)->create();
    }
}
