<?php

namespace Database\Seeders;

use App\Models\Licenca;
use Illuminate\Database\Seeder;

class LicencaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Licenca::factory()->count(50)->create();
    }
}
