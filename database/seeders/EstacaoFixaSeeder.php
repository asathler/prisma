<?php

namespace Database\Seeders;

use App\Models\Estacao;
use App\Models\EstacaoFixa;
use App\Models\TipoEstacao;
use App\Models\Usuario;
use Illuminate\Database\Seeder;

class EstacaoFixaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // EstacaoFixa::factory()->count(10)->create();
        // EstacaoFixa::factory()->create();

        $tiposTerrenos = TipoEstacao::where('IC_TERRENA', true)->pluck('ID_TIPO_ESTACAO');
        $estacoesFixas = Estacao::whereIn('ID_TIPO_ESTACAO', $tiposTerrenos)->get();

        foreach ($estacoesFixas as $estacao) {
            EstacaoFixa::insert([
                'UUID_ESTACAO' => $estacao->UUID_ESTACAO,
                'NU_LATITUDE' => fake()->latitude(),
                'NU_LONGITUDE' => fake()->longitude(),
                'NU_ALTITUDE' => fake()->randomNumber(3),
                // 'ponto' => null,
                // 'area' => null,
                'ED_ENDERECO' => fake()->sentence(),
                'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
            ]);

            Estacao::find($estacao->uuid)->update([
                'DE_FOTO_ESTACAO' => '1.png'
            ]);
        }
    }
}
