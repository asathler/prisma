<?php

namespace Database\Seeders;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Seeder;

class EstacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Registro controle, com os seguintes dados fixos
        Estacao::factory()->create([
            'UUID_ESTACOES' => 'abcdef00-e9da-439d-87ce-7cbadf778899',
            'NO_ESTACOES' => 'Estação ASmR de Radiofrequência (PRISMA/Teste) - UnB',
//            'nome_indicativo' => 'RFASmR/UnB',
            'SG_ESTACOES' => 'RFASmR',
            'NU_ESTACOES' => 001,
            'DE_FOTO_ESTACOES' => '1.png',
            'FK_TB_USUARIOS_TB_ESTACOES_INCLUSAO' => Usuario::first(),
        ]);

        $this->mostra('Gerando 1 registro de controle.');
    }

    private function mostra($texto)
    {
        echo '  '.$texto.PHP_EOL;
    }
}
