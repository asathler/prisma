<?php

namespace Database\Seeders;

use App\Models\TipoMovimentacao;
use Illuminate\Database\Seeder;

class TipoMovimentacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TipoMovimentacao::factory()->count(12)->create();
    }
}
