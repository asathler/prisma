<?php

namespace Database\Seeders;

use App\Models\ConjuntoEmissaoDetalhe;
use Illuminate\Database\Seeder;

class ConjuntoEmissaoDetalheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ConjuntoEmissaoDetalhe::factory()->count(15)->create();
    }
}
