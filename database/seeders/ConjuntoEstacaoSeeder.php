<?php

namespace Database\Seeders;

use App\Models\ConjuntoEstacao;
use Illuminate\Database\Seeder;

class ConjuntoEstacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ConjuntoEstacao::factory()->count(20)->create();
    }
}
