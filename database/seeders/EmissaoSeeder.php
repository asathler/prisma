<?php

namespace Database\Seeders;

use App\Models\Emissao;
use Illuminate\Database\Seeder;

class EmissaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Emissao::factory()->count(50)->create();
    }
}
