<?php

namespace Database\Factories;

use App\Models\PlanoBasico;
use App\Models\Servico;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<PlanoBasico>
 */
class PlanoBasicoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // TODO: Plano básico é importante e há várias colunas com dados de negócio. Rever essa factory.

        return [
            'FK_TB_SERVICOS_TB_PLANOS_BASICOS' => Servico::pluck('ID_SERVICOS')->random(),
            'SG_UF' => fake()->asciify('**'),
            'NO_MUNICIPIO' => fake()->numerify('#######'),
            'ID_IDTCANALIZACAO' => fake()->numberBetween(9999, 999999),
            'DE_PLANOS_BASICOS' => fake()->sentence()
        ];
    }
}
