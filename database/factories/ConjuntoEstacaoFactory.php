<?php

namespace Database\Factories;

use App\Models\ConjuntoEmissao;
use App\Models\ConjuntoEquipamento;
use App\Models\ConjuntoEstacao;
use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEquipamentoEstacao>
 */
class ConjuntoEstacaoFactory extends Factory
{
    protected $i = 0;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $equipamentos = ConjuntoEquipamento::pluck('ID_CONJUNTO_EQUIPAMENTO');
        $emissoes = ConjuntoEmissao::pluck('ID_CONJUNTO_EMISSAO');

        $equipamentos = count($equipamentos) > 0 ? $equipamentos->random() : null;
        $emissoes = count($emissoes) > 0 ? $emissoes->random() : null;
        return [
            'ID_CONJUNTO_ESTACAO' => fake()->unique()->numberBetween(550, 999),
            'UUID_ESTACAO' => Estacao::pluck('UUID_ESTACOES')->random(),
            'ID_CONJUNTO_EQUIPAMENTO' => $equipamentos,
            'ID_CONJUNTO_EMISSAO' => $emissoes,
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }

    public function definition_OLD(): array
    {
        $conjuntoEstacao = [
            'e' => $estacao,
            'q' => $equipamento,
            'f' => $frequencia
        ];

        // se conjunto estiver presente em TODAS, resorteia
        while (in_array($conjuntoEstacao, $conjuntoTodas)) {
            $estacao = Estacao::pluck('UUID_ESTACOES')->random();
            $equipamento = ConjuntoEquipamento::pluck('ID_CONJUNTO_EQUIPAMENTO')->random();
            $frequencia = ConjuntoEmissao::pluck('ID_CONJUNTO_EMISSAO')->random();

            $conjuntoEstacao = [
                'e' => $estacao,
                'q' => $equipamento,
                'f' => $frequencia
            ];
        }

        // senão, grava em todas!
        $conjuntoTodas[$this->i++] = $conjuntoEstacao;
        session(['conjunto_estacao' => $conjuntoTodas]);

        return [
            'UUID_ESTACOES' => $estacao,
            'ID_CONJUNTO_EQUIPAMENTO' => $equipamento,
            'ID_CONJUNTO_EMISSAO' => $frequencia,
            'UUID_USUARIO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
