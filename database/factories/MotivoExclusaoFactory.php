<?php

namespace Database\Factories;

use App\Models\MotivoExclusao;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<MotivoExclusao>
 */
class MotivoExclusaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'SG_MOTIVO_EXCLUSAO' => fake()->unique()->text(6),
            'DE_MOTIVO_EXCLUSAO' => fake()->sentence(),
        ];
    }
}
