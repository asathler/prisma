<?php

namespace Database\Factories;

use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Licenca>
 */
class LicencaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'UUID_LICENCA' => fake()->uuid(),
            'DE_LICENCA' => fake()->sentence(6),
            'TX_OBSERVACAO' => fake()->text(),
            'NU_LICENCA' => fake()->numerify(),
            'DE_NOSSO_NUMERO_TRIBUTO_TFI' => '123456789010001', // fistel + seq do sigec
            'DH_INICIO' => null,
            'DH_TERMINO' => null,
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
