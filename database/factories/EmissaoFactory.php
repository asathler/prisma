<?php

namespace Database\Factories;

use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Emissao>
 */
class EmissaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $p1 = fake()->numberBetween(0, 9999);
        $p2 = fake()->numberBetween(0, 999) / 1000;

        $float = $p1 + $p2;

        return [
            'ID_EMISSAO' => fake()->unique()->numberBetween(200, 500),
            'NU_FREQUENCIA_INICIO_TX' => $float,
            'NU_FREQUENCIA_TERMINO_TX' => $float,
            'NU_FREQUENCIA_INICIO_RX' => $float,
            'NU_FREQUENCIA_TERMINO_RX' => $float,
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
