<?php

namespace Database\Factories;

use App\Models\Entidade;
use App\Models\Outorga;
use App\Models\Servico;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Outorga>
 */
class OutorgaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'UUID_OUTORGAS' => fake()->uuid(),
            'DE_OUTORGAS' => fake()->sentence(),
            'DE_CERTIFICADO' => fake()->sentence(),
            'NU_FISTEL' => fake()->unique()->numberBetween(10000000000, 99999999999),
            'FK_TB_SERVICO_TB_OUTORGA' => Servico::pluck('ID_SERVICOS')->random(),
            'FK_TB_ENTIDADE_TB_OUTORGA' => Entidade::pluck('UUID_ENTIDADE')->random(),
            'FK_TB_USUARIOS_TB_OUTORGAS_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
