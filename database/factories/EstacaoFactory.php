<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Outorga;
use App\Models\Status;
use App\Models\TipoEstacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Estacao>
 */
class EstacaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $nome = fake()->name();

        return [
            'UUID_ESTACOES' => fake()->uuid(),
            'NO_ESTACOES' => $nome,
            'DE_ESTACOES' => fake()->sentence(17),
            'NU_ESTACOES' => fake()->unique()->randomNumber(),
            'SG_ESTACOES' => $this->sigla($nome),
            'DE_FOTO_ESTACOES' => fake()->randomElement([1, 2, 3, 4]).'.png',
            'FK_TB_TIPO_ESTACOES_TB_ESTACOES' => TipoEstacao::pluck('ID_TIPO_ESTACOES')->random(),
            'FK_TB_STATUS_TB_ESTACOES' => Status::pluck('ID_STATUS')->random(),
            'FK_TB_OUTORGA_TB_ESTACOES' => Outorga::pluck('UUID_OUTORGAS')->random(),
            'CO_LETRA_DEBITO_TFI' => fake()->randomElement(['A', 'B', 'C', 'D']),
            'FK_TB_USUARIOS_TB_ESTACOES_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }

    private function sigla($nome)
    {
        $nomes = Str::ucsplit($nome);

        $letras = array_map(
            fn($n) => Str::of($n)->substr(0, 1)->ascii(),
            $nomes
        );

        return implode('', $letras);
    }
}
