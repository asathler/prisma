<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\EstacaoRadioAmador;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoMovelAeronautica>
 */
class EstacaoRadioAmadorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_RADIO_AMADOR' => fake()->unique()->numberBetween(100, 200),
            'UUID_ESTACAO' => Estacao::pluck('UUID_ESTACOES')->random(),
            'NO_INDICATIVO' => 'PY2GC',
        ];
    }
}
