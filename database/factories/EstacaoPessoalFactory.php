<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoPessoal>
 */
class EstacaoPessoalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_PESSOAL' => fake()->unique()->numberBetween(150, 300),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 10)->pluck('UUID_ESTACOES')->random(),
            'DE_TECNOLOGIA' => Str::slug(fake()->tld())
        ];
    }
}
