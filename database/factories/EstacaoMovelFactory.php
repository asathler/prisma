<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoMovel>
 */
class EstacaoMovelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_MOVEL' => fake()->unique()->numberBetween(150, 300),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 'in', [10, 507, 604])->pluck('UUID_ESTACOES')->random(),
        ];
    }
}
