<?php

namespace Database\Factories;

use App\Models\TipoEstacao;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends Factory<TipoEstacao>
 */
class TipoEstacaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tipo = $this->sorteiaTipo();

        return [
            'NO_TIPO_ESTACOES' => fake()->unique()->words(1, true),
            'IC_TERRENA' => $tipo === 'terrena',
            'IC_MOVEL' => $tipo === 'movel',
            'IC_PESSOAL' => $tipo === 'pessoal',
            'IC_MARITIMA' => $tipo === 'maritima',
            'IC_AERONAUTICA' => $tipo === 'aeronautica',
            'IC_ESPACIAL' => $tipo === 'espacial',
        ];
    }

    private function sorteiaTipo()
    {
        return Arr::random([
            'terrena',
            'movel',
            'pessoal',
            'maritima',
            'aeronautica',
            'espacial',
        ]);
    }
}
