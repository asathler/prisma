<?php

namespace Database\Factories;

use App\Models\ConjuntoEstacao;
use App\Models\Licenca;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEstacaoLicenca>
 */
class ConjuntoEstacaoLicencaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_CONJUNTO_ESTACAO_LICENCA' => fake()->unique()->numberBetween(900, 999),
            'UUID_LICENCA' => Licenca::pluck('UUID_LICENCA')->random(),
            'ID_CONJUNTO_ESTACAO' => ConjuntoEstacao::pluck('ID_CONJUNTO_ESTACAO')->random(),
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
