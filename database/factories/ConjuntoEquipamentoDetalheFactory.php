<?php

namespace Database\Factories;

use App\Models\ConjuntoEquipamento;
use App\Models\Equipamento;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEquipamentoDetalhe>
 */
class ConjuntoEquipamentoDetalheFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_CONJUNTO_EQUIPAMENTO_DETALHE' => fake()->unique()->numberBetween(500, 600),
            'ID_CONJUNTO_EQUIPAMENTO' => ConjuntoEquipamento::pluck('ID_CONJUNTO_EQUIPAMENTO')->random(),
            'UUID_EQUIPAMENTO' => Equipamento::pluck('UUID_EQUIPAMENTO')->random(),
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
