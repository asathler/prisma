<?php

namespace Database\Factories;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Status>
 */
class StatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // TODO: Status parece conter os passos de workflow para os dados de Estação. Confirmar funcionalidade.

        return [
            'DE_STATUS' => fake()->sentence(),
        ];
    }
}
