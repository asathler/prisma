<?php

namespace Database\Factories;

use App\Models\TipoMovimentacao;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<TipoMovimentacao>
 */
class TipoMovimentacaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'CO_TIPO_MOVIMENTACOES' => fake()->numerify(),
            'DE_TIPO_MOVIMENTACOES' => fake()->sentence(),
            'NU_SEQUENCIA' => fake()->unique()->randomNumber(),
        ];
    }
}
