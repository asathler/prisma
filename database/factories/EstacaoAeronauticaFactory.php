<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoMovelAeronautica>
 */
class EstacaoAeronauticaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_AERONAUTICA' => fake()->unique()->numberBetween(800, 899),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 507)->pluck('UUID_ESTACOES')->random(),
            'NO_INDICATIVO' => Str::slug(fake()->name()),
        ];
    }
}
