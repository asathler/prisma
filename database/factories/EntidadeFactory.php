<?php

namespace Database\Factories;

use App\Models\Entidade;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Entidade>
 */
class EntidadeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'UUID_ENTIDADE' => fake()->uuid(),
            'DE_ENTIDADE' => fake()->company(),
            'FK_TB_USUARIOS_TB_ENTIDADES_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
