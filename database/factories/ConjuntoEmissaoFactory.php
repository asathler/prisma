<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEmissao>
 */
class ConjuntoEmissaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_CONJUNTO_EMISSAO' => fake()->unique()->numberBetween(300, 350),
            'NO_CONJUNTO_EMISSAO' => fake()->name()
        ];
    }
}
