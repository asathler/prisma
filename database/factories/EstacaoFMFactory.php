<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\PlanoBasico;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoPessoal>
 */
class EstacaoFMFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_FM' => fake()->unique()->numberBetween(900, 999),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 230)->pluck('UUID_ESTACOES')->random(),
            'NO_INDICATIVO' => Str::slug(fake()->name()),
            'ID_PLANO_BASICO' => PlanoBasico::pluck('ID_PLANOS_BASICOS')->random(),
        ];
    }
}
