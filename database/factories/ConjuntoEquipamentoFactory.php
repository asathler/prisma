<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEquipamento>
 */
class ConjuntoEquipamentoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_CONJUNTO_EQUIPAMENTO' => fake()->unique()->numberBetween(0, 100),
            'NO_NOME_REDE' => fake()->name()
        ];
    }
}
