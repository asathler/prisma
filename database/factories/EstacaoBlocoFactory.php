<?php

namespace Database\Factories;

use App\Models\EstacaoBloco;
use App\Models\Outorga;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<EstacaoBloco>
 */
class EstacaoBlocoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $hab = $this->faker->numberBetween(100, 999);
        $des = $this->faker->numberBetween(10, 99);
        $rea = $this->faker->numberBetween(10, 99);

        return [
            // 'ID_ESTACAO_BLOCOS' => fake()->unique()->numberBetween(500, 600),
            'FK_TB_OUTORGA_TB_ESTACAO_BLOCOS' => Outorga::pluck('UUID_OUTORGAS')->random(),
            'CO_SEQUENCIAL' => $this->faker->randomNumber(),
            'CO_REFERENCIA' => $this->faker->dateTimeBetween('-1 years', 'now')->format('Y-m'),
            'CO_LETRA_DEBITO_TFI' => $this->faker->randomElement(['A', 'B', 'C', 'D']),
            'NU_HABILITADAS' => $hab,
            'NU_DESABILITADAS' => $des,
            'NU_REABILITADAS' => $rea,
            'NU_SALDO' => $hab - $des + $rea,
            'NU_LICENCIADA' => $this->faker->randomNumber(),
            'NU_CREDITO' => $this->faker->randomNumber(),
            'NU_CREDITO_CANCELADO' => $this->faker->randomNumber(),
            'FK_TB_USUARIOS_TB_ESTACAO_BLOCOS_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
