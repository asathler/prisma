<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoFixa>
 */
class EstacaoFixaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @see EstacaoFixaSeeder
     */
    public function definition(): array
    {
        $lat = fake()->latitude();
        $long = fake()->longitude();

        return [
            'ID_ESTACAO_FIXA' => fake()->unique()->numberBetween(200, 299),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 230)->pluck('UUID_ESTACOES')->random(),
            'NU_LATITUDE' => $lat,
            'NU_LONGITUDE' => $long,
            'ED_ENDERECO' => fake()->address(),
            'GF_PONTO' => "POINT({$long} {$lat})",
            // 'area' => $table->multiPolygon('area', 4674)->nullable();
            //'usuario_inclusao' => Usuario::pluck('uuid')->random(),
        ];
    }
}
