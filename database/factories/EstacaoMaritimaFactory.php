<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoMaritima>
 */
class EstacaoMaritimaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_MARITIMA' => fake()->unique()->numberBetween(150, 300),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 604)->pluck('UUID_ESTACOES')->random(),
            'NO_INDICATIVO' => Str::slug(fake()->name()),
            'IC_PROVA' => false,
            // 'data_arrendamento' => null,
            // 'hora_servico' => null,
            // 'classificacao_geral' => null,
            // 'classificacao_individual' => null,
//            'IC_VIAGEM_INTERNACIONAL' => fake()->unique()->numberBetween(0, 1),
            // 'numero_telex' => null,
            // 'chamada_seletiva' => null,
            // 'mmsi' => null,
            // 'mmsi_grupo' => null,
            'DE_NATUREZA_SERVICO' => 'nat',
            'DE_SERVICOS_DISPONIVEIS' => 'sos',

            // 'epirb' => null,
            // 'chamada_seletiva_digital' => null,
            // 'tipo_equipamento_automatico_comunicacao' => null,
            // 'numero_inmarsat' => null,
            // 'tipo_inmarsat' => null,
            // 'iiac_inmarsat' => null,

            // 'inscricao_capitania' => null,
            // 'nome_capitania' => null,
            // 'quantidade_pessoas_bordo' => null,
            // 'quantidade_botes' => null,
            // 'quantidade_carga' => null,
            // 'numero_identificacao_navio' => null,
            // 'responsavel_aaic' => null,
            // 'contato' => null,
        ];
    }
}
