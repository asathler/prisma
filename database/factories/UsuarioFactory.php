<?php

namespace Database\Factories;

use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Usuario>
 */
class UsuarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $nome = fake()->unique()->firstName().' '.fake()->unique()->lastName();
        $email = Str::slug($nome, '.').'@fakemail.com';

        return [
            'UUID_USUARIO' => fake()->uuid(),
            'NO_USUARIO' => $nome,
            'ED_EMAIL' => $email,
            'CO_CPF' => fake()->unique()->numerify('###########'),
            'DH_NASCIMENTO' => fake()->dateTimeBetween('-50 years', '-20 years'),
            'DE_SENHA' => fake()->password(),
        ];
    }
}
