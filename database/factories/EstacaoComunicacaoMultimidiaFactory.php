<?php

namespace Database\Factories;

use App\Models\Estacao;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoMaritima>
 */
class EstacaoComunicacaoMultimidiaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_COMUNICACAO_MULTIMIDIA' => fake()->unique()->numberBetween(150, 300),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 045)->pluck('UUID_ESTACOES')->random(),
            'NU_CAPACIDADE_INSTALADA_MBPS' => fake()->numberBetween(1, 100),
        ];
    }
}
