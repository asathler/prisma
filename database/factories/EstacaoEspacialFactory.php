<?php

namespace Database\Factories;

use App\Models\Estacao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EstacaoEspacial>
 */
class EstacaoEspacialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_ESTACAO_ESPACIAL' => fake()->unique()->numberBetween(150, 300),
            'UUID_ESTACAO' => Estacao::whereRelation('outorga', 'FK_TB_SERVICO_TB_OUTORGA', 185)->pluck('UUID_ESTACOES')->random(),
            'NO_OPERADOR' => fake()->name(),
            'NO_UIT' => fake()->company(),
            'NU_CLASSE' => fake()->numerify('###'),
            'DT_DIREITO_EXPLORACAO' => '1985-01-01',
            'NU_LONGITUTE_ORBITAL' => 0,
            'IC_MERIDIANO' => 1,
            'DE_POSICAO_ORBITAL' => 2,
            'DE_FAIXA_ENTRADA' => 3,
            // 'DE_SUB_FAIXA' => 4,
            'CO_CONCATENADO' => fake()->randomElement(['reed solomon', 'turbo code']),
            'VR_CODIGO_CONCATENADO' => 0
        ];
    }
}
