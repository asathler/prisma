<?php

namespace Database\Factories;

use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Equipamento>
 */
class EquipamentoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'UUID_EQUIPAMENTO' => fake()->uuid(),
            'DE_EQUIPAMENTO' => fake()->sentence(17),
            'NO_MODELO' => fake()->FirstName(),
            'NO_FABRICANTE' => fake()->company(),
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
