<?php

namespace Database\Factories;

use App\Models\ConjuntoEmissao;
use App\Models\Emissao;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ConjuntoEquipamentoDetalhe>
 */
class ConjuntoEmissaoDetalheFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_CONJUNTO_EMISSAO_DETALHE' => fake()->unique()->ean8(),
            'ID_CONJUNTO_EMISSAO' => ConjuntoEmissao::pluck('ID_CONJUNTO_EMISSAO')->random(),
            'ID_EMISSAO' => Emissao::pluck('ID_EMISSAO')->random(),
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
