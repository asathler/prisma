<?php

namespace Database\Factories;

use App\Models\Outorga;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\tffasdasdasd>
 */
class TffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'NU_ANO' => $this->faker->randomNumber(4),
            'UUID_OUTORGA' => Outorga::pluck('UUID_OUTORGA')->random(),
            'QT_TOTAL_ESTACAO' => $this->faker->randomNumber(2),
            'QT_TIPO_A' => $this->faker->randomNumber(2),
            'QT_TIPO_B' => $this->faker->randomNumber(2),
            'QT_TIPO_C' => $this->faker->randomNumber(2),
            'QT_TIPO_D' => $this->faker->randomNumber(2),
            'QT_TIPO_E' => $this->faker->randomNumber(2),
            'QT_TIPO_F' => $this->faker->randomNumber(2),
            'QT_TIPO_G' => $this->faker->randomNumber(2),
            'QT_TIPO_H' => $this->faker->randomNumber(2),
            'QT_TIPO_I' => $this->faker->randomNumber(2),
            'QT_TIPO_J' => $this->faker->randomNumber(2),
            'QT_TIPO_L' => $this->faker->randomNumber(2),
            'QT_TIPO_M' => $this->faker->randomNumber(2),
            'QT_TIPO_N' => $this->faker->randomNumber(2),
            'QT_TIPO_O' => $this->faker->randomNumber(2),
            'QT_TIPO_P' => $this->faker->randomNumber(2),
            'QT_TIPO_Q' => $this->faker->randomNumber(2),
            'QT_TIPO_R' => $this->faker->randomNumber(2),
            'IC_GERADA' => $this->faker->boolean(),
            'UUID_USUARIO_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
