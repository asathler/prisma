<?php

namespace Database\Factories;

use App\Models\Servico;
use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Servico>
 */
class ServicoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ID_SERVICOS' => fake()->unique()->numberBetween(500, 999), //->numerify('###'),
            'DE_SERVICOS' => fake()->sentence(6),
            'FK_TB_USUARIOS_TB_SERVICOS_INCLUSAO' => Usuario::pluck('UUID_USUARIO')->random(),
        ];
    }
}
