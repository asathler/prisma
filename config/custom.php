<?php

// Custom Config
// -------------------------------------------------------------------------------------
//! IMPORTANT: Make sure you clear the browser local storage In order to see the config changes in the template.
//! To clear local storage: (https://www.leadshook.com/help/how-to-clear-local-storage-in-google-chrome-browser/).

return [
    'custom' => [
        // Options[String]: vertical(default), horizontal
        'myLayout' => 'vertical',

        // Options[String]: theme-default(default), theme-bordered, theme-semi-dark
        'myTheme' => 'theme-default',

        // Options[String]: light(default), dark
        'myStyle' => 'light',

        // options[Boolean]: true(default), false
        // To provide RTLSupport or not
        'myRTLSupport' => false,

        // options[Boolean]: false(default), true
        // To set layout to RTL layout (myRTLSupport must be true for rtl mode)
        'myRTLMode' => false,

        // options[Boolean]: true(default), false
        // Display customizer or not THIS WILL REMOVE INCLUDED JS FILE. SO LOCAL STORAGE WON'T WORK
        'hasCustomizer' => true,

        // options[Boolean]: true(default), false
        // Display customizer UI or not, THIS WON'T REMOVE INCLUDED JS FILE. SO LOCAL STORAGE WILL WORK
        'displayCustomizer' => true,

        // options[Boolean]: true(default), false
        // Layout(menu) Fixed
        'menuFixed' => true,

        // options[Boolean]: false(default), true
        // Show menu collapsed, Only for vertical Layout
        'menuCollapsed' => false,

        // options[Boolean]: false(default), true
        // Navbar Fixed
        'navbarFixed' => false,

        // options[Boolean]: false(default), true
        // Footer Fixed
        'footerFixed' => false,

        // true, false (for horizontal layout only)
        'showDropdownOnHover' => true,

        // To show/hide customizer options
        'customizerControls' => [
            'rtl',
            'style',
            'layoutType',
            'showDropdownOnHover',
            'layoutNavbarFixed',
            'layoutFooterFixed',
            'themes',
        ],
    ],
];
