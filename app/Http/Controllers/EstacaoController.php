<?php

namespace App\Http\Controllers;

use App\Models\Estacao;

class EstacaoController extends Controller
{
    public function index()
    {
//        $estacoes = Estacao::all();
//        $estacao = $estacoes[0];

//        dd(
//            'EstacaoController',
//            'index()',
//            $estacao->toArray(),
//            $estacao->tipo->nome,
//            // $estacao->planoBasico->descricao ?? '',
//            // $estacao->status->descricao,
//            // $estacao->outorga->descricao,
//            // $estacao->criador->nome,
//            $estacao->conjunto,
//            //
//            // $estacao,
//            // $estacoes,
//            '...'
//        );
        return view('estacao.index');
    }
    public function index_OLD()
    {
        return view('estacao.index');
    }

    public function show(Estacao $estacao)
    {
        // Se possível, DEVEM ser chamados em seus relacionamentos, os dados de:
        // Equipamentos, Emissoes, Licenças e Historico, etc.

        // 'equipamentos' => get_equipamento($conjunto_equipamento),
        // 'emissoes' => '', //get_emissoes($conjunto_equipamento),
        // 'historicolicenca' => [], // $historico,
        // 'licencas'=> get_licencas($historico),

        // $conjunto_equipamento = ConjuntoEquipamento::where('estacao_uuid', $id)->get();
        // $historico = get_LicencaHistorico($estacao->uuid);

        return view('estacao.show', [
            'estacao' => $estacao
        ]);
    }

    public function json($qtdeRegistros = 10, $filtros = [], $orderByCampos = 'NO_ESTACOES', $orderByDirecao = 'asc')
    {
        return Estacao::listagem(
            $qtdeRegistros,
            $filtros,
            $orderByCampos,
            $orderByDirecao
        );
    }
}
