<?php

namespace App\Http\Controllers\_exemplos\icons;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Tabler extends Controller
{
  public function index()
  {
    return view('content.icons.icons-tabler');
  }
}
