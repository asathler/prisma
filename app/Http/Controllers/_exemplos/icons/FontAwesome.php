<?php

namespace App\Http\Controllers\_exemplos\icons;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FontAwesome extends Controller
{
  public function index()
  {
    return view('content.icons.icons-font-awesome');
  }
}
