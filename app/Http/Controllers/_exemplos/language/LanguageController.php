<?php

namespace App\Http\Controllers\_exemplos\language;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    public function swap($locale)
    {
        $idiomasValidos = [
            'pt_BR',
            'en',
            'es',
            'fr',
            'de'
        ];

        if (!in_array($locale, $idiomasValidos)) {
            abort(400);
        }

        session()->put('locale', $locale);

        App::setLocale($locale);

        return redirect()->back();
    }
}
