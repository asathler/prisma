<?php

namespace App\Http\Controllers\_exemplos\authentications;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResetPasswordCover extends Controller
{
  public function index()
  {
    $pageConfigs = ['myLayout' => 'blank'];
    return view('content.authentications.auth-reset-password-cover', ['pageConfigs' => $pageConfigs]);
  }
}
