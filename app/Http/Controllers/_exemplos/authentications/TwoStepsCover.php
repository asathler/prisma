<?php

namespace App\Http\Controllers\_exemplos\authentications;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TwoStepsCover extends Controller
{
  public function index()
  {
    $pageConfigs = ['myLayout' => 'blank'];
    return view('content.authentications.auth-two-steps-cover', ['pageConfigs' => $pageConfigs]);
  }
}
