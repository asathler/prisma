<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEstacaoLicenca extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_ESTACAO_LICENCA';

    public $timestamps = false;
}
