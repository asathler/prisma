<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoFixa extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_FIXA';

    public $timestamps = false;
}
