<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    // protected $primaryKey = 'id';
    protected $table = 'TB_SERVICOS';
    protected $primaryKey = 'ID_SERVICOS';
    protected $keyType = 'string';

    public $timestamps = false;
    public $incrementing = false;
    // protected $with = ['historicolicenca'];


    public function outorga()
    {
        return $this->hasMany(Outorga::class, 'ID_SERVICOS', 'id');
    }
}
