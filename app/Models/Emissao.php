<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emissao extends Model
{
    use HasFactory;

    protected $table = 'TB_EMISSAO';
    public $timestamps = false;
}
