<?php

namespace App\Models;

use App\Models\Traits\ActivityLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entidade extends Model
{
    use ActivityLog;
    use HasFactory;

    protected $table = 'TB_ENTIDADES';
    protected $primaryKey = 'UUID_ENTIDADE';
    protected $keyType = 'string';

    public $incrementing = false;
    public $timestamps = false;
}
