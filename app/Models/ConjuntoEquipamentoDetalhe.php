<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEquipamentoDetalhe extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_EQUIPAMENTO_DETALHE';
    public $timestamps = false;
    protected $with = ['equipamentos'];

    public function equipamentos()
    {
        return $this->hasMany(Equipamento::class, 'UUID_EQUIPAMENTO', 'UUID_EQUIPAMENTO', 'equipamentos');
    }
}
