<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEstacao extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_ESTACAO';
    // // // protected $with = ['conjuntoEquipamentos', 'conjuntoEmissoes'];

    public $timestamps = false;

    public function estacoes()
    {
        return $this->hasMany(Estacao::class, 'UUID_ESTACAO', 'UUID_ESTACAO');
    }

    public function conjuntoEquipamentos()
    {
        return $this->hasMany(ConjuntoEquipamento::class, 'ID_CONJUNTO_EQUIPAMENTO', 'ID_CONJUNTO_EQUIPAMENTO');
    }

    public function conjuntoEmissoes()
    {
        return $this->hasMany(ConjuntoEmissao::class, 'ID_CONJUNTO_EMISSAO', 'ID_CONJUNTO_EMISSAO');
    }

    public function equipamentos()
    {
        return $this
            ->join(
                'TB_CONJUNTO_EQUIPAMENTO_DETALHE',
                'TB_CONJUNTO_EQUIPAMENTO_DETALHE.ID_CONJUNTO_EQUIPAMENTO_DETALHE',
                '=',
                'TB_CONJUNTO_ESTACAO.ID_CONJUNTO_EQUIPAMENTO'
            )
            ->join(
                'TB_EQUIPAMENTO',
                'TB_EQUIPAMENTO.UUID_EQUIPAMENTO',
                '=',
                'TB_CONJUNTO_EQUIPAMENTO_DETALHE.UUID_EQUIPAMENTO'
            )
            ->where('TB_CONJUNTO_ESTACAO.id', $this->id)
            ->get()
        ;
    }

    public function emissoes()
    {
        return $this
            ->join(
                'conjunto_emissao_detalhes',
                'conjunto_emissao_detalhes.conjunto_id',
                '=',
                'TB_CONJUNTO_ESTACAO.conjunto_emissao_id'
            )
            ->join('emissoes', 'emissoes.id', '=', 'conjunto_emissao_detalhes.emissao_id')
            ->where('TB_CONJUNTO_ESTACAO.id', $this->id)
            ->get()
        ;
    }
}
