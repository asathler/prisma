<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoMovimentacao extends Model
{
    use HasFactory;

    protected $table = 'TB_TIPO_MOVIMENTACOES';
    public $timestamps = false;
}
