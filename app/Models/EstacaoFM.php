<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EstacaoFM extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_FM';

    public $timestamps = false;

    
    public function planoBasico(): BelongsTo
    {
        return $this->belongsTo(PlanoBasico::class, 'ID_PLANO_BASICO', 'ID_PLANOS_BASICOS', 'planos_basicos');
    }
}
