<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoMovel extends Model
{
    use HasFactory;

    protected $table = 'estacoes_moveis';

    public $timestamps = false;
}
