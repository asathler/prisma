<?php

namespace App\Models;

use App\Models\Traits\ActivityLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Outorga extends Model
{
    use ActivityLog;
    use HasFactory;

    protected $table = 'TB_OUTORGAS';
    protected $primaryKey = 'UUID_OUTORGAS';
    protected $keyType = 'string';

    public $incrementing = false;
    public $timestamps = false;

    public function entidade()
    {
        return $this->belongsTo(Entidade::class, 'FK_TB_ENTIDADE_TB_OUTORGA', 'UUID_ENTIDADE', 'entidades');
    }

    public function servico()
    {
        return $this->belongsTo(Servico::class, 'ID_SERVICOS', 'ID_SERVICOS', 'servico');
    }
}
