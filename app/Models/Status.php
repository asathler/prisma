<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $table = 'TB_STATUS';
    protected $primaryKey = 'ID_STATUS';
    public $timestamps = false;
}
