<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEquipamento extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_EQUIPAMENTO';
    public $timestamps = false;
    protected $with = ['detalhes'];

    public function detalhes()
    {
        return $this->hasMany(ConjuntoEquipamentoDetalhe::class, 'conjunto_id', 'id');
    }
}
