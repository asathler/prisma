<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $table = 'TB_USUARIO';
    protected $primaryKey = 'UUID_USUARIO';
    protected $keyType = 'string';

    public $incrementing = false;
    public $timestamps = false;

    protected $hidden = [
        'senha'
    ];

    public function cpfLgpd()
    {
        $mascara = '%s%s%s.***.***-%s%s';
        $campo = $this->CO_CPF;

        return $this->formato($mascara, $campo);
    }

    public function cpfFormatado()
    {
        $mascara = '%s%s%s.%s%s%s.%s%s%s-%s%s';
        $campo = $this->CO_CPF;

        return $this->formato($mascara, $campo);
    }

    public function formato($mascara, $campo)
    {
        return vsprintf(
            $mascara,
            str_split($campo)
        );
    }
}
