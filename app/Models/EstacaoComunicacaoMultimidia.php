<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoComunicacaoMultimidia extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_COMUNICACAO_MULTIMIDIA';

    public $timestamps = false;
}
