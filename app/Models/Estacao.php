<?php

namespace App\Models;

use App\Http\Controllers\_exemplos\wizard_example\PropertyListing;
use App\Models\Traits\ActivityLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Estacao extends Model
{
    use ActivityLog;
    use HasFactory;

    protected $table = 'TB_ESTACOES';
    protected $primaryKey = 'UUID_ESTACOES';
    protected $keyType = 'string';
    // protected static $logAttributes = true;
    // protected static $recordEvents = ['created', 'updated', 'deleted'];
    // protected $appends = ['action'];

    public $incrementing = false;
    public $timestamps = false;

    public function tipo(): BelongsTo
    {
        return $this->belongsTo(TipoEstacao::class, 'FK_TB_TIPO_ESTACOES_TB_ESTACOES', 'ID_TIPO_ESTACOES', 'tipo_estacoes');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'FK_TB_STATUS_TB_ESTACOES', 'ID_STATUS', 'status');
    }

    public function outorga(): BelongsTo
    {
        return $this->belongsTo(Outorga::class, 'FK_TB_OUTORGA_TB_ESTACOES', 'UUID_OUTORGAS', 'outorga');
    }

    public function criador(): BelongsTo
    {
        return $this->belongsTo(Usuario::class, 'FK_TB_USUARIOS_TB_ESTACOES_INCLUSAO', 'UUID_USUARIO', 'usuarios');
    }

    public function conjunto(): HasMany
    {
        return $this->hasMany(ConjuntoEstacao::class, 'UUID_ESTACAO', 'UUID_ESTACAO');
    }


    public static function listagem($qtdeRegistros = 10, $filtros = [], $orderByCampos = 'NO_ESTACOES', $orderByDirecao = 'asc')
    {
        $columns = array(
            0 =>'uuid',
            1 =>'sigla',
            2 =>'nome',
            3=> 'nome_indicativo',
            4=> 'descricao'
        );
        $request = request();
        $totalData = Estacao::count();

        $totalFiltered = $totalData;
        $limit = $qtdeRegistros;
        $start = 0;
        $order = $orderByCampos;
        $dir = $orderByDirecao;
        $filtro = false;
//        foreach ($request->columns as $colunas) {
//            // NOTE: O filtro para Busca das palavras Ocorre somente se o termo digitador tiver ao menos 4 caracteres
//            if ($colunas['search']['value'] && strlen($colunas['search']['value']) > 4) {
//                $filtro = true;
//            };
//        }
        if(!$filtro)
        {
            $estacoes = Estacao::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $flag = false;
            $estacoes = '';
            foreach ($request->columns as $colunas) {
                if ($colunas['search']['value'] && strlen($colunas['search']['value']) > 4) {
                    $search = $colunas['search']['value'];

                    if(!$flag) {
                        $flag = true;
                        $totalFiltered = Estacao::where($colunas['data'], 'LIKE', "%{$search}%")->count();
                        $estacoes = Estacao::where($colunas['data'], 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    }else{
                        $totalFiltered = $estacoes::where($colunas['data'], 'LIKE', "%{$search}%")->count();
                        $estacoes::where($colunas['data'], 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    }
                }
            }
        }
        $data = array();
        if(!empty($estacoes))
        {
            foreach ($estacoes as $estacao)
            {
                $nestedData['uuid'] = $estacao->UUID_ESTACOES;
                $nestedData['sigla'] = $estacao->SG_ESTACOES;
                $nestedData['nome'] = $estacao->NO_ESTACOES;
                $nestedData['nome_indicativo'] = $estacao->nome_indicativo;
                $nestedData['descricao'] =  $estacao->DE_ESTACOES;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

//        return Estacao::select([
//            'uuid',
//            'sigla',
//            'nome',
//            'nome_indicativo',
//            'descricao'
//        ])
//            ->orderBy($orderByCampos, $orderByDirecao)
//            ->paginate($qtdeRegistros);
    }







    public function ___licencas()
    {
        return $this->hasMany(Licenca::class, 'outorga_uuid', 'uuid', 'outorgas');
    }

    public function ___getActionAttribute($value)
    {
        return '1';
    }

    public function ConjuntoEstacao()
    {
        return $this->hasMany(ConjuntoEstacao::class, 'estacao_uuid', 'uuid');
    }



    /*
    // TODO: Buscar do relacionamento de outorga, se necessário
    public function entidade()
    {
        return $this->belongsTo(Entidade::class, 'entidades_uuid', 'uuid', 'entidades');
    }


    public function dataInicioLicenca()
    {
        return Carbon::createFromFormat(
            'Y-m-d',
            $this->historicoLicenca()->first('data_inicio')->data_inicio
        );
    }

    public function dataTerminoLicenca()
    {
        return Carbon::createFromFormat(
            'Y-m-d',
            $this->historicoLicenca()->orderByDesc('data_termino')->first()->data_termino
        );
    }

    public function conjuntoEstacao()
    {
        return $this->hasMany(ConjuntoEstacao::class, 'estacao_uuid', 'uuid');
    }

    public function full()
    {
        return $this
            // ->where('estacoes.uuid', 'abcdef00-e9da-439d-87ce-7cbadf778899')
            ->leftJoin('tabela_20 as T2', 'T2.estacao_uuid', '=', 'estacoes.uuid')
            ->leftJoin('conjunto_equipamento as CQ', 'CQ.conjunto_id', '=', 'T2.conjunto_equipamento_id')
            ->leftJoin('equipamentos as QP', 'QP.uuid', '=', 'CQ.equipamento_uuid')
            ->leftJoin('conjunto_emissao as CM', 'CM.conjunto_id', '=', 'T20.conjunto_emissoes_id')
            ->leftJoin('emissoes as EM', 'EM.id', '=', 'CM.emissoes_id')
            // ->get()
            ;
    }

    public function licencas()
    {
        return $this->hasManyThrough(
            Licenca::class,
            HistoricoLicenca::class,
            'estacoes_uuid',
            'uuid',
            '',
            'licencas_uuid'
        );
    }
    */
}

class Estacao_OLD extends Model
{
    // TODO: Ver passagem dos parâmetros vindo da requisição para retorno dos registros corretos!
    public static function listagem($qtdeRegistros = 10, $filtros = [], $orderByCampos = 'nome', $orderByDirecao = 'asc')
    {
        $columns = array(
            0 =>'uuid',
            1 =>'sigla',
            2 =>'nome',
            3=> 'nome_indicativo',
            4=> 'descricao'
        );
        $request = request();
        $totalData = Estacao::count();

        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $filtro = false;
        foreach ($request->columns as $colunas) {
            // NOTE: O filtro para Busca das palavras Ocorre somente se o termo digitador tiver ao menos 4 caracteres
            if ($colunas['search']['value'] && strlen($colunas['search']['value']) > 4) {
                $filtro = true;
            };
        }
        if(!$filtro)
        {
            $estacoes = Estacao::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $flag = false;
            $estacoes = '';
            foreach ($request->columns as $colunas) {
                if ($colunas['search']['value'] && strlen($colunas['search']['value']) > 4) {
                    $search = $colunas['search']['value'];

                    if(!$flag) {
                        $flag = true;
                        $totalFiltered = Estacao::where($colunas['data'], 'LIKE', "%{$search}%")->count();
                        $estacoes = Estacao::where($colunas['data'], 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    }else{
                        $totalFiltered = $estacoes::where($colunas['data'], 'LIKE', "%{$search}%")->count();
                        $estacoes::where($colunas['data'], 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    }
                }
            }
        }
        $data = array();
        if(!empty($estacoes))
        {
            foreach ($estacoes as $estacao)
            {
                $nestedData['uuid'] = $estacao->uuid;
                $nestedData['sigla'] = $estacao->sigla;
                $nestedData['nome'] = $estacao->nome;
                $nestedData['nome_indicativo'] = $estacao->nome_indicativo;
                $nestedData['descricao'] =  $estacao->descricao;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

//        return Estacao::select([
//            'uuid',
//            'sigla',
//            'nome',
//            'nome_indicativo',
//            'descricao'
//        ])
//            ->orderBy($orderByCampos, $orderByDirecao)
//            ->paginate($qtdeRegistros);
    }

    /*
    // TODO: Buscar do relacionamento de outorga, se necessário
    public function entidade()
    {
        return $this->belongsTo(Entidade::class, 'entidades_uuid', 'uuid', 'entidades');
    }
    */

    public function historicoLicenca()
    {
        return $this->hasMany(HistoricoLicenca::class, 'estacoes_uuid', 'uuid');
    }

    public function dataInicioLicenca()
    {
        return Carbon::createFromFormat(
            'Y-m-d',
            $this->historicoLicenca()->first('data_inicio')->data_inicio
        );
    }

    public function dataTerminoLicenca()
    {
        return Carbon::createFromFormat(
            'Y-m-d',
            $this->historicoLicenca()->orderByDesc('data_termino')->first()->data_termino
        );
    }






    /*
    public function full()
    {
        return $this
            // ->where('estacoes.uuid', 'abcdef00-e9da-439d-87ce-7cbadf778899')
            ->leftJoin('tabela_20 as T2', 'T2.estacao_uuid', '=', 'estacoes.uuid')
            ->leftJoin('conjunto_equipamento as CQ', 'CQ.conjunto_id', '=', 'T2.conjunto_equipamento_id')
            ->leftJoin('equipamentos as QP', 'QP.uuid', '=', 'CQ.equipamento_uuid')
            ->leftJoin('conjunto_emissao as CM', 'CM.conjunto_id', '=', 'T20.conjunto_emissoes_id')
            ->leftJoin('emissoes as EM', 'EM.id', '=', 'CM.emissoes_id')
            // ->get()
            ;
    }
    */

    public function licencas()
    {
        return $this->hasManyThrough(
            Licenca::class,
            HistoricoLicenca::class,
            'estacoes_uuid',
            'uuid',
            '',
            'licencas_uuid'
        );
    }
}
