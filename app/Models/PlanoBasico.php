<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanoBasico extends Model
{
    use HasFactory;

    protected $table = 'TB_PLANOS_BASICOS';

    public $timestamps = false;
}
