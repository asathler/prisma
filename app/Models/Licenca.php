<?php

namespace App\Models;

use App\Models\Traits\ActivityLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Licenca extends Model
{
    use ActivityLog;
    use HasFactory;

    protected $primaryKey = 'UUID_LICENCA';
    protected $table = 'TB_LICENCA';
    protected $keyType = 'string';

    public $incrementing = false;
    public $timestamps = false;
}
