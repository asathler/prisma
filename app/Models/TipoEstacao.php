<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoEstacao extends Model
{
    use HasFactory;

    protected $table = 'TB_TIPO_ESTACOES';
    protected $primaryKey = 'ID_TIPO_ESTACOES';
    public $timestamps = false;
}
