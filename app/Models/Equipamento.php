<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Equipamento
 * @package App\Models
 * @note    SCH (antigo), SGCH (atual) e CERTIFICA (futuro) são os "donos" deste negócio!
 *          PK = CodHomologacao
 */
class Equipamento extends Model
{
    use HasFactory;

    protected $table = 'TB_EQUIPAMENTO';
    protected $primaryKey = 'UUID_EQUIPAMENTO';
    protected $keyType = 'string';

    public $incrementing = false;
    public $timestamps = false;
}
