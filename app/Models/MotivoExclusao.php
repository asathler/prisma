<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotivoExclusao extends Model
{
    use HasFactory;

    protected $table = 'TB_MOTIVO_EXCLUSAO';
    public $timestamps = false;
}
