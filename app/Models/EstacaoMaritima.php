<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoMaritima extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_MARITIMA';

    public $timestamps = false;
}
