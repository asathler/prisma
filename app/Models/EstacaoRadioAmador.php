<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoRadioAmador extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_RADIO_AMADOR';

    public $timestamps = false;
}
