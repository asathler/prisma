<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEmissao extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_EMISSAO';
    public $timestamps = false;
    protected $with = ['emissoes'];

    public function emissoes()
    {
        return $this->hasMany(Emissao::class, 'id', 'emissao_id');
    }
}
