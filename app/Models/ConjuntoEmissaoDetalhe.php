<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConjuntoEmissaoDetalhe extends Model
{
    use HasFactory;

    protected $table = 'TB_CONJUNTO_EMISSAO_DETALHE';
    protected $with = ['emissoes'];

    public $timestamps = false;

    public function emissoes()
    {
        return $this->hasMany(Emissao::class, 'ID_EMISSAO', 'ID_EMISSAO');
    }
}
