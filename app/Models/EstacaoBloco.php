<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoBloco extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_BLOCOS';

    public $timestamps = false;
}
