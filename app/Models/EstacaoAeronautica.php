<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstacaoAeronautica extends Model
{
    use HasFactory;

    protected $table = 'TB_ESTACAO_AERONAUTICA';

    public $timestamps = false;
}
