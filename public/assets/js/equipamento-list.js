/**
 * DataTables Advanced (jquery)
 */
'use strict';
$(function () {
    let dt_basic_table = $('.datatables-basic-equipamento'),
        dt_basic;

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
        dt_basic = dt_basic_table.DataTable({
            ajax: baseUrl + 'equipamento/list/'+$estacao_id,
            columns: [
                { data: 'equipamento' },
                { data: 'fabricante' },
                { data: 'modelo' },
                { data: 'descricao' },
            ],
            columnDefs: [
                {
                    visible: false,
                    targets: 0,
                    render: function (data, type, full, meta) {
                        return data;
                    }
                },     {
                    targets: 1,
                    render: function (data, type, full, meta) {
                        return data;
                    }
                },     {
                    targets: 2,
                    render: function (data, type, full, meta) {
                        return data;
                    }
                },     {
                    targets: 3,
                    render: function (data, type, full, meta) {
                        return data;
                    }
                }

            ],
            dom:
                '<"card-header flex-column flex-md-row"<"head-label-equipamento text-center"><"row me-2"' +
                '<"col-md-2"<"me-3">>' +
                '<"col-md-10"<" text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"f>>' +
                '>>t',
            displayLength: 4,
        });
        $('div.head-label-equipamento').html('<h5 class="card-title mb-0">Equipamentos</h5>');
    }

    // on key up from input field
    $('input.dt-input').on('keyup', function () {
        filterColumn($(this).attr('data-column'), $(this).val());
    });

    // Filter form control to default size
    // ? setTimeout used for multilingual table initialization
    setTimeout(() => {
        $('.dataTables_filter .form-control').removeClass('form-control-sm');
        $('.dataTables_length .form-select').removeClass('form-select-sm');
    }, 200);
});

